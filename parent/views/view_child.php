<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Home | Parent Portal</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />-->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>
<body>
	<div id="wrapper">
    	<?php include('includes/top.php'); ?>
        <div id="menu">
        	<?php include('includes/nav.php'); ?>
        </div>
        <div id="content">
        	
        	<div id="content-main">
            	<table width="95%" border="0">
                  <tr>
                    <td width="13%" valign="top"><?php include("includes/left-side.php"); ?></td>
                    <td width="87%" valign="top">
                    <table width="95%" border="0" align="center">
                  <tr>
                    <td width="25%"><h3><?php echo $std[0]['firstname']," ",$std[0]['lastname']; ?></h3></td>
                    <td width="75%"><!--<a href="<?php echo $timetable; ?>">Download timetable </a>--></td>
                    </tr>
                </table>
                <table width="95%" border="0" align="center">
                  <tr>
                    <td valign="top">Status:</td>
                    <td valign="top"><img src="../images/<?php echo $std[0]['status']; ?>.gif"  height="20"/></td>
                  </tr>
                  <tr>
                    <td width="166" valign="top">Student ID: </td>
                    <td width="780" valign="top"><?php echo $std[0]['student_id']; ?></td>
                  </tr>
                  
                  <tr>
                    <td valign="top">Current Class: </td>
                    <td valign="top"><?php 
                    if(student::is_registered($std[0]['id']) == "Yes")
                        echo $student_obj->get_current_class($std[0]['id']);
                    elseif(student::is_registered($std[0]['id']) == "No") 
                        echo "Not Registered!";
                    ?></td>
                  </tr>
                  <tr>
                    <td valign="top">Date of birth </td>
                    <td valign="top"><?php echo readable_date2($std[0]['dob']); ?></td>
                  </tr>
                </table>
                  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
                    <tr>
                      <td colspan="5"><h3>Grades</h3></td>
                    </tr>
                    <tr>
                      <td width="31%">Subject</td>
                      <?php echo $header; ?>
                      <td width="20%">Total</td>
                    </tr>
                    <?php 
                    if(count($data) > 0)
                    {
                        for($i= 0; $i < count($data); $i++) {?>
                    <tr bgcolor="<?php echo color($i); ?>">
                      <td><?php echo $data[$i]['name']; ?></td>
                      <?php echo $asst->assesment_scores($data[$i]); ?>
                      <td><?php echo $data[$i]['total']; ?></td>
                    </tr>
                    <?php }
                    }
                    else{
                    ?>
                    <tr >
                      <td colspan="5" align="center" style="font-weight:bold">No grades entered yet!!</td>
                    </tr>
                    <?php
                    }
                    ?>
                  </table>  
                    </td>
                  </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>