<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Notice: <?php echo $notice[0]['title']; ?></title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col">
<div style="width:95%; margin-left:auto; margin-right:auto;">
     <br />
     <table width="50%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><h2>Notices</h2></td>
        </tr>
		 <tr>
          <td bgcolor="#FFFF99"> Click on the notice to view details</td>
        </tr>
		<?php if(count($notices) > 0): ?>
        <?php foreach($notices as $notice): ?>
	    <tr>
          <td><a href="index.php?action=view_notice&amp;id=<?php echo $notice['id']; ?>"><?php echo $notice['title']; ?></a></td>
        </tr>
		<?php endforeach; ?>
		<?php endif; ?>
    </table>
</div>
</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
