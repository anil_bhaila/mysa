<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Change Password | Parent Portal</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />-->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>
<body>
	<div id="wrapper">
    	<?php include('includes/top.php'); ?>
        <div id="menu">
        	<?php include('includes/nav.php'); ?>
        </div>
        <div id="content">
        	
        	<div id="content-main">
            <form id="change_password" name="change_password" method="post" action="index.php?action=change_password">
              <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
                <tr>
                  <td colspan="2"><h2>Change Password</h2></td>
                </tr>
                <tr>
                  <td colspan="2" style="color:#EE0000"><?php echo $msg; ?></td>
                </tr>
                <tr>
                  <td width="23%">Parent ID</td>
                  <td width="77%">
                  <input type="hidden" name="username" value="<?php echo $_SESSION['userinfo']['email']; ?>" />
                  <input type="hidden" name="parent_id" value="<?php echo $_SESSION['userinfo']['parent_id']; ?>" />
                  <?php echo $_SESSION['userinfo']['email']; ?>      </td>
                </tr>
                <tr>
                  <td>Old password </td>
                  <td><label>
                    <input type="password" name="old_password" />
                  </label></td>
                </tr>
                <tr>
                  <td>New password </td>
                  <td><input type="password" name="new_password[]" /></td>
                </tr>
                <tr>
                  <td>Confrim new password </td>
                  <td><label>
                    <input type="password" name="new_password[]" />
                  </label></td>
                </tr>
                
                <tr>
                  <td>&nbsp;</td>
                  <td><label>
                    <input type="submit" name="Submit" value="Submit" />
                  </label></td>
                </tr>
              </table>
            </form>

            </div>
        </div>
    </div>
</body>
</html>