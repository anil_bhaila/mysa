<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">

h2
{
	margin:0;
}

#left-pane
{
	width:800px;
	height:600px;
	border:1px solid #CCC;
	margin-right: 20px;
	float:left;
	padding:0.5em;
}

#right-pane
{
	width:200px;
	border:1px solid #0C6;
	float:left;
	padding:0.5em;
}

.widget
{
	width:150px;
	height: 200px;
	background-color:#9CC;
	margin-bottom: 10px;
	margin-left:auto;
	margin-right:auto;
}

</style>
</head>

<body>
<div id="left-pane">
	<h1><img src="" alt="" name="logo" width="133" height="150" align="top" id="logo" />Welcome to Christian Royal College</h1>
</div>
<div id="right-pane">
	<div id="address" class="widget">Contact Us</div>
	<div id="events" class="widget">Upcoming Events</div>
    <div id="info" class="widget">Other Info</div>
</div>
</body>
</html>