<?php
  class user extends Model
  {
      var $table_name = "users";
      var $primary_key = "username";

      function change_password($table)
      {
            $username = $_POST['username'];
            $old_password = $_POST['old_password'];
            $new_password = $_POST['new_password'];

           
            if(empty($new_password[0]))
            {
                $msg = "All fields are required and must not be empty";
                include(VIEWPATH."change_password.php");
                exit;
            }

            //check if old password is correct and make changes
            $login = new login($username,$old_password);
            $login = $this->set_columns($login, $table);
            $login->set_table($table);
            if($login->authenticate())
            {
                //check that both new passwords match
                if($new_password[0] == $new_password[1])
                {
                        $new = array('password' => md5($new_password[0]));

                        switch($table)
                        {
                            case "admins":
                                $admin_obj = new admin();
                                $admin_obj->update($username,$new);
                                break;
                            case "teachers":
                                $teacher_obj = new teacher();
                                $id = $_POST['id'];
                                $teacher_obj->update($id,$new);
                                break;
                            case "parents":
                                $parent_obj = new thirdeye_parent();
                                $parent_id = $_POST['parent_id'];
                                $parent_obj->update($parent_id,$new);
                                break;
                            case "students":
                                $student_obj = new student();
                                $id = $_POST['id'];
                                $student_obj->update($id,$new);
                                break;
                        }
                        
                        $msg = "Your changes were made successfully. Changes will take effect next time you login";
                        include(VIEWPATH."change_password.php");
                        //inform user that changes will apply when next they login
                }
                else
                {
                    $msg = "The new passwords you supplied does not match. Please re-type again";
                    include(VIEWPATH."change_password.php");
                }

            }
            else
            {
                $msg =  "There is an error in the password supplied";
                include(VIEWPATH."change_password.php");
                echo formPopulator::populate();
            }
      }

      private function set_columns(login $login,$table)
      {
            switch($table)
            {
                case "teachers":
                    $login->set_columns("teacher_id","password");
                    break;
                case "parents":
                    $login->set_columns("email","password");
                    break;
                case "students":
                    $login->set_columns("student_id","password");
                    break;
            }
           return $login;
      }
    
  }
?>
