<?php

/**
 * This class extends the Model class and respresents a class in a school. It consists of methods that are used manuipulate and
 * retrieve information about a class.
 * @author FO
 * @version 1.0
 * @package mysa
 */
class schoolClass extends Model {
    var $table_name = "classes";

    var $primary_key = "class_id";

	/**
	* This function overrides the parent select_all function
	* (non-PHPdoc)
	* @see Admin/public_html/lib/Model#select_all($select_column, $start, $where)
	*/
	function select_all(array $select_column = null, $start = null,$where = null) {
	$sql = "SELECT class_id, firstname, lastname
				FROM classes
				LEFT JOIN teachers
				ON  teachers.id = classes.teacher_id
				ORDER BY class_id
				";
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else
	    return $this->mysql->fetch($this->mysql->result);
	}

	/**
	* gets all the subjects offered in a class
	* @param string $class_id - class id of the class
	* @return Array
	*/
	function get_subjects($class_id) {
	$sql = "SELECT class_id, subjects.subject_id, name
				FROM subjects, class_subjects
				WHERE subjects.subject_id = class_subjects.subject_id and class_id='$class_id'
		";
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else
	    return $this->mysql->fetch($this->mysql->result);
	}
	
	function offer_subject($subject_id)
	{
		$sql = "SELECT class_id
				FROM  class_subjects
				WHERE subject_id = '$subject_id'
		";
		if (!$this->mysql->execute_query($sql))
			echo get_class($this)."-".$this->mysql->error;
		else
			return $this->mysql->fetch($this->mysql->result);
	}

	/**
	* gets the student ids of all the student in a given class
	* @param string $class_id
	* @param string $session_id
	* @param int term
	* @return Array
	*/
	function get_student_ids($class_id, $session, $term)
	{
		$sql = "SELECT id
			        FROM students, student_class
					WHERE student_class.student_id = students.id
					AND student_class.class_id = '$class_id'
					AND student_class.session = '$session'
					AND student_class.term = '$term'
			";

		//echo $sql;
		if (!$this->mysql->execute_query($sql))
		    echo get_class($this)."-".$this->mysql->error;
		else {
		    return $this->mysql->fetch($this->mysql->result);
		}
	}

	/**
	* gets the number of students in a given class for a given session and term
	* @param $class_id
	* @param $session
	* @param $term
	* @return int - returns the number of students in the supplied class
	*/
	function get_student_count($class_id) {
	$sess = new session();
	$curr = $sess->get_current_session_term();
	$session = $curr['session'];
	$term = $curr['term'];
	
	$sql = "SELECT id
				FROM students, student_class
				WHERE student_class.student_id = students.id
				AND student_class.class_id = '$class_id'
				AND student_class.session = '$session'
				AND student_class.term = '$term'
		";
	if (!$this->mysql->execute_query($sql)) {
	    echo get_class($this)."-".$this->mysql->error;
	    exit();
	}
	else {
	    return $this->mysql->rows_affected();
	}
	}

	/**
	* gets the students in a class. If subject id is provided, it will return students who offer that particular subject e.g Jss3A students who offer french
	* @param string $class_id
	* @param string $subject_id
	* @return Array
	*/
	function get_students($class_id,$subject_id = null) {
	$sess = new session();
	$curr = $sess->get_current_session_term();
	$session = $curr['session'];
	$term = $curr['term'];

	$sql = "SELECT id, firstname, lastname FROM students, student_class
			WHERE students.id = student_class.student_id 
			AND students.status = 'enrolled' 
			AND student_class.class_id = '$class_id' 
			AND session = '$session' 
			AND term = '$term'";

	if(!is_null($subject_id))
	{
		$sql = "SELECT id, firstname, lastname, name, student_class.session_id
				FROM students, student_class, student_subjects, subjects
				WHERE students.id = student_class.student_id
				AND student_class.session_id = student_subjects.session_id
				AND student_subjects.subject_id = subjects.subject_id
				AND students.status = 'enrolled'
				AND student_class.class_id = '$class_id'
				AND session = '$session'
				AND term = '$term'
				AND subjects.subject_id = '$subject_id' 
				ORDER BY students.id";
	}
	//echo $sql;
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else
	    return $this->mysql->fetch($this->mysql->result);
	}

	/**
	* gets the grades of all students in a given class for a particular subject
	* @param $class_id
	* @param $subject_id
	* @return Array
	*/
	function get_students_grades($class_id,$subject_id) {
	$sess = new session();
	$curr = $sess->get_current_session_term();
	$session = $curr['session'];
	$term = $curr['term'];

	$asst = new assesment();
	$asst_string = $asst->assesment_string();

	$sql = "SELECT student_class.session_id, firstname, lastname, students.student_id, students.id, grades.*, ($asst_string) as total
				FROM students, student_class, grades
				WHERE  grades.session_id = student_class.session_id
				AND student_class.student_id = students.id
		                AND students.status = 'enrolled'
				AND grades.subject_id = '$subject_id'
				AND student_class.class_id = '$class_id'
				AND student_class.session = '$session'
				AND student_class.term = '$term'
		";

	//echo $sql;
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else
	    return $this->mysql->fetch($this->mysql->result);
	}

	/**
	* gets the list of all students in a class, irrespective of the subject
	* @param string $class_id
	* @param string $session - academic session
	* @param int $term - academic term
	* @return Array
	*/
	function get_student_list($class_id,$session,$term) {
	$sql = "SELECT firstname, lastname, students.id
				FROM students, student_class
				WHERE student_class.student_id = students.id
				AND student_class.class_id = '$class_id'
				AND student_class.session = '$session'
				AND student_class.term = '$term'
				AND students.status != 'expelled'
		";
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else
	    return $this->mysql->fetch($this->mysql->result);
	}

	function add_subject($class_id, $subject_id, $teacher_id) {
	$sql = "INSERT INTO `class_subjects` (`class_id`, `subject_id`, `teacher_id`)
				VALUES ('$class_id','$subject_id','$teacher_id')";
	if(!$this->mysql->execute_query($sql)) {
	    echo "Query failed to execute<br>{$this->mysql->error}<br>$sql";
	    exit();
	}
	}
	
	function remove_subject($class_id, $subject_id) {
	$sql = "DELETE FROM `class_subjects` WHERE class_id = '$class_id' AND subject_id = '$subject_id'";
	if(!$this->mysql->execute_query($sql)) {
	    echo "Query failed to execute<br>{$this->mysql->error}<br>$sql";
	    exit();
	}
	}

	function insert_grades($post) {
	$scores = $post['score'];
	$ca = strtolower($_SESSION['work']['ca']);
	$subject_id = $_SESSION['work']['subid'];
	$entered_by = $_SESSION['userinfo']['firstname']." ".$_SESSION['userinfo']['lastname'];
	foreach($scores as $session_id => $score) {

	    $comment = $post['comment'][$session_id];

	    $sql = "INSERT INTO `grades` (`session_id`, `subject_id`, `comment`,`entered_by`,`$ca`)
					VALUES ('$session_id','$subject_id','$comment','$entered_by','$score')
					ON DUPLICATE KEY UPDATE `$ca`='$score', `comment`='$comment'
		    ";

	    //echo $sql; exit;

	    if (!$this->mysql->execute_query($sql))
		echo get_class($this)."-".$this->mysql->error;

	}

	}

	function delete($id) {
	//delete classs
	parent::delete($id);
	//delete all subjects registered to the class
	self::delete_subject($id);
	}

	public function delete_subject($class_id) {
	$sql = "DELETE FROM class_subjects WHERE class_id='$class_id'";
	if (!$this->mysql->execute_query($sql))
	    die(get_class($this)."-".$this->mysql->error);
	}

	function get_teachers($class_id) {
	$sql = "SELECT teacher_id, subject_id
				FROM class_subjects
				WHERE class_id = '$class_id'";
	if (!$this->mysql->execute_query($sql))
	    echo get_class($this)."-".$this->mysql->error;
	else {
	    $rs = $this->mysql->fetch($this->mysql->result);
	    for($i = 0; $i < count($rs); $i++) {
		$teachers[$rs[$i]['subject_id']] = $rs[$i]['teacher_id'];
	    }
	    return $teachers;
	}
	}

	
	function get_teacher($class_id, $subject_id){
	$sql = "SELECT CONCAT(firstname,' ',lastname) as name
				FROM teachers, class_subjects
				WHERE teachers.id = class_subjects.teacher_id
				AND class_id = '$class_id'
				AND subject_id = '$subject_id'";

		#echo $sql; exit;		
		if (!$this->mysql->execute_query($sql))
		    echo get_class($this)."-".$this->mysql->error;
		else {
		    $rs = $this->mysql->fetch($this->mysql->result);
		    
		    return $rs[0]['name'];
		}
	}


	function get_timetables() {

	}

	function get_timetable($class_id) {

	}
}

?>
