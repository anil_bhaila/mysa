<?php

/**
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class teacher extends Model
{
	var $table_name = "teachers";
	
	var $primary_key = "id";
		
	/**
	 * gets the subjects a teachers teaches to a class
	 * @param $teacher_id
	 * @param $class_id
	 * @return unknown_type
	 */
	function get_subjects($teacher_id,$class_id)
	{
		/*
		 * To get the subjects taught by a teacher we need to join two tables
		 * it is important to note that a teacher can teach many subjects to many classes
		 */
		$sql = "SELECT subjects.subject_id, name 
				FROM subjects, class_subjects 
				WHERE subjects.subject_id = class_subjects.subject_id 
				and class_id = '$class_id' 
				and teacher_id = '$teacher_id' 
				ORDER BY name";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        	return $this->mysql->fetch($this->mysql->result);
		
	}
	
	/**
	 * Gets the classes taught by a teacher
	 * @param $teacher_id - teacher's id
	 * @return Array
	 */
	function get_classes($teacher_id)
	{
		$sql = "SELECT DISTINCT class_id, firstname, lastname 
				FROM class_subjects,teachers 
				WHERE class_subjects.teacher_id = teachers.id
				AND class_subjects.teacher_id = '$teacher_id' 
				ORDER BY class_id";
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        	return $this->mysql->fetch($this->mysql->result);
	}
	
	
	/**
	 * Gets subjects taught by a teacher
	 * @param $teacher_id
	 * @return unknown_type
	 */
	function get_subjects_taught($teacher_id)
	{
		$sql = "SELECT subjects.name, class_subjects.class_id
				FROM teachers, subjects, class_subjects
				WHERE teachers.id = class_subjects.teacher_id
				AND class_subjects.subject_id = subjects.subject_id
				AND teachers.id = '$teacher_id'
				ORDER BY class_subjects.class_id
				";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        	return $this->mysql->fetch($this->mysql->result);
	}
	
	static function get_total_teachers()
    {
    	$tch = new teacher();
    	$row = $tch->select_all(array("teacher_id"),null);
    	return count($row);
    }
    
    function reset_password($teacher_id)
    {
    	$password['password'] = md5('password');
    	$this->update($teacher_id,$password);
    }
    
	function search($keywords)
	{
		$search_results = array();
		//search keywords against student id, firstname and lastname
		foreach($keywords as $k => $v)
		{
			$sql = "SELECT * FROM `teachers` 
					WHERE teacher_id LIKE '$v%' 
					or firstname LIKE '$v%'
					or lastname LIKE '$v%'";
			
			//echo $sql; echo "<br>";
			if (!$this->mysql->execute_query($sql))
			{
            	echo get_class($this)."-".$this->mysql->error;
			}
        	else
        	{
            	while($row = @mysql_fetch_assoc($this->mysql->result))
                {
                    if(!in_array($row,$search_results))    
                		$search_results[] = $row;
                }
            
        	}
		}//end foreach
		//print_r($search_results);
		return $search_results;
	}
	
}

?>