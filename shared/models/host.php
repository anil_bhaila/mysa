<?php
class host
{
	var $subdomain;
	var $school_name;
	
	
	function get_subdomain()
	{
		$host = $_SERVER['HTTP_HOST'];
		if(ENV == "dev")
		{
			$pattern = "/^([a-zA-Z0-9]+)\.mysa\.com$/";
		}
		elseif(ENV == "live")
		{
			$pattern = "/^([a-zA-Z0-9]+)\.myschoolassist\.com$/";
		}
		
		preg_match($pattern, $host,$match);
		
		if(sizeof($match) > 0)
		{
			return $match[1];
		}
		else
		{
			return null; //there is no subdomain
		}
	}
	
	function is_with_subdomain()
	{
		$host = new host();
		if(!is_null($host->get_subdomain()))
		{
			//this is a hack to make the www subdomain look like their no subdomain
			//so we can redirect to the website directory ;)			
			if(strtolower($host->get_subdomain()) == 'www')	
				return false;
			else
			{		
				$this->subdomain = $host->get_subdomain();
				return true;
			}
		}
		else
		{
			#die("Error: No subdomain. Cannot access myschoolassist");
			return false;
		}
	}
	
	function validate_subdomain()
	{
		if(ENV == "dev")
		{
			$dbname = "mysa";
			$hostname = "localhost";
			$username = "root";
			$password = "";
		}
		
		if(ENV == "live")
		{
			$dbname = "mysa";
			$hostname = "myschoolassist.ipagemysql.com";
			$username = "fobilow";
			$password = "hongkong";
		}
		
		$query = "SELECT `school_name` FROM accounts WHERE url='{$this->subdomain}'";
		
		$con = @mysql_connect($hostname,$username,$password) or die("could not connect to MySQL server");
	
		@mysql_select_db($dbname,$con) or die("could not select database - ".$dbname);
		
		$result = mysql_query($query,$con);
		
		while($row = mysql_fetch_assoc($result))
    	{
    	$data[] = $row;
    	}
        
		$this->school_name = $data[0]['school_name'];
		
		if(mysql_affected_rows($con) == 1)
				return true;
			else
				return false;
		
	}
	
	static function start_app()
	{
		if(SUBDOMAIN == "on")
		{
			//echo "on"; //exit();
			$self = new host();
			if($self->is_with_subdomain())
			{
				if($self->validate_subdomain())
				{
                    require_once(ROOTPATH."/shared/dbconfig/{$self->subdomain}_dbConfig.php");
					return $self->school_name;
				}
				else
				{
					//account has been deactivated or does not exist. If you feel there has been
					//a mistake somewhere please send us a message. To resolve this problem, you have to renew
					//your account.
					header("Location: ../index.php");
					//echo "bad subdomain";
				}
			}
		}
		elseif(SUBDOMAIN == "off")
		{
			//echo "off"; //exit();
			require_once(ROOTPATH."/shared/dbconfig/dbConfig.php");
			return SCHOOLNAME;
		}
			
		
		
	}
	
	/**
	 * This function loads the database configuration for the school acessing thirdeye
	 * Used to be start_app was renamed for v1.1 for better API
	 * @return unknown_type
	 */
	static function load_db()
	{
		return host::start_app();
	}
}
?>
