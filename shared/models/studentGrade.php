<?php
/**
 * @author FO
 *
 */
class studentGrade extends Model
{
	var $table_name = "grades";


	function update_grades($post)
	{
    	foreach($post as $session_id => $grades)
    	{
    		if($session_id != "Submit")
    		{
	    		$grades['session_id'] = $session_id;
	    		$this->update($grades);
    		}
    	}
	}

	function update($post)
	{
		$post = $this->filter_input($post);

		$session_id = $post['session_id'];
		$subject_id = $post['subject_id'];

		$qb = new queryBuilder();
        $qb->set_table_name('grades');
        $qb->set_type("UPDATE");
        $qb->set_where("WHERE session_id = '$session_id' AND subject_id = '$subject_id' ");
        $qb->prepare_data($post);
        $sql = $qb->build_query();

        //echo $sql; return;

        if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
	}

	private function get_grades($class_id, $subject_id, $session, $term)
	{
		$session_obj = new session();

		$asmt = new assesment();
		$asmt_string = $asmt->assesment_string();
		if($asmt_string != '')
		{
			$sql = "SELECT grades.session_id, subject_id, ($asmt_string) as total
				FROM grades, student_class
				WHERE grades.session_id = student_class.session_id
				AND student_class.class_id = '$class_id'
				AND student_class.session = '$session'
				AND student_class.term = '$term'
				AND grades.subject_id = '$subject_id'
				ORDER BY total DESC;
				";

			//echo $sql;
			if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	            return $this->mysql->fetch($this->mysql->result);
		}
		else
		{
			return array(); //return an empty array
		}
	}

	function get_student_average_total($class, $session, $term)
	{
		$asmt = new assesment();
		$asmt_string = $asmt->assesment_string();

		if($asmt_string != '')
		{
			$sql = "SELECT student_class.session_id, AVG($asmt_string) as total
				FROM student_class
				LEFT OUTER JOIN grades ON grades.session_id = student_class.session_id
				WHERE student_class.class_id = '$class'
				AND student_class.session = '$session'
				AND student_class.term = '$term'
				GROUP BY grades.session_id
				ORDER BY total DESC
				";
			//echo $sql;
			if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	            return $this->mysql->fetch($this->mysql->result);
		}
		else
		{
			return array();
		}
	}


	function position_in_subject($class, $session, $term)
	{
		//get all subject in a given
		$class_obj = new schoolClass();
		$subjects = $class_obj->get_subjects($class);
		//for each subject,
		for($i = 0; $i < count($subjects); $i++)
		{
			//get student grades and compute their position
			$grades = $this->get_grades($class,$subjects[$i]['subject_id'],$session,$term);
			if(count($grades) > 0)
			{
				$positions = $this->compute_position($grades);
				//update their position
				$this->update_subject_position($positions, $subjects[$i]['subject_id']);
			}
		
		}
	}

	function position_in_class($class, $session, $term)
	{
		//echo "position_in_class";
		//get overall average total
		$avg_total = $this->get_student_average_total($class, $session, $term);

		if(count($avg_total) > 0)
        {
            //compute their position
            $positions = $this->compute_position($avg_total);
            //update their position
            $this->update_class_position($positions);
        }
              
	}



	private function compute_position($scores)
	{
		//print_r($scores);  exit;
		$next = null; //there is no next
		$previous = null; // there is no previous

		for($i = 0; $i < count($scores); $i++)
		{
			if($i == 0)
			{
				//must be 1st position, since array is already sorted
				$position = 1;
				$previous = $position;
				$next = $position + 1;

			}
			else
			{
				if($scores[$i]['total'] == $scores[$i-1]['total'])
				{
					$position = $previous;
				}
				else
				{
					$position = $next;
					$previous = $position;
				}

				$next++;
			}

			$session_id = $scores[$i]['session_id'];
			$pos[$session_id] = $position;

		}

		//print_r($pos);
		return $pos;
	}

	function update_subject_position($positions, $subject_id)
	{
		foreach($positions as $session_id => $position)
		{
			$sql = "UPDATE grades SET `position` = '$position'
					WHERE session_id = '$session_id'
					AND subject_id = '$subject_id'";
			//echo $sql;
			if (!$this->mysql->execute_query($sql))
            	echo get_class($this)."-".$this->mysql->error;
		}
	}

	function update_class_position($positions)
	{
		foreach($positions as $session_id => $position)
		{
			$sql = "UPDATE student_class SET `class_position` = '$position'
					WHERE session_id = '$session_id'";
			//echo $sql;
			if (!$this->mysql->execute_query($sql))
            	echo get_class($this)."-".$this->mysql->error;
		}
	}

	/**
	 * @param int $position_number
	 * @return string
	 * converts 1 = 1st, 2 = 2nd, 3 = 3rd, 4 = 4th etc.
	 */
	function position_text($position_number)
	{
		$r = $position_number % 10;
		if($position_number >= 10 and $position_number <=20)
		{
			return $position_number."th";
		}
		else
		{
			if($r == 1)
				return $position_number."st";

			elseif($r == 2)
				return $position_number."nd";

			elseif($r == 3)
				return $position_number."rd";
			else
				return $position_number."th";
		}
	}

}

?>