<?php
class notice extends Model
{
	var $table_name = 'notices';
	
	var $primary_key = 'id';
	
	function select_all(array $select_column = null, $start = null,$where = null)
	{
		$qb = new queryBuilder();
		$qb->set_table_name($this->get_table_name());
		$qb->set_type("SELECT");
		if (!is_null($select_column)) {
			$qb->set_field($select_column);
		}
		if (!is_null($start)) {
			$rpp = $this->get_record_per_page();
			$qb->set_limit("LIMIT $start,$rpp");
		}
		if (!is_null($where)) {
			$rpp = $this->get_record_per_page();
			$qb->set_where($where);
		}
			$qb->set_order("ORDER BY date_posted DESC");

		$sql = $qb->build_query();
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
			echo get_class($this)."-".$this->mysql->error;
		else
			return $this->mysql->fetch($this->mysql->result);
	}
}

?>