<?php
/**
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class subject extends Model
{
	var $table_name = "subjects";
	
	var $primary_key = "subject_id";
	

	function select_all()
	{
		$qb = new queryBuilder();
		$qb->set_table_name($this->get_table_name());
		$qb->set_type("SELECT");
		$qb->set_order("ORDER BY name");
		
		$sql = $qb->build_query();
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
			echo get_class($this)."-".$this->mysql->error;
		else
			return $this->mysql->fetch($this->mysql->result);
	}
	
	function insert($data)
	{
		$data = $this->filter_input($data);
		$qb = new queryBuilder();
		$qb->set_table_name($this->get_table_name());
		$qb->set_type("INSERT");
		$qb->prepare_data($data);
		$sql = $qb->build_query();
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
		    echo get_class($this)."-".$this->mysql->error;
		else
			return $this->mysql->get_last_id();
	}
	
	function delete($subject_id)
	{
		parent::delete($subject_id);
		$sql = "DELETE from `class_subjects` WHERE subject_id = $subject_id";
		if(!$this->mysql->execute_query($sql))
   		{
   			echo "Query failed to execute<br>{$this->mysql->error}<br>$sql";
   			exit();
   		}
	}
    
	function change_teacher($subject_id, $class_id, $teacher_id)
	{
		$sql = "INSERT INTO `class_subjects` (`class_id`, `subject_id`, `teacher_id`) 
				VALUES ('$class_id','$subject_id','$teacher_id')
				ON DUPLICATE KEY UPDATE class_id = '$class_id', subject_id = '$subject_id',teacher_id = '$teacher_id'";
		if(!$this->mysql->execute_query($sql))
		{
			echo "Query failed to execute<br>{$this->mysql->error}<br>$sql";
			exit();
		}
	}
    
	function get_last_id()
	{
		$sql = "SELECT MAX(subject_id)as id FROM subjects";
		if (!$this->mysql->execute_query($sql))
		    echo get_class($this)."-".$this->mysql->error;
		else
		{
			$id = $this->mysql->fetch($this->mysql->result);
			return $id[0]['id'];
		}

	}
	
	function get_teachers($subject_id)
	{
		$sql = "SELECT teacher_id, class_id
				FROM class_subjects
				WHERE subject_id = '$subject_id'";
		if (!$this->mysql->execute_query($sql))
			echo get_class($this)."-".$this->mysql->error;
		else
		{
		$rs = $this->mysql->fetch($this->mysql->result);
			for($i = 0; $i < count($rs); $i++)
			{
				$teachers[$rs[$i]['class_id']] = $rs[$i]['teacher_id'];
			}
			return $teachers;
		}
	}    

}
?>
