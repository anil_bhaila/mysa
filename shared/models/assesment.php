<?php
/**
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class assesment extends Model
{
	var $table_name = "assesments";
	
	var $primary_key = "assesment_id";
	
	var $grade_table = "grades";
	
	
	function insert($data)
	{
		if(isset($data['assesment_id']))
		{
			parent::update($data['assesment_id'],$data);
		}
		else
		{
			$real_name = $this->real_name($data['assesment_name']);
			$data['real_name'] = $real_name;
			parent::insert($data);
			$this->create_new_column($real_name);
		}
	}
	
	
	function select_active()
	{
		$sql = "SELECT *
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
            return $this->mysql->fetch($this->mysql->result);
	}
	
	/**
	 * Creates new column in the grades table to hold scores of the newly created assessment.
	 * @param $column_name
	 * @return unknown_type
	 */
	private function create_new_column($column_name)
	{
		$column_name = mysql_escape_string($column_name);
		$column_name = strtolower($column_name);
		$sql = "ALTER TABLE `$this->grade_table` ADD `$column_name` VARCHAR( 3 ) NOT NULL DEFAULT  '--'";
		//$sql = "ALTER TABLE `$this->grade_table` ADD `$column_name` INT NOT NULL";
		//ALTER TABLE  `grades` ADD  `dummy` VARCHAR( 3 ) NOT NULL DEFAULT  '--'
		
		if(!$this->mysql->execute_query($sql))
			echo $this->mysql->error;
		else
			return true;
	}
	
	
	function assesment_string()
	{
		$ass_string = "";
		
		$sql = "SELECT real_name
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        {
            $active_ass = $this->mysql->fetch($this->mysql->result);
            for($i = 0; $i < count($active_ass); $i++)
            {
            	$ass_string .= "CAST(".$active_ass[$i]['real_name']." AS UNSIGNED)+";	
            }
            //take out the extra + at the end of the string
            $minus = strlen($ass_string) - 1;
            $ass_string = substr($ass_string, 0,$minus);
            return $ass_string;
        }
	}
	
	function assement_header()
	{
		$header = "";
		
		$sql = "SELECT assesment_name
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        {
            $active_ass = $this->mysql->fetch($this->mysql->result);
            for($i = 0; $i < count($active_ass); $i++)
            {
            	$header .= "<td>".$active_ass[$i]['assesment_name']."</td>";	
            }
            
            return $header;
        }
	}
	
	
	function assesment_scores($row)
	{
		$scores = "";
		
		$sql = "SELECT real_name
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        {
            $active_ass = $this->mysql->fetch($this->mysql->result);
            for($i = 0; $i < count($active_ass); $i++)
            {
            	$name = $active_ass[$i]['real_name'];
				$scores .= "<td>".$row[$name]."</td>";	
            }
            
            return $scores;
        }
	}
	
	function edit_assesment_scores($row)
	{
		$scores = "";
		
		$sql = "SELECT real_name
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        {
            $active_ass = $this->mysql->fetch($this->mysql->result);
            for($i = 0; $i < count($active_ass); $i++)
            {
            	$name = $active_ass[$i]['real_name'];
            	//$txtname = 
				$scores .= "<td><input type=text name=$name value=".$row[$name]." size=5 /></td>";	
            }
            
            return $scores;
        }
	}
	
	function inline_edit_scores($row)
	{
		$scores = "";
		
		$sql = "SELECT real_name
				FROM $this->table_name
				WHERE status = 'Active'";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
        {
            $active_ass = $this->mysql->fetch($this->mysql->result);
            for($i = 0; $i < count($active_ass); $i++)
            {
            	$name = $active_ass[$i]['real_name'];
            	$txtname = $row['session_id']."[".$name."]";
				$scores .= "<td><input type=\"text\" name=\"{$txtname}\" value=\"{$row[$name]}\" size=\"5\" /></td>";	
            }
            
            return $scores;
        }
	}
	
	
	function real_name($string)
	{
		$valid_chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		//trim
		$string = trim($string);
		
		$new_string = "";

		//For every character on the string.
		for($i = 0; $i < strlen($string); $i++)
		{
			$char = substr($string, $i, 1);
			$position = strpos($valid_chars, $char);
			if($position !== false)
			{
					$new_string .= $char;
			}
		}
		
		$new_string = strtolower($new_string);

		return $new_string;
	}
	
	
}

?>