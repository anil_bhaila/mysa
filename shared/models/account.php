<?php
class account extends Model
{
	var $table_name = "accounts";
	var $primary_key = "url";
	
	

	function insert($datax,$name)
	{
		//$this->create_database($name);
		
		$this->create_config_file($name);
	
   		$this->mysql->select();
		parent::insert($datax);

	}
	
	function create_database($name)
	{
		$dbcon = new dbConfig();
		$dbcon->dbname = $name;
		$dbcon->hostname = "localhost";
		$dbcon->username = "root";
		$dbcon->password = "";
		
		$sql = "CREATE DATABASE `$name`";
		
		$setup = new mySQLConnection($dbcon);
		$setup->execute_query($sql);
		
		$setup->select();
		
		$dbfile = file_get_contents("thirdeyedb.sql");
		$setup_query = explode(';',$dbfile);
		for($i = 0; $i < count($setup_query); $i++)
		{
			$setup->execute_query($setup_query[$i]);	
		}
		
		unset($setup);
	}
	
	function create_config_file($name)
	{
		$config_txt = '<?php
		class dbConfig
		{
			var $hostname = \'localhost\';   //hostname for db connection
			var $username = \'root\';  // username for db connection
			var $password = \'\';  //password for db conection
			var $dbname = \''.$name.'\';  //database name
		}
?>	
		';
		
		$config_file = "../dbconfig/{$name}_dbConfig.php";
		file_put_contents($config_file,$config_txt);
	}
}

?>