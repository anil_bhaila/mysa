// JavaScript Document

function checkAll(name,obj)
{
	checkList  = document.getElementsByName(name);
		if(obj.checked)
		{
			for(i = 0; i < checkList.length; i++)
			{
			checkList[i].checked = true;
			/*hilite(checkList[i]);*/
			}
		}
		else
		{
			for(i = 0; i < checkList.length; i++)
			{
			checkList[i].checked = false;
			/*hilite(checkList[i]);*/
			}
		}
		
		toggle_edit_delete(name,'edit','delete');
}

function getStudents()
{

	var cls = document.getElementById('report-class');
	var session = document.getElementById('report-session');
	var term = document.getElementById('report-term'); 
	
	var class_id = cls.options[cls.selectedIndex].text;
	var session_text = session.options[session.selectedIndex].text;
	var term_text = term.options[term.selectedIndex].text; 
	
	var url = "get_students.php?class="+class_id+"&session="+session_text+"&term="+term_text;
	$.get(url, function(data){
	$("#studentList").attr("innerHTML", data);
	});
}

function show(obj)
{
	var class_id = obj.options[obj.selectedIndex].value; 
	var url = "sbj.php?class="+class_id;
	$.get(url, function(data){
	$("#subjects").attr("innerHTML", data);
	});   
}

function populateElement(selector, defvalue) 
{
	    $(selector).each(
			function() 
			{
				if($.trim(this.value) == "") {
					this.value = defvalue;
				}
			}
		);
		
	  
	    $(selector).focus(function() {
	        if(this.value == defvalue) {
	            this.value = "";
	        }
	    });
	    
	    $(selector).blur(function() {
	        if($.trim(this.value) == "") {
	            this.value = defvalue;
	        }
	    });
}

function generateReport()
{
	session = document.getElementById('report-session').value;
	term = document.getElementById('report-term').value;
	class = document.getElementById('report-class').value;
	
	url = 'report_generator.php?class='+class+'&session='+session+'&term='+term;
	window.open(url,'report','status,menubar,scrollbars');
}