<?php
//****************************************************************************
//**Name: QueryBuilder.php
//**Author: Okechukwu Ugwu
//**Description: This class helps you build SQL queries of different types
//**Date: 3/3/2008
//****************************************************************************

class queryBuilder
{
        var $data;
        var $field;
        var $tableName;

        var $where;
        var $order;
        var $group;
        var $limit;
        var $queryString;
        var $error;


        private function put_quotes1($field)
        {
                $field = trim($field);
                $field = "`".$field."`";
                return $field;
        }

        private function put_quotes2($field)
        {
                $field = trim($field);
                $field = "'".$field."'";
                return $field;
        }
        
        function set_type($type)
        {
                $this->type = $type;
        }
        
        function set_data($data)
        {
                $this->data = $data;
        }
        
        function set_field($field = null)
        {
                $this->field = $field;
        }
        
        function set_where($where)
        {
                $this->where = $where;
        }
        
        function set_limit($limit)
        {
                $this->limit = $limit;
        }
        
        function set_order($order)
        {
                $this->order = $order;
        }
		
		function set_table_name($name)
		{
			$this->tableName = $name;
		}
        
        function prepare_data($data)
        {
                if(is_array($data))
                {
                        foreach($data as $k => $v)
                        {
                                $this->field[] = $k; //setting the column names
                                $this->data[] = $v; // setting the values
                        }
                }

        }
		 
        function build_query()
        {
             switch($this->type)
             {
                case 'INSERT':
                                $table_name = $this->put_quotes1($this->tableName);
                                $this->queryString = "INSERT INTO ".$table_name." (";
                                $noFields = count($this->field);
                                $noData = count($this->data);
                                
                                if($noFields > 0 && $noData > 0)
                                {
                                        for($i = 0; $i < $noFields; $i++)
                                        {
                                                if($i == ($noFields- 1))
                                                $this->queryString .= $this->put_quotes1($this->field[$i]).')';
                                                else
                                                $this->queryString .= $this->put_quotes1($this->field[$i]).',';
                                        }

                                        $this->queryString.= " VALUES (";


                                        for($i = 0; $i < $noData; $i++)
                                        {
                                                if($i == ($noData -1))
                                                $this->queryString .= $this->put_quotes2($this->data[$i]).');';
                                                else
                                                $this->queryString .= $this->put_quotes2($this->data[$i]).',';
                                        }
                                        
                                }
                                else
                                {
                                        $this->error = "No column name or data was supplied";
                                }

                                break;

                case 'SELECT':
                                $table_name = $this->put_quotes1($this->tableName);

                                $this->queryString = "SELECT ";
                                if(!isset($this->field) || is_null($this->field))
                                        $this->queryString .= "* ";
                                else{
                                        $noFields = count($this->field); //no of fields in table
                                        for($i = 0; $i < $noFields; $i++)
                                        {
                                                if($i == ($noFields- 1))  // if on the last field
                                                        $this->queryString .= $this->put_quotes1($this->field[$i]).' ';
                                                else
                                                        $this->queryString .= $this->put_quotes1($this->field[$i]).',';
                                        }
                                    }

                                $this->queryString .= "FROM ".$table_name;

                                if(isset($this->where))
                                        $this->queryString .= " ".$this->where;
                                
                                if(isset($this->order))
                                $this->queryString .= " ".$this->order;
                                
                                if(isset($this->limit))
                                $this->queryString .= " ".$this->limit;

                                else
                                        $this->queryString .= ";";

                              break;


                case 'UPDATE':

                                $table_name = $this->put_quotes1($this->tableName);

                                $this->queryString = "UPDATE ". $table_name. " SET ";


                                $noFields = count($this->field); //no of fields in table

                                if(is_array($this->field) && is_array($this->data) && isset($this->where))
                                {
                                        for($i = 0; $i < $noFields; $i++)
                                        {
                                                if($i == ($noFields -1))
                                                $this->queryString .= $this->put_quotes1($this->field[$i])." = ". $this->put_quotes2($this->data[$i]).' ';
                                                else
                                                $this->queryString .= $this->put_quotes1($this->field[$i])." = ". $this->put_quotes2($this->data[$i]).',';

                                        }

                                        $this->queryString .= " ".$this->where.";";
                                }
                                else
                                {
                                        $this->error = "Cannot build query. One of the following was not set";
                                }

                                break;


                case 'DELETE':
                                $table_name = $this->put_quotes1($this->tableName);

                                $this->queryString = "DELETE FROM ".$table_name;

                                if(isset($this->where))
                                {
                                        $this->queryString .= " ".$this->where.";";
                                }
                                else
                                {
                                        $this->error = "Connot build. No condition was set";
                                }
                                break;
          }
       return $this->queryString;
	   }
       
}

?>
