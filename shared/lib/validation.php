<?php
class validation extends validationTest
{
    
    var $unvalidated_data; //data to be validated
    
  
    function validation($post)
    {
         $this->unvalidated_data = $post;
    }
    
   
   
    function is_empty($field)
    {
    	if(is_array($field))
    	{
	    	for($i = 0; $i < count($field); $i++)
	    	{
	    		$key = $field[$i];
	    		if(array_key_exists($key,$this->unvalidated_data))
	    			$this->test_empty($key,$this->unvalidated_data[$key]);
	    		
	    	}
    	}
    	else
    	{
    		$this->test_empty($field,$this->unvalidated_data[$field]);
    	}
    }
    
    
    //check for only aplhabets, no numbers or characters
    function is_text($field)
    {
        if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $field[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_text($key,$this->unvalidated_data[$key]);
        	}
        }
        else
        {
        	$this->test_text($field,$this->unvalidated_data[$field]);
        }
        
    }
    
    
    
    function is_number($field)
    {
    	if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $field[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_number($key,$this->unvalidated_data[$key]);
        	}
        }
        else
        {
        	$this->test_number($field,$this->unvalidated_data[$field]);
        }
    }
    
    function is_email($field)
    {
    	if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $field[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_email($key,$this->unvalidated_data[$key]);
        	}
        }
        else
        {
        	$this->test_email($field,$this->unvalidated_data[$field]);
        }
    }
    
    function is_date($field)
    {
    	if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $field[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_date($key,$this->unvalidated_data[$key]);
        	}
        }
        else
        {
        	$this->test_date($field,$this->unvalidated_data[$field]);
        }
    }
    
	function is_dob($field)
    {
    	if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $field[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_dob($key,$this->unvalidated_data[$key]);
        	}
        }
        else
        {
        	$this->test_dob($field,$this->unvalidated_data[$field]);
        }
    }
    
    function is_password($field,$length = 6,$must_contain_numbers = true)
    {
    	if(is_array($field))
        {
        	for($i = 0; $i < count($field); $i++)
        	{
        		$key = $password[$i];
        		if(array_key_exists($key,$this->unvalidated_data))
        			$this->test_password($key,$this->unvalidated_data[$key],$length,$must_contain_numbers);
        	}
        }
        else
        {
        	$this->test_password($field,$this->unvalidated_data[$field],$length,$must_contain_numbers);
        }
    }
    
    //use this function to test if two fields contain the same value. e.g confirming passwords or email
    function is_same($field_array)
    {
    	
    	$k = count($field_array);
    	if($k > 1)
    	{
    		for($i = 0; $i < $k-1; $i++)
	    	{
		    		echo $field_array[$i],":",$field_array[$i+1],"<br>";
	    			if(strcmp($field_array[$i],$field_array[$i+1]) != 0)
			    	{
			    		$this->error_msg['password'] = "fields do not match";
			    		return;
			    	}
	    	}
    	}
    	else
    		die("cannot compare a single value - size of array should be atleast 2");
    }
    
    
    function get_error_messages()
    {
    	return $this->error_msg;
    }
    
    //methods that allow you customize error messages
    
    function set_message($field,$message)
    {
    	$this->error_msg[$field] = $message;
    }
    		

}

?>