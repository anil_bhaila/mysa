<?php
//**************************************************************************
//**Name: Model.php
//**Author: Okechukwu Ugwu
//**Description: this class  is a base class for all models
//**Date: 3/3/2008
//**************************************************************************

class Model
{
	    var $fields;

	    var $table_name;
	    
	    var $per_page = 10;
	    
	    var $mysql;
	
	    function __construct($table_name=null)
	    {
	        $this->mysql = new mySQLConnection();
	        $this->mysql->select();

	        if(!is_null($table_name))
	        {
	        	$this->table_name = $table_name;
	        }
	    }
	    
	    function get_record_per_page()
	    {
	        return $this->per_page;
	    }
	    
	    function set_record_per_page($no)
	    {
	        $this->per_page = $no;
	    }
		
		static function page_links($data,$per_page, $start)
		{
			$total_records = count($data);
			//first determine how many pages we will have
			$pages = (int)($total_records/$per_page);
			if($total_records%$per_page > 0){
				$pages  += 1;
			}
			
			$paging = '';
			
			if($pages > 1){
				$paging .= '<ul>';
				for($i = 0; $i < $pages; $i++){
					$s = $per_page * $i;
					if($s == $start){
						$paging .= '<li><a id="active" href="?start='.$s.'">'.($i+1).'</a></li>';
					}
					else{
						$paging .= '<li><a href="?start='.$s.'">'.($i+1).'</a></li>';
					}
				}
				$paging .= '</ul>';
				
			}
			
			return $paging;
			
		}
	
	
	    function get_table_name()
	    {
	        return $this->table_name;
	    }
	
	    function insert($data)
	    {
	    	$filtered_data = $this->filter_input($data);
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("INSERT");
	        $qb->prepare_data($filtered_data);
	        $sql = $qb->build_query();
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	        	return $this->mysql->get_last_id();
	    }
	
	    function update($id, $data)
	    {
	        $data = $this->filter_input($data);
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("UPDATE");
	        $qb->set_where("WHERE $this->primary_key = '$id'");
	        $qb->prepare_data($data);
	        $sql = $qb->build_query();
	        //echo $sql; exit;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	    }
	
	    function delete($id)
	    {
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("DELETE");
	        $qb->set_where("WHERE $this->primary_key = '$id'");
	        $sql = $qb->build_query();
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	    }
	
	    function select($id, array $select_column = null)
	    {
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("SELECT");
	        if (!is_null($select_column)) {
	            $qb->set_field($select_column);
	        }
	        $qb->set_where("WHERE $this->primary_key = '$id'");
	        $sql = $qb->build_query();
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	            return $this->mysql->fetch($this->mysql->result);
	    }
	
	    function select_all(array $select_column = null, $start = null,$where = null)
	    {
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("SELECT");
	        if (!is_null($select_column)) {
	            $qb->set_field($select_column);
	        }
	        if (!is_null($start)) {
	            $rpp = $this->get_record_per_page();
	            $qb->set_limit("LIMIT $start,$rpp");
	        }
	        if (!is_null($where)) {
	            $rpp = $this->get_record_per_page();
	            $qb->set_where($where);
	        }
	
	        $sql = $qb->build_query();
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	            return $this->mysql->fetch($this->mysql->result);
	    }
	
	    function get_total_rows()
	    {
	        $table = $this->get_table_name();
	        $sql = "SELECT count(*) AS total FROM `$table`";
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else {
	            $data = $this->mysql->fetch($this->mysql->result);
	            return $data[0]['total'];
	        }
	    }
	
	    function search($data)
	    {
	        foreach ($data as $k => $v) {
	            $by = $k;
	            $for = $v;
	        }
	        $qb = new queryBuilder();
	        $qb->set_table_name($this->get_table_name());
	        $qb->set_type("SELECT");
	        $qb->set_where("WHERE $by LIKE '$for%'");
	        $sql = $qb->build_query();
	        //echo $sql;
	        $this->mysql->execute_query($sql);
	        return $this->mysql->fetch($this->mysql->result);
	    }
	    
	    function get_fields()
	    {
	        $sql = "SHOW COLUMNS FROM {$this->get_table_name()}" ;
	        //echo $sql;
	        if (!$this->mysql->execute_query($sql))
	        {
	            echo get_class($this)."-".$this->mysql->error;
	            exit;
	        }
	        else
	        {
	            $data = $this->mysql->fetch($this->mysql->result);
	            for($i = 0; $i < count($data); $i++)
	            {
	                $fields[] = $data[$i]['Field'];
	            }
	            $this->fields = $fields;
	        }
	    }
	
	    function filter_input($unfiltered)
	    {  
	    	$this->get_fields();
	        foreach ($unfiltered as $k => $v) {
	            if (in_array($k, $this->fields)) {
	                $filtered[$k] = mysql_real_escape_string($v,$this->mysql->conn);
	            }
	        }
	        return $filtered;
	    }
    
    
        
}

?>
