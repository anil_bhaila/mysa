<?php
//****************************************************************************
//**Name: MySQL.php
//**Author: Okechukwu Ugwu
//**Description: This class wraps some of mysql's functions and provides an
//**             interface to access MYSQL database
//**Date: 3/3/2008
//****************************************************************************

class mySQLConnection
{
        var $conn;
        var $result;
	var $error;
	var $config;
        
        
        //constructor loads db config
        function mySQLConnection($dbconfig = null)
        {
                if(is_null($dbconfig))
                {
        	       $this->config = new dbConfig();
                }
        	if(is_a($dbconfig,"dbConfig"))
        	{
        		$this->config = $dbconfig;
        	}
                	
                $this->connect();
        }

        function connect()
        {
                $con = @mysql_pconnect($this->config->hostname,$this->config->username,$this->config->password);
                if(!$con)
                {
                $this->error = "cannot connect to MySQL server";
                return false;
                }
                else
                {
                $this->conn = $con;
                return true;
                }
        }

        function select()
        {
                //selecting a database;
                if($this->connect())
                @mysql_select_db($this->config->dbname,$this->conn) or die("could not select database - ".$this->config->dbname);
                
                //echo "selected db - {$this->config->dbname}<br>";
        }
		
	function execute_query($query)
        {
        	if($result = mysql_query($query,$this->conn))
                {
                    $this->result = $result;
                    return true;
                }
                else
                {
                     
                        $this->error = "QUERY ERROR: ".mysql_error($this->conn);
                     return false;
                }
        }

        function fetch($rs)
        {
                while($row = @mysql_fetch_assoc($rs))
                {
                        $data[] = $row;
                }
                return $data;
        }

        function rows_affected()
        {
                 return  mysql_affected_rows($this->conn);
        }

        function close()
        {
                mysql_close($this->conn);
        }
		
	function get_last_id()
	{
		return mysql_insert_id($this->conn);
	}

}

?>
