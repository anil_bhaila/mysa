<?php
class validationTest
{
	var $error_msg;
    
	/**
	 * use this function to check if any errors were found during validation
	 * @return bool
	 */
	function error_found()
	{
		if(sizeof($this->error_msg) > 0)
			return true;
		else
			return false;
	}
    
	protected function test_empty($field,$data)
	{
		if(empty($data))
        {
        	$this->error_msg[$field] = " You cannot leave this field empty";
            return;
        }
	}
	
	protected function test_text($field,$data)
	{
		if(empty($data))
		{
		    $this->error_msg[$field] = " You cannot leave this field empty";
		    return;
		}
		if(!ctype_alpha($data))
		{
		    $this->error_msg[$field] = "should only contain alphabets";
		    return;
		}
	}
    
	protected function test_number($field,$data)
	{
		if(empty($data))
		{
		    $this->error_msg[$field] = "please enter a number";
		    return;
		}
		elseif(!empty($data) and !ctype_digit($data))
		{
		    $this->error_msg[$field] = "must contain only numbers";
		    return;
		}
	}
    
	protected function test_email($field,$data)
	{
		if(empty($data))
		{
		    $this->error_msg[$field] = "email field is empty";
		    return;
		}
		if(!ereg('^([a-zA-Z0-9_-]+).([\a-zA-Z0-9_-]+)@([a-zA-Z0-9_-]+)(\.[a-zA-Z0-9_-]+)+$', $data))
		{
		    $this->error_msg[$field] = 'email address is not correct';
		    return;
		}
	}
    
	protected function test_date($field,$data)
	{
		$chkdate = explode('-',$data);

		if(empty($data))
			$this->error_msg[$field] = 'You must select or enter a date';
		elseif(!@checkdate($chkdate[1],$chkdate[2],$chkdate[0]))
			$this->error_msg[$field] = 'the date you supplied is invalid, please make sure date format is yyyy-mm-dd';
		return;
	}
   
	protected function test_dob($field,$data)
	{
	
		$chkdate = explode('-',$data);
		$today = date('Y-m-d');

		if(empty($data))
			$this->error_msg[$field] = 'You must select or enter date of birth';
		elseif(!@checkdate($chkdate[1],$chkdate[2],$chkdate[0]))
			$this->error_msg[$field] = 'An invalid date of birth was entered';
		elseif(@checkdate($chkdate[1],$chkdate[2],$chkdate[0]))
		{
			if(strcmp($today,$data) < 0)
				$this->error_msg[$field] = 'An invalid date of birth was entered';
		}
	}
						
    
	protected function test_password($field,$password,$length,$must_contain_numbers)
	{
		echo "calling test_password<br>";
		if(strlen($password)< $length && $must_contain_numbers && ctype_alnum($password))
		{
	
				$this->error_msg[$field] = "password must be atleast $length characters";
				return;
	
		}
		if(strlen($password)>= $length && $must_contain_numbers && preg_match('/^(\D)+$/', $password))
		{
	
				$this->error_msg[$field] = "password must contain at least a number";
				return;
		
		}
	}
}
