<?php

// this function converts a date in sql format i.e yyyy-mm-dd
// to a human readable format i.e dd\mm\yyyy UK format
function sql_to_uk_date($sqldate)
{
	$date2 = explode('-',$sqldate);

	$date2 = array_reverse($date2);

	$date = join($date2,'/');
	
	return $date;
}

// this function converts a date in sql format i.e yyyy-mm-dd
// to a human readable format i.e mm\dd\yyyy US format
function sql_to_us_date($sqldate)
{
	$date2 = explode('-',$sqldate);
	
	$date = $date2[1]."/".$date2[2]."/".$date2[0];
	
	return $date;
}


/*function mmddyy($date,$sep){};
function ddmmyy($date,$sep){};
*/


function readable_date($date)
{
	return date('l, F d, Y',strtotime($date));
}

function readable_date2($date)
{
	return date('F d, Y',strtotime($date));
}


//this function is used to build dynamic drop down options
//takes an associative array as a parameter
//the key serves as the value of the option and the value as the name of the option
//e.g $arr['lastname'] = 'Last name';
function build_options($searchby)
{
	foreach($searchby as $k => $v)
	{
		if($_POST['by'] == $k)
		$option .= "<option value=$k selected >$v</option>";
		else
		$option .= "<option value=$k >$v</option>";
		
	}
	return $option;
}

/*this function is used give tables rows alternate colors
when put in a loop, different values of $i is supplied to the function
which returns alternate colors*/
function color($i)
{
	if($i%2 == 0)
		//return '#E1E1E1';
		return '#FFFFFF';
	else
		//return '#3399FF';
		//return '#FAFAFA';
		//return '#FFF';
		return '#F1F1F1';
}

/*$rco = new tableDecorator();
$rco->set_colors("color1","color2");
$rco->get_color("row_index");

$dc->dd_mm_yyyy("sql_date_string");*/

function generate_password($length=6,$level=2){

   list($usec, $sec) = explode(' ', microtime());
   srand((float) $sec + ((float) $usec * 100000));

   $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
   $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
   $validchars[3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

   $password  = "";
   $counter   = 0;

   while ($counter < $length) {
     $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level])-1), 1);

     // All character must be different
     if (!strstr($password, $actChar)) {
        $password .= $actChar;
        $counter++;
     }
   }

   return $password;

}

//converts 1 = 1st, 2 = 2nd, 3 = 3rd, 4 = 4th etc.
function to_sequence($number)
{
	$r = $number % 10;	
	if($number >= 10 and $number <=20)
	{
		return $number."th";
	}
	else
	{
		if($r == 1)
			return $number."st";
			
		elseif($r == 2)
			return $number."nd";
			
		elseif($r == 3)
			return $number."rd";
		else	
			return $number."th";
	}
}


function get_keywords()
{

	$search_string = $_GET['search_string'];
	
	$keywords = explode(" ",$search_string);
	if(!in_array($search_string,$keywords))
		array_push($keywords,$search_string);
		
	return $keywords;

}

function session_dropdown()
{
	$year = date('Y');
	$start = $year - 3;
	for($i = $start; $i <= $year; $i++)
	{
		$value = $i.'/'.($i+1);
		$options .= '<option value="'.$value.'">'.$value.'</option>';
	}
	
	echo $options;
}



?>
