<?php
/*
  This class is still very fragile, not considered robust - therefore all parameters
  supplied to the methods must be correct as it has no exception handling. I hope to do this
  in near future, for now i just cant find the patience to work on it.
  
  I'm also looking forward to validating the dimensions of image files, with respect to certain images
  such as banners of website having to be a particular size. 
*/
Class fileUpload
{
  var $files = array();
  var $error = array();
  
  var $rule = array("image" => array("image/jpeg","image/pjpeg","image/png","image/gif"),
                    "pdf" => array("application/pdf"),
  					"word" => array("application/msword"),
  					"pdf/word" => array("application/pdf","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
  					"word/pdf" => array("application/pdf","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
                    "image/flash" => array("application/x-shockwave-flash","image/jpeg","image/pjpeg","image/png","image/gif"),
  					"flash/image" => array("application/x-shockwave-flash","image/jpeg","image/pjpeg","image/png","image/gif"),
                    "flash" => array("application/x-shockwave-flash")
  					//add more file types as u wish
  );
  
  var $ready = false; //specifies if file(s) are ready to be moved to server
  
  //rename variables
  var $rename_path;
  var $rename_oldname;
 
  function fileUpload()
  {
  	if(!isset($_FILES))
  	{
  		die("No files were submitted");
  	}
  }
  
  function add_file($file_field,$file_type,$destination,$rename=false)
  {
  	  $_FILES[$file_field]['form_field'] = $file_field;
      
      $this->rename_path = $destination;
      
      $destination = $destination."".$_FILES[$file_field]['name'];
    
	  $this->files[] = array('field' => $file_field, 'type' => $file_type, 'destination' => $destination, 'rename' => $rename);
  }
    
  
  function get_rule($type)
  {
      if(array_key_exists($type,$this->rule))
            return $this->rule[$type];
      else
      {
            echo "Uknown type - $type";
            exit();
      }
            
  }
  
  function upload()
  {     
      //$this->validate();
      if($this->ready)
      {
            //move files all at once, if no type error occured
            for($i = 0; $i < count($this->files); $i++)
            { 
                    $key = $this->files[$i]['field'];
                    if(!move_uploaded_file($_FILES[$key]['tmp_name'],$this->files[$i]['destination']))
                    {
                    		$this->error[$key] = "Could not upload file due to internal error. Contact your developer";
                    		return false;
                    }
                    
                    //check if files needs to be renamed
                    if(is_bool($this->files[$i]['rename']) && $this->files[$i]['rename'])
                    {
                    	//rename file by generating random name
                    	$this->rename_oldname = $this->files[$i]['destination'];
                    	$newname = $this->rename();
                    	
                    	$this->files[$i]['destination'] = $newname;	
                    }
                    elseif(!is_bool($this->files[$i]['rename']))
                    {
                    	//rename file with name supplied
                    	$this->rename_oldname = $this->files[$i]['destination'];
                    	$newname = $this->rename($this->files[$i]['rename']);
                    	
                    	$this->files[$i]['destination'] = $newname;	
                    }
                    
                    //attaches the destination of the uploaded file to the POST array, you can change this if your form uses GET
                    //like this: $_GET[$key] = $this->files[i]['destination']
                    $_POST[$key] = $this->files[$i]['destination'];
            }
            
            return true;
      }
      else
      {
      		return false;
      }
  }
  
  function validate()
  {
      for($i = 0; $i < count($this->files); $i++)
      {    
          
      	$field = $this->files[$i]['field'];
      	$type = $this->files[$i]['type'];
      	
      	//if file upload was successful  
          if($this->is_ok($_FILES[$field]))
          { 
                //continue with user-defined validation rules
                $type = $this->get_rule($type);
           
                if(in_array($_FILES[$field]['type'],$type))
                {
                    //set ready_to_move flag to true
                    //echo "file is valid. Ready for upload<br>";
                    $this->ready = true;
                }
                else
                {
                    //raise an exception, error has occured
                   $this->ready = false; 
                   $this->error[$field] = "Was expecting a {$this->files[$i]['type']} file, but got {$_FILES[$field]['type']} file";
                }
                
          }
          
          //if file is not the right size (exceeds the ini setting)
          if(!$this->is_right_size($_FILES[$field]))
          {
                  //take note of the error
                  $this->ready = false; 
                  $this->error[$field] = "File is too big";
          }
          
          if($this->is_partial_upload($_FILES[$field]))
          {
                  //take of note of partial error
                  $this->ready = false; 
                  $this->error[$field] = "File was partially uploaded";
          }
          
          if($this->is_empty($_FILES[$field]))
          {
                  //take note of empty file
                  $this->ready = false; 
                  $this->error[$field] = "No file was uploaded, you must upload a file";
          }
                      
      } 
  }         
  
  
  function rename($rename = null)
  {
  	//Use this function to rename a file after it has been uploaded
  	$ext = $this->get_file_extension($this->rename_oldname); // getting the extension of the file
  	if(is_null($rename))
  	{
    	$name = rand(100,9999999999); //generate a random name
    	$new_name = $this->rename_path.''.$name.'.'.$ext; //puts together the full name of file (+ extension)
		if(file_exists($new_name)) //checks whether new name generated already exists
			$this->rename(); //if name exists, generate another name
  	}
    elseif(!is_null($rename))
    {
    	$name = $rename;
		$new_name = $this->rename_path.''.$name.'.'.$ext; //puts together the full name of file (+ extension)
		if(file_exists($new_name))
			unlink($new_name);
	}
	
	
	rename($this->rename_oldname,$new_name); //rename file
	return $new_name; //return new file name to caller
  }
  
  
  private function get_file_extension($file_name)
  {
  	$pos = strrpos($file_name,".");

	$len = strlen($file_name);

	$start = $pos + 1;
	
	if($pos !== false)
	{
		return substr($file_name,$start,$len);
	}
	else
	{
		die("file does not have an extension!");
	}
  }
  

  
  //standard upload validation functions
  function is_ok($file)
  {
      if($file['error'] == 0)
            return true;
      else
            return false;
  }
  
  function is_right_size($file)
  {
      if($file['error'] == 1)
            return false;
      else
            return true;
  }
  
  function is_empty($file)
  {
      if($file['error'] == 4)
            return true;
      else
            return false;
  }
  
  function is_partial_upload($file)
  {
     if($file['error'] == 3)
            return true;
     else
            return false;
  } 
  
  
  

                
}       

/*$fu = new fileUpload();
$fu->add_file('logo','image','/images/');
$fu->validate();*/
//print_r($fu->files);
  
?>
