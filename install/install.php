<?php 
include('../config.php');
include('bootstrap.php');
//echo ROOTPATH;
$step = isset($_GET['step'])? (int)$_GET['step'] : false;
if(!$step)
{
	header('Location: ../');
	exit;
}

switch($step)
{
	case 1:
		session_start();
		$_SESSION['school_info'] = $_POST;
		header('Location: index.php?step=2');
		break;
	case 2:
		$sql = file_get_contents('mysa-setup.sql');
		if(isset($_POST['db_host']) && isset($_POST['db_username']) && isset($_POST['db_name']))
		{
			//over-ride default db config
			$db = new stdClass();
			$db->hostname = $_POST['db_host'];
			$db->username = $_POST['db_username']; 
			$db->password = $_POST['db_password'];  
			$db->dbname = $_POST['db_name'];

			$dbconfig_file = 
			"<?php
class dbConfig
{
	var \$hostname = '$db->hostname';   //hostname for db connection
	var \$username = '$db->username';  // username for db connection
	var \$password = '$db->password';  //password for db conection
	var \$dbname = '$db->dbname';  //database name
}
?>";

			if(!file_put_contents('../shared/dbconfig/dbconfig.php', $dbconfig_file))
			{
				echo "Installer failed to write dbconfig file. Make sure directory is writable";
			}
			else
			{
				host::load_db();

				$db = new dbConfig();
				$mysql = new mySQLConnection($db);
			    $mysql->select();

				$queries = explode(';', $sql);
				foreach ($queries as $query )
				{
					$mysql->execute_query($query);
				}

				session_start();
				$school = new school();
				$school->insert($_SESSION['school_info']);
			
				header('Location: index.php?complete');
			}	
		}
		else
		{
			header('Location: index.php?step=2&msg='.urlencode("Database parameters incomplete. Please check and ensure that you have supplied correct details"));
		}
		break;
		
}



 ?>