SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `admins` (
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `admins` (`username`, `password`, `name`, `role`) VALUES
('admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', 'admin');

CREATE TABLE IF NOT EXISTS `assesments` (
  `assesment_id` int(11) NOT NULL AUTO_INCREMENT,
  `assesment_name` varchar(100) NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `max_score` int(11) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`assesment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `attendance` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `entered_by` varchar(100) NOT NULL,
  PRIMARY KEY (`session_id`,`subject_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `calendar` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(7) NOT NULL,
  `event_details` text NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `classes` (
  `class_id` varchar(10) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'this is the class teacher',
  `timetable` text NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `class_subjects` (
  `class_id` varchar(10) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  PRIMARY KEY (`class_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `grades` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  `date_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entered_by` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `position` int(4) NOT NULL DEFAULT '1',
  `assignment` varchar(3) NOT NULL DEFAULT '--',
  `test` varchar(3) NOT NULL DEFAULT '--',
  `exam` varchar(3) NOT NULL DEFAULT '--',
  `crap` varchar(3) NOT NULL DEFAULT '--',
  PRIMARY KEY (`session_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `gradingscheme` (
  `id` int(1) NOT NULL,
  `grade_letter` varchar(1) NOT NULL,
  `grade_min` int(2) NOT NULL,
  `grade_max` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `gradingscheme` (`id`, `grade_letter`, `grade_min`, `grade_max`) VALUES
(1, 'A', 70, 100),
(2, 'B', 60, 69),
(3, 'C', 55, 59),
(4, 'P', 50, 54),
(5, 'F', 0, 49);

CREATE TABLE IF NOT EXISTS `message_recieved` (
  `message id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(200) NOT NULL,
  `from` varchar(200) NOT NULL,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`message id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `message_sent` (
  `message id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(200) NOT NULL,
  `from` varchar(200) NOT NULL,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`message id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `posted_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `parents` (
  `parent_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_phone` varchar(15) NOT NULL,
  `land_phone` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `portal` varchar(1) NOT NULL DEFAULT 'N',
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `school` (
  `school_name` varchar(200) NOT NULL,
  `school_admin` varchar(100) NOT NULL,
  `school_email` varchar(100) NOT NULL,
  `school_phoneno` varchar(22) NOT NULL,
  `school_address` text NOT NULL,
  `school_logo` varchar(200) NOT NULL,
  `theme` varchar(50) NOT NULL DEFAULT '../css/themes/thirdeye_default.css',
  PRIMARY KEY (`school_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `session` (
  `month_from` int(2) NOT NULL,
  `month_to` int(2) NOT NULL,
  `term` int(1) NOT NULL,
  PRIMARY KEY (`term`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `session` (`month_from`, `month_to`, `term`) VALUES
(9, 12, 1),
(1, 4, 2),
(5, 8, 3);

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(40) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `picture` varchar(200) NOT NULL DEFAULT '../images/face_outline.gif',
  `status` varchar(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `student_class` (
  `session_id` varchar(32) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `session` varchar(10) NOT NULL,
  `term` int(11) NOT NULL,
  `mode` varchar(10) NOT NULL,
  `class_position` int(4) NOT NULL DEFAULT '1',
  `comment` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `student_subjects` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` varchar(10) NOT NULL,
  PRIMARY KEY (`session_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `subjects` (`subject_id`, `name`) VALUES
(1, 'English Language'),
(2, 'Mathematics');

CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` varchar(40) NOT NULL,
  `firstname` varchar(100) NOT NULL DEFAULT '-',
  `lastname` varchar(100) NOT NULL DEFAULT '-',
  `email` text NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `picture` varchar(200) NOT NULL DEFAULT '../images/face_outline.gif',
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

