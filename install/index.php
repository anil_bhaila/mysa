<?php
session_start(); 

//check if mysa is already installed
if(file_exists('../installed'))
{
    $_GET['complete'] = '';
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>MySchoolAssist: <?php echo $school; ?></title>
<link rel="icon" href="website/images/ico.png" />
<style type="text/css">
body{font-family: Arial; margin:0; padding:0;}
#wrapper{width:960px; height:600px; margin:20px auto 0 auto;}
#school-name{width:662px; height:36px; margin:0px auto 30px 200px; font-size:23px; background-color:#fff; padding-left:0px;}
#content{width:672px; margin:0px auto 0 auto; padding:10px; border:1px solid #ccc;}
h1{width:672px; margin:0px auto 10px auto;}
#install-form{background-color:#fff; margin:0 auto 0 auto; padding:20px; font-size:16px; -webkit-border-radius:10px;}
#install-form input{width:200px; height:25px; font-size:19px; border:1px solid #ccc;}
#install-form textarea{border:1px solid #ccc;}
#install-form form{}
#install-form div, #install-form span{padding:10px 10px 10px 10px;}
#login-btn{width:95px !important; height:34px !important;  margin-left:120px; border:0; color:#fff; font-size:19px;}
#error-msg{text-align:center; color:#F00; width:330px; margin:0 auto 10px auto; background-color:#FBD510; padding:5px; font-size:12px;}
.good{color:green;}
.bad{color:red;}
.prechecks{width:672px; margin:0 auto 0 auto; background-color:#ccc; margin-bottom:10px; padding:10px;}
</style>
</head>

<body onload="document.forms[0].school_name.focus();">

<div id="wrapper">
<h1>Welcome to MySa Installer</h1>
<div class="prechecks">
<?php 
    //do some checks that certain folders are writable
    if(is_writable('../'))
    {
        echo "<div class='good'>main directory is writable. Good!</div>";
    }
    else
    {
        echo "<div class='bad'>main directory is not writable. Please ensure that directory is writable before proceeding with installation</div>";
    }

    if(is_writable('../shared/dbconfig/dbConfig.php'))
    {
        echo "<div class='good'>dbconfig directory is writable. Good!</div>";
    }
    else
    {
        echo "<div class='bad'>dbconfig directory is not writable. Please ensure that directory is writable before proceeding with installation</div>";
    }
 ?>
</div>

  <div id="content">
        <?php if(!isset($_GET['complete'])): ?>
            <div id="install-form">
                <?php if(!isset($_GET['complete']) && !isset($_GET['step']) || $_GET['step'] == 1): ?>
                <h2 style="font-weight:normal; font-size:26px;">Step 1: Enter School Information</h2>
                <form id="form" action="install.php?step=1" method="post">
                    <span>Name of School:</span>
                    <div><input type="text" name="school_name" /></div>
                    <span>School Phone Number:</span>
                    <div><input type="text" name="school_phoneno"/></div>
                    <span>School Address:</span>
                    <div><textarea name="school_address" cols="80" rows="10"></textarea></div>
                    <!-- <span>School Logo:</span>
                    <div><input type="file" name="school_logo"/></div> -->
                    <span>School Admin:</span>
                    <div><input type="text" name="school_admin"/></div>
                    <span>School Email:</span>
                    <div><input type="text" name="school_email"/></div>
                    <input type="submit" name="Submit" value="Next" id="login-btn"/>
                </form>
                <?php elseif(!isset($_GET['complete']) && $_GET['step'] == 2): ?>
                <h2 style="font-weight:normal; font-size:26px;">Step 2: Database Installation</h2>
                <form id="form" action="install.php?step=2" method="post">
                    <span>Database Server:</span>
                    <div><input type="text" name="db_host" value="localhost" /></div>
                    <span>Database Name:</span>
                    <div><input type="text" name="db_name"/></div>
                    <span>Username:</span>
                    <div><input type="text" name="db_username"/></div>
                    <span>Password:</span>
                    <div><input type="password" name="db_password"/></div>
                    <input type="submit" name="Submit" value="Install" id="login-btn"/>
                </form>
                <?php endif; ?>
            </div>
        <?php elseif(isset($_GET['complete'])): ?>
        <?php 
            //create installed file. Inside it will contain. installation date and time, version of mysa installed
            file_put_contents('../installed', "Date: ".date('d/m/Y H:iA')."\nVersion: 1");
         ?>
        <h2>Congratulation! You have successfully Installed Mysa</h2>
        <p>Proceed to <a href="../">login</a> and start setting up mysa for your use</p>
        <p><strong>Admin username:</strong> admin<br/>
            <strong>Password:</strong> password</p>
        <p>Please ensure you change your username and password when you login</p>
        <p>Cheers, enjoy using mysa!</p>
        <?php endif; ?>
       </div>  <!--  end of content -->
    </div>
</body>
</html>
	