<?php
class indexController extends Controller
{
	function index()
	{
		$id = $_SESSION['userinfo']['id'];
		$student_obj = new student();
		$student = $student_obj->select($id);
		
		$session_obj = new session();
		$session = $session_obj->get_current_session_term();
		include(VIEWPATH."home.php");
	}
	
	function myaccount()
	{
            include(VIEWPATH."change_password.php");
	}

        function change_password()
	{
            if(!isset($_POST['Submit']))
            {
                include(VIEWPATH."change_password.php");
            }
            else
            {
                $user_obj = new user();
                $user_obj->change_password("students");
            }
	}

	function subjects()
	{
		$id = $_SESSION['userinfo']['id'];
		$student_obj = new student();
		$subjects = $student_obj->get_subject_offered($id);
		include(VIEWPATH."subjects.php");
	}
	
	function view_grades()
        {
            $student_id = $_SESSION['userinfo']['id'];

            $asst = new assesment();
            $student_obj = new student();

                    $student = $student_obj->select($student_id);
            $data = $student_obj->get_grades($student_id);
            include(VIEWPATH."student_grades.php");
        }
	
	function notices()
	{
		$notice_obj = new notice();
		$notices = $notice_obj->select_all(array('id','title'));
		include(VIEWPATH."notices.php");
	}
	
	function school_calendar()
	{
		$event_obj = new event();
		$events = $event_obj->select_all(array('event_id','event_name','event_date'));
		include(VIEWPATH."school_calendar.php");
	}

        function view_notice()
        {
            $id = $_GET['id'];
            $notice_obj = new notice();
            $notice = $notice_obj->select($id);
            include(VIEWPATH."view_notice.php");
        }

        function view_event()
        {
            $id = $_GET['id'];
            $event_obj = new event();
            $event = $event_obj->select($id);
            include(VIEWPATH."view_event.php");
        }
	
	function timetable()
	{

            $student_id = $_SESSION['userinfo']['id'];
            $student_obj = new student();
            $student_timetable = $student_obj->get_timetable($student_id);
            include(VIEWPATH."timetable.php");
	}
}

?>