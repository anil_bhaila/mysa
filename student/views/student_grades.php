<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>My Assessment | Student Portal</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />-->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>
<body>
	<div id="wrapper">
    	<?php include('includes/top.php'); ?>
        <div id="menu">
        	<?php include('includes/nav.php'); ?>
        </div>
        <div id="content">
        	
        	<div id="content-main">
    			<table width="98%" border="0" align="" cellpadding="3" cellspacing="0">
                <tr>
                  <td colspan="2" valign="top"><h2>My Assesments </h2></td>
                </tr>
                </table>
                <table width="95%" border="0" align="center" cellpadding="5" cellspacing="2">
                <tr class="table-header">
                  <td width="46%">Subject Name</th>
                  <?php echo $asst->assement_header(); ?>
                  <td width="14%">Total</th>
                </tr>
                <?php for($i= 0; $i < count($data); $i++) {?>
                <tr bgcolor="<?php echo color($i); ?>">
                  <td><?php echo $data[$i]['name']; ?></td>
                  <?php echo $asst->assesment_scores($data[$i]); ?>
                  <td><?php echo $data[$i]['total']; ?></td>
                </tr>
                <?php }?>
                </table>

            </div>
        </div>
    </div>
</body>
</html>