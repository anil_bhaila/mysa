<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Notice: <?php echo $notice[0]['title']; ?></title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col"><br />
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <th><?php echo $notice[0]['title']; ?></th>
  </tr>
  <tr>
    <td width="400"><?php echo $notice[0]['body']; ?></td>
  </tr>
  <tr>
    <td style="font-size:11px">
	Posted by <?php echo $notice[0]['posted_by']; ?> on <?php echo readable_date2($notice[0]['date_posted']); ?>
	</td>
  </tr>
</table>
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
    <tr>
      <td align="right"><a href="index.php?action=notices">Back</a></td>    
    </tr>
  </table>
</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
