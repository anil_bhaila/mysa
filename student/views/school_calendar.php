<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>School Calendar</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
  <?php include("includes/top.php"); ?>
</div>
<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
</div>
<div id="mid-col"><br />
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><h2>Upcoming Events </h2> </td>
  </tr>
  <?php if(count($events) > 0): ?>
  <?php foreach($events as $event): ?>
  <tr>
    <td><a href="index.php?action=view_event&id=<?php echo $event['event_id']; ?>"><?php echo $event['event_name']; ?> - <?php echo readable_date2($event['event_date']); ?></a></td>
  </tr>
  <?php endforeach; ?>
  <?php endif; ?>
</table>
</div>
<div id="footer">
	  <p>&reg;myschoolassist 2010</p>
	  <p> <a href="http://myschoolassist.com">myschoolassist.com	</a></p>
	  <p>A product of B.E Systems</p>
	  <p>Read the <a href="#">Terms &amp; Conditions</a> </p>
	</div>
</body>
</html>
