<?php
//determine which version of thirdeye to use.
include("config.php");

require_once("bootstrap.php");

//check if mysa is installed
if(!file_exists('installed'))
{
	header('Location: '.ROOTPATH.'/install/');
}




if(SUBDOMAIN == "on")
{
	$host = new host();
	if($host->is_with_subdomain())
	{
		if($host->validate_subdomain())
		{
			$school = $host->school_name;
			include("login.php");
		}
		else //invalid subdomain - account does not exist
			echo "Invalid subdomain. Check your URL, make sure did not misspell it";
	}
	else
	{
		header("Location: /site");
	}
}
elseif(SUBDOMAIN == "off")
{
	$school = SCHOOLNAME;
	include("login.php");
}


?>
