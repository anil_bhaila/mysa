<?php
define('ROOTPATH', dirname('../index.php')); //hack to make bootstrap work
include('../config.php');
include('../bootstrap.php');



if(isset($_GET['school']))
{
	$school = $_GET['school'];
	include('../shared/dbconfig/'.$school.'_dbConfig.php');
}
else
{
	include('../shared/dbconfig/dbConfig.php');
}

//data format e.g json or xml
$f = $_GET['f'];


$subject_obj = new subject();
$subjects = $subject_obj->select_all(array('name'));

//default to xml
if(!isset($_GET['f']))
	$f = 'xml';


if($f == 'xml')
{
	header("Content-Type: application/xml");
	echo '<?xml version="1.0" ?>';
	echo '<subjects>';
	foreach($subjects as $s){
		echo '<subject>'.$s['name'].'</subject>';
	}
	echo '</subjects>';

}
elseif($f = 'json')
{
	header("Content-Type: text/plain");
	echo json_encode($subjects);
}

?>