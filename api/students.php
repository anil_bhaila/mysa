<?php
define('ROOTPATH', dirname('../index.php')); //hack to make bootstrap work
include('../config.php');
include('../bootstrap.php');

#echo "<pre>";
#print_r($_SESSION); exit;
//check if api is called within myschoolassist.com


if(isset($_GET['sid']))
{
	$school = $_GET['sid'];
	include('../shared/dbconfig/'.$school.'_dbConfig.php');
}
else
{
	include('../shared/dbconfig/dbConfig.php');
}

//data format e.g json or xml
$f = $_GET['f'];


$student_obj = new student();
$students = $student_obj->select_all(array('student_id','firstname','lastname'));

//default to xml
if(!isset($_GET['f']))
	$f = 'xml';


if($f == 'xml')
{
	header("Content-Type: application/xml");
	echo '<?xml version="1.0" ?>';
	echo '<students>';
	foreach($students as $s){
		echo '<student>';
		echo '<id>'.$s['student_id'].'</id>';
		echo '<firstname>'.$s['firstname'].'</firstname>';
		echo '<lastname>'.$s['lastname'].'</lastname>';
		echo '</student>';
	}
	echo '</students>';

}
elseif($f = 'json')
{
	header("Content-Type: text/plain");
	echo json_encode($students);
}

?>