<?php
define('ROOTPATH', dirname('../../index.php')); //hack to make bootstrap work
include('../../config.php');
include('../../bootstrap.php');

host::load_db();

$class_obj = new schoolClass();
$classes = $class_obj->select_all();
?>
<html>
<head>
<title>
ClassLister
</title>
</head>
<body>
<ul>
<?php foreach($classes as $class): ?>
	<li><?php echo $class['class_id']; ?>
	<ul>
	<?php $class_students = $class_obj->get_students($class['class_id']); 
		for($i = 0; $i < count($class_students); $i++): 
	?>
		<li><?php echo $class_students[$i]['firstname'].' '.$class_students[$i]['lastname']; ?></li>
	<?php endfor; ?>
	</ul>
	</li>
	
<?php endforeach; ?>
</ul>
</body>
</html>