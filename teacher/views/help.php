<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin | Students</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />-->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>
<body>
	<div id="wrapper">
    	<?php include('includes/top.php'); ?>
        <div id="menu">
        	<?php include('includes/nav.php'); ?>
        </div>
        <div id="content">
        	
        	<div id="content-main">
            	<h1>Welcome to myschoolassist help</h1>
                
                <p>Here we will show you how to carry out all tasks from the obvious to the less obvious.
                
                If you find this help incomplete please let us know by sending an email to <a href="mailto:help@myschoolassist.com">help@myschoolassist.com</a></p>
                
                <h2>Getting started</h2>
                <p>To get started you need to be familiar with the interface of myschoolassist. The top part of the interface displays basic school information which includes the school logo, name of user currently logged in, name of your school, current school session and term.</p>
                <p>The menu bar is next bar below the top described above. myschoolassist consists of 9 menu items</p>
                
                <p>Home |
                  Students |
                  Teachers |
                  Subjects |
                  Classes |
                  Notices/Events |
                  Report |
                  My Account |
                  Help</p>
                <h2>Customizing myschoolassist</h2>
                <p>The first thing you probably want to do is customize myschoolassist to your taste. This section will show you all the possible ways you can customize myschoolassist. To begin customizing myschoolassist click on the customize button located on the top of the page. There are 5 customizable options in myschoolassist</p>
                <ol>
                <li><h3>School Logo</h3>
                If you haven't changed your school logo already, you willl notice that myschoolassist comes with a default logo placeholder. Changing this logo is very easy.
                To replace the default logo with your school logo,  click on the customize button located on the top of the page. The customization form will be displayed. There you will see a form which will ask you to upload a logo. Click on the browse button and locate the logo on your computer, and click on upload button. Your new logo will appear on the top of the page.</li>
                
                <li><h3>Theme</h3>
                To change the theme of myschoolassist to fit the colour of your school, click on customize button. Under the Look &amp; feel section, select the colour of your choice and it will be immeadiately applied to your myschoolassist interface. If you do not find your desired colour amongst the option provided. Please inform us on <a href="mailto:info@myschoolassist.com">info@myschoolassist.com</a> and we will add it immediately</li>
                
                <li><h3>Grading Scheme</h3>
                Grading scheme is a setting that allows you to define how you grade your students. myschoolassist comes with a default grading scheme, you can modifiy this scheme by clicking the customize button. Scroll down to the Grading Scheme section, there you will see the default grading scheme, you can customize this by clicking on the edit link. Click on submit button to save your changes or cancel button to discard your changes.</li>
                
                <li><h3>School Assessment</h3>
                myschoolassist allows you to customize your assesments types. We know that different school assess students differently. To customize your school assessments. Click on customize button located at the top of the page. Scroll down to the School assesment section. To add a new assessment click on the add new assesment link. To edit an existing assessment click on the assessment name.</li>
                
                <li><h3>School Session</h3>
                In myschoolsssist a session refers to a school session e.g 2009/2010. Over time, a school moves from one session to another. myschoolassist allows you to set this once and it will automatically be able to detect the current session at anytime. To set this, simply specify which periods (months) where 1st, 2nd and 3rd fall into and save.</li>
                </ol>
                
                <h2>My Account</h2>
                <p>My Account menu allows you to manage your myschoolassist account. You can change your username and password, view your subscription status and renew your subscription</p>
                
                <h3>How to change password</h3>
                <p>Changing your myschoolassist password is very easy. Click on my account menu, then click on the change password submenu. Make changes to your password and then click on the submit button to save your changes.</p>
                
                
                <h3>How to renew subscription</h3>
                <p>This feature is coming soon. Not currently possible through the myschoolassist interface. Please contact the myschoolassist team to assist you with this</p>
                
                
                <h3>How to cancel subscription</h3>
                <p>This feature is coming soon. Not currently possible through the myschoolsssist interface. Please contact the myschoolassist team to assist you with this</p>
                
                
                 
                <h2>Student</h2>
                <p>Any tasks pertaining to students can be accomplished on the student page. To access the student page click on the student menu</p>
                
                <h3>Finding a student</h3>
                <p>One basic but very important task is finding a student. It comes in handy in almost all tasks you might need to carry out on a student. Finding a student is easy, just click on the student menu and use the search box to search for a student. You can search by name and student id.</p>
                
                
                
                <h3>How to add a new student</h3>
                <p>To add a student, click on student menu, you will see a Add button click on it. A form will be displayed to allow you enter student information. After entry click on submit to save the student information</p>
                
                
                <h3>How to edit student information</h3>
                <p>To edit student information, click on students menu. Locate student either by looking through the list or by using the search box. When you have found the student select the student by clicking on the checkbox associated to the student and then click on the edit button on top of the list. An edit form will be displayed, make the desired changes and click on submit button to save your changes.</p>
                
                
                <h3>How to delete student</h3>
                <p>To delete a student, click on the students menu. Find the student you want to delete and select the student by clicking the checkbox then click on the submit button to delete the student.</p>
                
                <br/><em>Consequences:</em> When you delete a student, only the student information is lost, the student's grades and parent information will remain in the system</p>
                <h3>How to register student</h3>
                <p>To register a student, click on the students menu then Locate the student either by looking through the list or by using the search the seacrh box. When you have found the student, click on the student's name to access his/her profile. On the student profile page you will find a list of actions, click on register to begin the registration process. </p>
                
                
                <h3>How to access student profile</h3>
                <p>A student profile, is a well layout page displaying a student's information and related actions that can be performed on a student. To access a student's profile, simply find the student and click on the student's name
                
                
                </p>
                <h3>How to unregister student</h3>
                <p>To unregister a student is a very easy.  Go to the student's profile. Click on unregister to unregister the student. 
                
                Note: This action will drop all student subject, remove all scores entered if any and remove the student from the class previously registered to.</p>
                
                <h3>How to drop /pick subject for student</h3>
                <p>You can drop and pick subjects for student through the student profile. Click on the drop/pick subject menu. You will see a list of possible subjects for that student. Make your desired changes and click the submit button to save changes.</p>
                
                
                <h3>What is student status?</h3>
                <p>A student can have five(5) possible status</p>
                <ol>
                <li>enrolled: this means the student is currently a student of your school. However, this is not the same thing as a student being registered</li>
                <li>expelled: this means student has been expelled from school. No results will be generated for this student. No more records will be generated for this student
                  </li>
                </li>
                <li>suspended: this means student has been suspended and will resume school after suspension. result will still be generated for this student.</li>
                <li>graduated: student has graduated from school. All student records will remain on your account</li>
                <li>transferred: student has been transferred to another student. Partial student records will  remain in your account</li>
                </ol>
                
                
                <h3>How to administer student portal account</h3>
                <p>After adding a student, a student portal account is automatically created for that student. Every new student has a default username same as their student id and their password as "password"</p>
                
                
                <h3>How to reset password for student</h3>
                <p>To reset a student password, go to the student's profile and click on the reset password action.</p>
                
                
                <h3>How to Handle students with the same parent</h3>
                <p>It is a common occurrence that you might need to add more than one student that have the same parent. myschoolassist saves you from the stress of having to add the parent information for every student. It allows you to enter this information only once and then just select the parent for the next student.</p>
                
                
                <h3>How to import student records *</h3>
                <p>Coming very soon. Maybe before you even read this</p>
                
                 
                <h2>Teachers</h2>
                <p>Teachers menu allows you to manage all your school teachers. Add new ones, assign them to subjects and classes etc.</p>
                
                <h3>How to add a new teacher</h3>
                <p>To add a new teacher, click on teachers menu then click on the add button</p>
                
                <h3>How to edit teacher information</h3>
                <p>To edit a teacher's information, click on the teachers menu and then select the teacher you want to edit and click on the edit button. An edit form will be displayed, make the desired changes and click submit to save changes.</p>
                
                
                <h3>How to delete teacher</h3>
                <p>To delete a teacher, click on the teachers menu. Select the teacher you want to delete and click on the delete button</p>
                
                
                <h3>How to assign subjects to teachers</h3>
                <p>It is common for a teacher to teach more than one subject. myschoolassist allows you to assign a teacher to more than one subject. To do this, </p>
                
                
                <h3>How to access teacher profile</h3>
                <p>A teacher profile is very similar to a student profile. the only difference is that it displays teachers information
                
                To access a teacher's profile, simply find the teacher and click on the teacher's name</p>
                
                
                <h3>How to reset password</h3>
                <p>To reset a teachers password, go to the teacher's profile and click on the reset password action. This action resets the teacher's password to the default.</p>
                
                 
                <h2>Subjects</h2>
                
                <h3>How to add a subject</h3>
                <p>To add a subject, click on the subjects menu then click on the add button.</p>
                
                
                <h3>How to edit a subject</h3>
                <p>To edit subject, click on the subjects menu. Find the subject you wish to edit, select it by clicking on the checkbox and then click on the edit button to begin editing.</p>
                
                
                <h3>How to delete a subject</h3>
                <p>To delete a subject, click on the subjects menu. Select the subject you wish to delete and click on the delete button.</p>
                
                <h3>Deleting more than one subject</h3>
                <p>Repeat the exact same steps as how to delete a subject expect that this time you select as many subjects as you want to delete and then click on the delete button </p>
                
                
                <h2>Notices/Events</h2>
                
                <h3>Posting a notice</h3>
                <p>In myschoolassist a notice is a message, which the school wants to commuincate to everybody, students, teachers and parents. It helps a school to share information to everybody involved. Posting a notice is very easy. To post a notice click on the notices/events menu then click on the post notice link. Enter details of the notice and click on post button to post it</p>
                
                
                <h3>Editing a notice</h3>
                <p>This feature is coming soon</p>
                
                
                <h3>Deleting a notice</h3>
                <p>To delete a notice click on the home menu. Locate the notice you wish to delete. click on the delete icon beside it and thats it</p>
                
                
                <h3>Adding an event</h3>
                <p>To add an event,  click on the notices/events menu then click on the add new event link. Enter details of the event and click on submit button to save the event.</p>
                
                
                <h3>Editing an event</h3>
                <p>The easiest way to do this is from the home page. Locate the event you wich to edit and click on the edit icon to start editing</p>
                
                
                <h3>Deleting an event</h3>
                <p>The easiest way to do this is from the home page. Locate the event yu wish to delete and click the delete icon beside it.</p>
                
                
                <h2>Classes</h2>
                
                <h3>How create a class</h3>
                <p>To create a new class in your school, click on classes menu then enter the class name and select the class teacher. When you are done click on add button to save the class information.</p>
                
                
                <h3>How edit a class</h3>
                <p>To edit a class, click on classes menu. Find the class you want to edit, select the class by checking the checkbox associated with the class and then click on edit button to begin editing</p>
                
                
                
                <h3>How to delete a class</h3>
                <p>To  delete a class, click on classes menu. Select the class or classes you  want to delete by checking the checkbox associated with the class and then click on delete button to delete </p>
                
                
                
                <h2>Report Card</h2>
                
                <h3>How to generate report card for a class</h3>
                <p>To generate a report card for a class is very easy. Click on the report card menu. Select the class, session and term you want to generate a report for and click on the generate button. The report is in PDF format and will open in a new window.</p>
                
                
                <h3>Printing a report card</h3>
                <p>Once you have generated a report card. Printing is very easy, jst select the print option from the File menu as you would do for any standard document</p>
                
                
                <h3>Customizing report card *</h3>
                <p>Not currently possible on myschoolassist. Coming soon</p>
            </div>
        </div>
    </div>
</body>
</html>
