<?php
$event_obj = new event();
$events = $event_obj->select_all(array('event_id','event_name','event_date'));

$notice_obj = new notice();
$notices = $notice_obj->select_all(array('id','title'));
?>
<br />
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <th>Notices</th>
  </tr>
  <?php foreach($notices as $notice): ?>
  <tr>
    <td><a href="index.php?action=view_notice&id=<?php echo $notice['id']; ?>"><?php echo $notice['title']; ?></a></td>
  </tr>
  <?php endforeach; ?>
</table>
<p>&nbsp;</p>
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <th>School Calendar </th>
  </tr>
  <?php foreach($events as $event): ?>
  <tr>
    <td><a href="index.php?action=view_event&id=<?php echo $event['event_id']; ?>"><?php echo $event['event_name']; ?> - <?php echo readable_date2($event['event_date']); ?></a></td>
  </tr>
  <?php endforeach; ?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
