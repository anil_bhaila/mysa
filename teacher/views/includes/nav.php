<?php 
$teacher_obj = new teacher();
$default_class = $teacher_obj->get_classes($_SESSION['userinfo']['id']);		

$menu = array('Dashboard' => 'index.php', 
			  'My Classes' => 'myclasses.php?action=get_subjects&class='.$default_class[0]['class_id'], 
			 /* 'Notices/Events' => 'notices-events.php' , */
			  'Report' => 'report.php',
			  'My Account' => 'myaccount.php'
			  /*'Help' => 'help.php'*/
			  );

//echo "<pre>";
//print_r($_SERVER); 

//determine which menu was clicked
//script name
$script_name = $_SERVER['SCRIPT_NAME'];
$sn = substr($script_name,strrpos($script_name,'/')+1,strlen($script_name));

echo "<ul>";
foreach($menu as $page => $link ){
	if(strpos($link,$sn) !== false)
	{
		echo "<li id=\"current\"><a href=\"$link\" >$page</a></li>";
	}
	else
		echo "<li><a href=\"$link\">$page</a></li>";	
}
echo "</ul>";
?>
