<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enter Attendance</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="../css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
<?php include("includes/jscript_include.php"); ?>
<script type="text/javascript" language="javascript">
$(function()
	{
		$('.date-pick').datePicker({clickInput: true,createButton: false});
	});
	
$(function()
	{
		$('.date-pick-past').datePicker({startDate:'1980-01-01',clickInput: true,createButton: false});
	});
</script>
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col">
  <form action="index.php?action=enter_attendance" method="post" name="enter_attendance" id="enter_attendance">
    <label> <br />
</label>
<table width="70%" border="0" align="center">
  <tr>
    <td><h3>Attendance: <?php echo $this->class, ' : ', $_SESSION['work']['subname']; ?> </h3>
      <table width="400" border="0" cellpadding="3" cellspacing="5">
        <tr>
          <td width="79">Date</td>
          <td width="294">
          <input type="text" name="date" class="date-pick dp-applied" />
          <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>" />
          </td>
        </tr>
      </table>
      <table width="400" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2"><a href="#" onclick="selectAll('student[]')">select all</a> | <a href="#" onclick="unselectAll('student[]')">unselect all</a></td>
          </tr>
        <tr>
          <th width="180">Student Name</th>
          <th width="204">Attendance</th>
        </tr>
      </table>
      <div style="width:400px; height:290px; overflow:auto; background-color:#FFFFFF">
        <table width="100%" border="0" cellpadding="5" cellspacing="0"  bgcolor="#FFFFFF">
          <?php if(count($students) > 0): ?>
          <?php foreach($students as $student): ?>
          <tr>
            <td width="200"><?php echo $student['firstname']," ",$student['lastname']; ?></td>
            <td width="200"><label>
              <input type="checkbox" name="student[]" value="<?php echo $student['id']; ?>" />
            </label>
            </td>
          </tr>
          <?php endforeach; ?>
          <?php else: ?>
            <tr><td><strong>No students seem to be registered to this class for this subject.</strong></td></tr>
         <?php endif; ?>
             </table>
      </div>
        <?php if(count($students) > 0): ?>
       <p><input type="submit" name="Submit" value="Submit" /></p>
      <?php endif; ?>
     </td>
  </tr>
</table>
<label><br />
</label>
  </form>
</div>
<div id="footer">
myschoolassist 2009 myschoolassist.com
</div>
</body>
</html>