<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?> Student Profile</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
  <?php include("includes/top.php"); ?>
</div>
<?php include("includes/main-nav.php"); ?>
<div id="left-main-col">
  <table width="150" border="0" id="student-photo">
  <tr>
    <td>
	<img src="<?php echo $student[0]['picture']; ?>" width="150" height="156"/>
	</td>
  </tr>
</table>
<br />
  <table width="90%" border="0" align="center" id="session">
  <tr>
    <td valign="top"><p><strong>Session: </strong>2009/2010<?php echo $session[0]['session'];?></p>
      <p><strong>Term: </strong>1st <?php echo $session[0]['term'];?></p>
	</td>
  </tr>
</table>
<br />
<?php include("includes/student-action.php"); ?>
</div>
<div id="right-main-col">
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="2" valign="top"><h2><?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?></h2></td>
    </tr>
  </table>
  <br />
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#F1F1F1">
  
  <tr>
    <td valign="top">Status</td>
    <td valign="top"><?php echo $student[0]['status']; ?></td>
  </tr>
  <tr>
    <td width="234" valign="top">Student ID: </td>
    <td width="493" valign="top"><strong><?php echo $student[0]['student_id']; ?></strong></td>
  </tr>
  
  <tr>
    <td valign="top">Gender</td>
    <td valign="top"><?php echo $student[0]['gender']; ?></td>
  </tr>
  <tr>
    <td valign="top">Current Class: </td>
    <td valign="top">
	<?php 
	if(student::is_registered($student[0]['id']) == "Yes")
		echo $student_obj->get_current_class($student[0]['id']);
	elseif(student::is_registered($student[0]['id']) == "No") 
		echo "--";
	?></td>
  </tr>
  <tr>
    <td valign="top">Date of birth </td>
    <td valign="top"><?php echo readable_date2($student[0]['dob']); ?></td>
  </tr>
</table>
  <p>&nbsp;</p>
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#F1F1F1">
    <tr>
      <td colspan="2" valign="top"><strong>Contact Information </strong></td>
    </tr>
    <tr>
      <td width="234" valign="top">Email:</td>
      <td width="493" valign="top"><?php echo $student[0]['email']; ?></td>
    </tr>
    <tr>
      <td valign="top">Phone number:</td>
      <td valign="top"><?php echo $student[0]['phoneno']; ?></td>
    </tr>
    
    <tr>
      <td valign="top"> Addresss:</td>
      <td valign="top"><?php echo $student[0]['address']; ?></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#F1F1F1">
    <tr>
      <td valign="top"><strong>Subjects Offered </strong></td>
    </tr>
    <tr>
      <td width="234" valign="top">
          <?php if(count($subjects) > 0) {?>
          <ol>
            <?php for($j= 0; $j < count($subjects); $j++) {?>
            <li><?php echo $subjects[$j]['name']; ?></li>
            <?php }?>
        </ol>
          <?php } 
  else
  		echo $student[0]['lastname']," ",$student[0]['firstname']," is not registered for the current session/term.";
  ?>      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
<div id="footer"> myschoolassist 2009 myschoolassist.com </div>
</body>
</html>
