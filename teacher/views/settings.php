<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Settings</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<?php include("includes/main-nav.php"); ?>
<div id="main-col">
 <div id="centered">
 <div id="settings-menu">
	 <ul>
		<!-- CSS Tabs -->
		<li><a href="settings.php?action=set_session">School Session</a></li>
		<li><a href="settings.php?action=set_grading_scheme">Grading Scheme</a></li>
		<li><a href="settings.php?action=set_assesment">School Assesment</a></li>
		<li><a href="settings.php?action=set_theme">Theme</a></li>
	</ul>
</div>
</div>
</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
