<?php include('includes/header.php'); ?>
<h1>Students: <?php echo $this->class, ' (', $_SESSION['work']['subname']; ?>)</h1>

  	  <form action="index.php?tab=student&action=add_student" method="post" name="add_student" id="add_student">
         <table width="95%" border="0" cellpadding="5" cellspacing="0">
            <tr class="table-header">
              <td width="13%">ID</td>
              <td width="45%">Name</td>
              <?php echo $header; ?>
              <td width="11%">Total</td>
              <td width="11%">&nbsp;</td>
            </tr>
            <?php
            for($i = 0; $i < count($students); $i++)
            { ?>
            <tr bgcolor="<?php echo color($i); ?>">
              <td><?php echo $students[$i]['student_id']; ?></td>
              <td><?php echo $students[$i]['firstname']; ?> <?php echo $students[$i]['lastname']; ?></td>
              <?php echo $asst->assesment_scores($students[$i]); ?>
              <td><?php echo $students[$i]['total']; ?>/100</td>
              <td><a href="myclasses.php?action=edit_scores&class=<?php echo $this->class; ?>&id=<?php echo $students[$i]['id']; ?>">edit scores</a> </td>
            </tr>
            <?php } ?>
          </table>
        </form>
</div> <!-- end of main -->
