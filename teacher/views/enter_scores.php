
<?php include('includes/header.php'); ?>
  	  <form action="index.php?class=<?php echo $this->class; ?>&action=enter_scores" method="post" name="edit_score" id="edit_score">

      <h1>Enter Scores: <?php echo $this->class, " : ", $_SESSION['work']['subname']; ?></h1></td>
    
      <table width="100%" border="0" cellpadding="3" cellspacing="0">
        <tr class="table-header">
          <td width="200">Student name</td>
          <?php echo $header; ?>
          <td>Comment</td>
        </tr>
     </table>
        <div style="width:100%; height:500px; overflow:auto; background-color:#eee; ">
          <table width="100%" border="0" cellpadding="3" cellspacing="0"  bgcolor="#FFFFFF">
          <?php if(count($students) > 0): ?>
          <?php foreach($students as $student): ?>
            <tr bgcolor="<?php echo color($i); ?>">
                  <td width="200"><?php echo $student['firstname'].' '.$student['lastname']; ?></td>
                 <?php echo $asst->inline_edit_scores($student); ?>
                <td>
                <label>
                  <textarea name="<?php echo $student['session_id']; ?>[comment]" rows="2"><?php echo $student['comment']; ?></textarea>
                  <input name="<?php echo $student['session_id']; ?>[subject_id]" type="hidden" id="subject_id" value="<?php echo $student['subject_id']; ?>" />
                </label>
                </td>
            </tr>
          <?php endforeach; ?>
          <?php else: ?>
          <tr><td><strong>No students seem to be registered to this class for this subject.</strong></td></tr>
          <?php endif; ?>
        
      </table>
        </div>
         <?php if(count($students) > 0): ?>
        <div style="width: 100%; margin-left: auto; margin-right: auto"><input type="submit" name="Submit" value="Submit" /></div>
        <?php endif; ?>
    </form>
</div>

</div> <!-- end of main -->

