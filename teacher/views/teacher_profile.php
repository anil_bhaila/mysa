<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $teacher[0]['firstname']," ",$teacher[0]['lastname']; ?> Profile</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="../../shared/jquery/css/ui-lightness/jquery-ui-1.7.2.css"rel="stylesheet" type="text/css" />
<link href="../css/datePicker.css" rel="stylesheet" type="text/css" />
<link href="../css/autocomplete.css" rel="stylesheet" type="text/css" />
<?php include('includes/jscript_include.php');?>
</head>

<body>
<div id="top"><?php include("includes/top.php"); ?></div>
<?php include("includes/main-nav.php"); ?>
<div id="left-col">
  <table width="150" border="0" id="student-photo">
  <tr>
    <td>
	<img src="<?php echo $teacher[0]['picture']; ?>" width="150" height="156"/>
	</td>
  </tr>
</table>
<br />
  <table width="90%" border="0" align="center" id="session">
  <tr>
    <td valign="top"><p><strong>Session: </strong>2009/2010<?php echo $session[0]['session'];?></p>
      <p><strong>Term: </strong>1st <?php echo $session[0]['term'];?></p>
	</td>
  </tr>
</table>
<br />
<?php include("includes/teacher-action.php"); ?>
</div>
<div id="mid-col">
<table width="95%" border="0" align="center">
  <tr>
    <td colspan="2" valign="top"><h2><?php echo $teacher[0]['firstname']," ",$teacher[0]['lastname']; ?></h2></td>
    </tr>
  <tr>
    <td width="234" valign="top"><strong>Teacher ID: </strong></td>
    <td width="493" valign="top"><?php echo $teacher[0]['teacher_id']; ?></td>
  </tr>
  <tr>
    <td valign="top"><strong>Name:</strong></td>
    <td valign="top"><?php echo $teacher[0]['firstname']," ",$teacher[0]['lastname']; ?></td>
  </tr>
</table>
<br/>
<div id="accordion">
	<h3><a href="#">Contact Information </a></h3>
	<div>
		<p><strong>Email:</strong><br/> 
		<?php echo $teacher[0]['email']; ?></p>
		<p><strong>Phone number:</strong> <br/>
		  <?php echo $teacher[0]['phoneno']; ?> </p>
		<p><strong>Addresss:</strong><br/>
		<?php echo $teacher[0]['address']; ?>
		</p>
	</div>
	<h3><a href="#">Subjects Taught </a></h3>
    <div>
	<ol> 
	<?php 
	 for($i = 0; $i < count($subjects); $i++)
	 {
	 ?>
	 <li><?php echo $subjects[$i]['name']," - ",$subjects[$i]['class_id']?></li>
	 <?php }?>
	 </ol>
  </div>
</div>
</div>
<!--<div id="right-col"><?php include("includes/right-side.php"); ?></div>-->
<div id="footer">myschoolassist 2009 myschoolassist.com</div>
</body>
</html>