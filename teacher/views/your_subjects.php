<?php include('includes/header.php'); ?>
        
<h1>My Classes</h1>

Select Class:
<select id="teacher-classes" onchange="window.location=$(this).val();">
  <?php foreach($classes as $class): ?>
    <?php if($_GET['class'] == $class['class_id']): ?>
    <option value="myclasses.php?action=get_subjects&amp;class=<?php echo $class['class_id'];?>" selected> <?php echo $class['class_id'];?></option>
    <?php else: ?>
    <option value="myclasses.php?action=get_subjects&amp;class=<?php echo $class['class_id'];?>"> <?php echo $class['class_id'];?></option>
    <?php endif; ?>
  <?php endforeach;?>
</select>

<table width="100%" border="0" cellpadding="5" cellspacing="0" style="margin-top:10px;" >
<tr class="table-header">
<td colspan="2">Subjects</td>
  </tr>
<?php $i= 0; foreach($subjects as $subject): ?>
<tr bgcolor="<?php echo color($i++); ?>">
<td width="53%"><strong style="font-weight:normal;"><?php echo $subject['name']; ?></strong></td>
<td width="47%" align="right" style="font-size:12px;">
  <a href="myclasses.php?action=enter_scores&amp;class=<?php echo $this->class; ?>&amp;subname=<?php echo $subject['name']; ?>&amp;subid=<?php echo $subject['subject_id']; ?>">Enter scores</a> 
  <!--<a href="index.php?action=enter_attendance&amp;class=<?php echo $this->class; ?>&amp;subname=<?php echo $subject['name']; ?>&amp;subid=<?php echo $subject['subject_id']; ?>">Enter Attendance</a> -->
  <a href="myclasses.php?action=show_students&amp;class=<?php echo $this->class; ?>&amp;subname=<?php echo $subject['name']; ?>&amp;subid=<?php echo $subject['subject_id']; ?>">Students</a>
</td>
</tr>
<?php endforeach; ?>
</table>


</div> <!-- end of main -->
 