<?php
class classController extends Controller
{
	function indexController()
	{
		if(isset($_GET['class']))
			$this->class = $_GET['class'];
			
		parent::Controller();
	}
	
	function index()
	{	
		$school_obj = new school();
		$sch_info = $school_obj->select_all(array('school_logo'));
		
		$teacher_obj = new teacher();
		$classes = $teacher_obj->get_classes($_SESSION['userinfo']['id']);		
		$this->class = $class = $classes[0]['class_id'];
		$subjects = $teacher_obj->get_subjects($_SESSION['userinfo']['id'],$class);
		
		
		
		$_SESSION['work']['teacher'] = $_SESSION['userinfo']['id']; 
		include(VIEWPATH."your_subjects.php");
	}
	
	function get_subjects()
	{	
		$school_obj = new school();
		$sch_info = $school_obj->select_all(array('school_logo'));
		
		$teacher_obj = new teacher();
		$classes = $teacher_obj->get_classes($_SESSION['userinfo']['id']);		
		$this->class = $class = $_GET['class'];
		$subjects = $teacher_obj->get_subjects($_SESSION['userinfo']['id'],$class);
		
		
		
		$_SESSION['work']['teacher'] = $_SESSION['userinfo']['id']; 
		include(VIEWPATH."your_subjects.php");
	}
	
	function enter_scores()
	{
		$this->class = $_GET['class'];
		$class_obj = new schoolClass();
		
		$asst = new assesment();
		$header = $asst->assement_header();
		
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		
		if(!isset($_POST['Submit']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$students = $class_obj->get_students_grades($this->class,$subject_id);
			include(VIEWPATH."enter_scores.php");
		}
		else
		{
			$studentGrade_obj = new studentGrade();
			$studentGrade_obj->update_grades($_POST);

			$subject_id = $_SESSION['work']['subid'];
			$students = $class_obj->get_students_grades($this->class,$subject_id);
			include(VIEWPATH."your_students.php");
		}
	}
	
	
	function edit_scores()
	{
		$this->class = $_GET['class'];
		
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		else
		{
			$subject_id = $_SESSION['work']['subid'];
		}
	
		$stud = new student();
		$students = $stud->get_subject_grade($_GET['id'],$subject_id);
		include(VIEWPATH."edit_score.php");
	}
	
	function show_students()
	{
		$this->class = $_GET['class'];
		
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		else
		{
			$subject_id = $_SESSION['work']['subid'];
		}
		
		$asst = new assesment();
		$header = $asst->assement_header();

		$class = new schoolClass();
		$students = $class->get_students_grades($this->class,$subject_id);
		include(VIEWPATH."your_students.php");
	}
	
	
}

?>