<?php
class indexController extends Controller
{
	function indexController()
	{
		if(isset($_GET['class']))
			$this->class = $_GET['class'];
			
		parent::Controller();
	}
	
	function index()
	{	
		include(VIEWPATH."home.php");
	}
	
	function get_subjects()
	{
		$school_obj = new school();
		$sch_info = $school_obj->select_all(array('school_logo'));
		
		$this->class = $_GET['class'];
		$teacher_obj = new teacher();
		$subjects = $teacher_obj->get_subjects($_SESSION['userinfo']['id'],$this->class);
		
		$_SESSION['work']['teacher'] = $_SESSION['userinfo']['id']; 
		include(VIEWPATH."your_subjects.php");
	}
	
	function enter_scores()
	{
		$class_obj = new schoolClass();
		
		$asst = new assesment();
		$header = $asst->assement_header();
		
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		
		if(!isset($_POST['Submit']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$students = $class_obj->get_students_grades($this->class,$subject_id);
			include(VIEWPATH."enter_scores.php");
		}
		else
		{
			$studentGrade_obj = new studentGrade();
			$studentGrade_obj->update_grades($_POST);

			$subject_id = $_SESSION['work']['subid'];
			$students = $class_obj->get_students_grades($this->class,$subject_id);
			include(VIEWPATH."your_students.php");
		}
	}
	
	
	function edit_scores()
	{
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		else
		{
			$subject_id = $_SESSION['work']['subid'];
		}
	
		$stud = new student();
		$students = $stud->get_subject_grade($_GET['id'],$subject_id);


		
		include(VIEWPATH."edit_score.php");
	}
	
	function show_students()
	{
		if(isset($_GET['subname']) && isset($_GET['subid']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_SESSION['work']['subid'] = $_GET['subid'];
		}
		else
		{
			$subject_id = $_SESSION['work']['subid'];
		}
		
		$asst = new assesment();
		$header = $asst->assement_header();

		$class = new schoolClass();
		$students = $class->get_students_grades($this->class,$subject_id);
		include(VIEWPATH."your_students.php");
	}
	
	function enter_attendance()
	{
		if(!isset($_POST['Submit']))
		{
			$_SESSION['work']['subname'] = $_GET['subname'];
			$subject_id = $_GET['subid'];
			
			$class_obj = new schoolClass();
			
			$students = $class_obj->get_students($this->class,$subject_id);
                        //$this->xray($students); exit;
			include(VIEWPATH."attendance.php");
		}
		else
		{
			$attendance_obj = new attendance();
			$attendance_obj->insert($_POST);
			
			include(VIEWPATH."attendance_success.php");
		}
	}
	
	/* event and notice */
	
	function view_event()
	{
		$id = $_GET['id'];
		$event_obj = new event();
		$event = $event_obj->select($id);
		include(VIEWPATH."event.php");
	}
	
	function view_notice()
	{
		$id = $_GET['id'];
		$notice_obj = new notice();
		$notice = $notice_obj->select($id);
		include(VIEWPATH."notice.php");
	}
	
	/* search student */
	function search()
	{
		$keywords = $this->get_keywords();
		$student_obj = new student();
		$students = $student_obj->search($keywords);
		
		$c = count($keywords);
		$searched = $keywords[$c-1];
		
		include(VIEWPATH."sr_student.php");
	}
	
	function get_keywords()
	{
		if(isset($_POST['search_string']))
		{
			$search_string = $_POST['search_string'];
			
			$keywords = explode(" ",$search_string);
			if(!in_array($search_string,$keywords))
				array_push($keywords,$search_string);
				
			return $keywords;
		}
		else
		{
			header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
		}
	}
	
	/* student profile */
	
	function student_profile()
	{
		$id = $_GET['id'];
		$student_obj = new student();
		$student = $student_obj->select($id);
		$subjects = $student_obj->get_subject_offered($id);
		include(VIEWPATH."student_profile.php");
	}
	
	function view_grades()
    {
    	$student_id = $_GET['student_id'];
    	
    	$asst = new assesment();
    	$student_obj = new student();
    	
		$student = $student_obj->select($student_id);
    	$data = $student_obj->get_grades($student_id);
    	include(VIEWPATH."student_grades.php");
    }
    
    function view_attendance()
    {
    	$student_id = $_GET['student_id'];
    	$student_obj = new student();
		$student = $student_obj->select($student_id);
    	include(VIEWPATH."student_attendance.php");
    }
}

?>