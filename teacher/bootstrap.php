<?php

define("ROOTPATH",dirname("../index.php"));

$model_path = ROOTPATH."/shared/models/";

$lib_path = ROOTPATH."/shared/lib/";

$view_path = "views/";

$controller_path = "controllers/";

define("LIBPATH",$lib_path);

define("MODELPATH",$model_path);

define("VIEWPATH",$view_path);

define("CONTROLLERPATH",$controller_path);


function __autoload($class)
{
    $dir = array(LIBPATH,MODELPATH,CONTROLLERPATH);
    $file_found = false;
    for($i = 0; $i < count($dir); $i++)
    {
        if(is_file($dir[$i]."$class.php"))
        {
            require_once($dir[$i]."$class.php");
            $file_found = true;
            break;
        }
    }  
    
    if(!$file_found)
    echo "File: $class.php was not found";
}


?>