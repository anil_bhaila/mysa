<?php
ob_start("ob_gzhandler");
require_once("bootstrap.php");
require_once("../config.php");

$school = host::load_db();

if(!isset($_SESSION))
    session_start();

if($_SESSION['admin_login'])
    header("Location: index.php");

if(isset($_POST['Submit']) && $_POST['Submit'] == "Login")
{
    $uname = $_POST['username'];
    $pswd = $_POST['password'];
    //TODO change all header parameters to complete URL
    
    $login = new login($uname,$pswd);
    $login->set_table("admins");
    if($login->authenticate())
    {
        $_SESSION['admin_login'] = true; 
        $_SESSION['userinfo'] = $login->session_data;
        
        $school_obj = new school();
        $theme = $school_obj->select_all(array('theme'));
        $_SESSION['theme'] = $theme[0]['theme'];	
        
        header("location: index.php");
    }
    else
    {
        $msg = $login->get_error();
		$_SESSION['login_err'] = 1;
		header("location: ../index.php?user=admin");
    } 
}
?>