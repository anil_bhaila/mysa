<?php 
include_once("bootstrap.php");
include_once("../config.php");

host::start_app(); 

/*echo "selected class: ".$_GET['class'];*/
if($_GET['class'] != "select")
{
$class_obj = new schoolClass();
$subjects = $class_obj->get_subjects($_GET['class']);
 if(count($subjects) > 0)
 {
?>
<table width="95%" cellspacing="0" cellpadding="5">
<tr><td>Subjects</td></tr>
<tr>
  <td>Register student subjects by checking the subject the student will study this term. </td>
</tr>
</table>
    <table width="95%" border="0" cellpadding="5" cellspacing="0">
	<?php foreach($subjects as $subject): ?>
      <tr>
        <td width="60%"><?php echo $subject['name']; ?></td>
        <td width="40%">
        <label>
          <input name="subjects[]" type="checkbox" value="<?php echo $subject['subject_id']; ?>" checked="checked" />
        </label>
        </td>
      </tr>
	  <?php endforeach; ?>
	</table>
    <table width="95%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td>
		<input type="submit" name="Submit" value="Submit" />
		</td>
      </tr>
</table>
      <?php 
	}
	else 
	{ ?>
</p>
   <table width="80%" border="0" bgcolor="#FFFFCC" style="border:1px #FF0000 solid; font-size:13px;">
      <tr>
        <td valign="top"><p align="left"><strong><img src="../images/alert.png" width="48" height="50" align="left" />No subjects have been added for this class.<br />
        </strong>To do this click on classes then click on class name. Select subjects offered by class and select a teacher for each subject then submit the form </p>
        </td>
      </tr>
</table>
	<?php }
} ?>
