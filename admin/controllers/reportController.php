<?php

class reportController extends Controller
{
	function index()
    {	
    	$class_obj = new schoolClass();
		$class_list = $class_obj->select_all(array("class_id"));
		
		include(VIEWPATH."report_home.php");
    }
    
    function show_grades()
    {
    	$student_id = $_GET['id'];
    	$session = $_GET['ses'];
    	$term = $_GET['term'];
    	
    	$student_obj = new student();
    	$data = $student_obj->get_grades($student_id,$session,$term);
		
		$assesment_obj = new assesment();
		$header = $assesment_obj->assement_header();
    	include(VIEWPATH."student_grades.php");
    }
	
	function comment()
	{
		$student_id = $_POST['id'];
    	$session = $_POST['session'];
    	$term = $_POST['term'];
    	
    	$student_obj = new student();
    	$data = $student_obj->principal_comment($_POST);
    	$data = $student_obj->get_grades($student_id,$session,$term);
		
		$assesment_obj = new assesment();
		$header = $assesment_obj->assement_header();
    	include(VIEWPATH."student_grades.php");
		
	}

    function print_report()
    {
        
        $sch = new school(); // new school object
        $student = new student();  //new student object
        $schclass = new schoolClass();  //new school class object
        $asm = new assesment(); //new assesment object
        $sg = new studentGrade();

        $class = $_GET['class'];
        $session = $_GET['session'];
        $term = $_GET['term'];

        //Front cover of the report Begining
        $school_info = $sch->select_all(array('school_name','school_logo','school_address','school_phoneno'));
        
        $school_logo = $school_info[0]['school_logo'];
        $school_name = $school_info[0]['school_name'];
        $school_address = $school_info[0]['school_address'];
        $school_phone = $school_info[0]['school_phoneno'];
        $report_title = "Report Sheets for $class";
        
        $sg->position_in_subject($class,$session,$term); //compute and update student position in subject
        $sg->position_in_class($class,$session,$term);


        $students = $schclass->get_student_ids($class,$session,$term); //get student ids
        $assesments = $asm->select_active(); //active assesments for current school
        
        include(VIEWPATH."report_card.php");
    }
}

?>

