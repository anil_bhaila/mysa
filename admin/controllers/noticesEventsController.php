<?php
class noticesEventsController extends Controller
{
	
	function index()
	{	
		$notice_obj = new notice();
		$notices = $notice_obj->select_all();
		
		$event_obj = new event();
		$events = $event_obj->select_all();
		
		include(VIEWPATH."notices_events.php");
	}
	
	
	function post_notice()
	{
		$notice_obj = new notice();
		$_POST['posted_by'] = $_SESSION['userinfo']['name'];
		$notice_obj->insert($_POST);

		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
		
	}
	
	function delete_notice()
	{
		$notice_obj = new notice();
		$notice_obj->delete($_GET['id']);

		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
		
	}
	
	function add_event()
	{
		if(!isset($_POST['Submit'])){
			include(VIEWPATH."add_event.php");
		}
		else{
			$validation_obj = new validation($_POST);
			$notempty = array('event_name','event_date','event_details');
			
			$validation_obj->is_empty($notempty);
			$validation_obj->is_date('event_date');
			if(!$validation_obj->error_found()){
				$_POST['event_time'] = $_POST['hour'].":".$_POST['minute']."".$_POST['ampm'];
				$event_obj = new event();
				$event_obj->insert($_POST);
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
			}
			else{
				$errors = $validation_obj->get_error_messages();
				include(VIEWPATH."add_event.php");
				
				echo formPopulator::populate(); //populate the form with typed input
			}
		}
	}
	
	function view_event()
	{
		$id = $_GET['event_id'];
		$event_obj = new event();
		$event = $event_obj->select($id);
		//$this->xray($event);
		include(VIEWPATH."view_event.php");
	}
	
	function edit_event()
	{
		$id = $_GET['event_id'];
		$event_obj = new event();
		$event = $event_obj->select($id);
		$hour = substr($event[0]['event_time'],0,2);
		$min = substr($event[0]['event_time'],3,2);
		$ampm = substr($event[0]['event_time'],5,2);
		
		//$this->xray($event); exit;
		include(VIEWPATH."edit_event.php");
	}
	
	function update_event()
	{
		if(isset($_POST['Submit'])){
			$validation_obj = new validation($_POST);
			$notempty = array('event_name','event_date','event_details');
			
			$validation_obj->is_empty($notempty);
			$validation_obj->is_date('event_date');
			if(!$validation_obj->error_found()){
				$_POST['event_time'] = $_POST['hour'].":".$_POST['minute']."".$_POST['ampm'];
				$event_obj = new event();
				$event_obj->update($_POST['event_id'], $_POST);
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
			}
			else{
				$errors = $validation_obj->get_error_messages();
				include(VIEWPATH."edit_event.php");
				
				echo formPopulator::populate(); //populate the form with typed input
			}
		}
	}
	
	function delete_event()
	{
		$id = $_GET['event_id'];
		$event_obj = new event();
		$event = $event_obj->delete($id);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	

	
}

?>