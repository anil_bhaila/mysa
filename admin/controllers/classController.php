<?php

class classController extends Controller
{
	function __construct()
	{
		if(SUBDOMAIN == "on")
		{
			$host = new host();
			if($host->get_subdomain())
			{
				$this->subdomain = $host->subdomain;
			}
		}
		elseif(SUBDOMAIN == "off")
			$this->subdomain = SCHOOL;
		
		parent::Controller();
	}
	
	function index()
	{		
		$class_obj = new schoolClass();
		$class_list = $class_obj->select_all();
		
		$teacher = new teacher();
		$teacher_list = $teacher->select_all(array("id","firstname","lastname"));
	
		include(VIEWPATH."view_classes.php");
	}
	
	function add_class()
	{
		if(!isset($_POST['Submit']))
		{
			$teacher = new teacher();
			$teacher_list = $teacher->select_all(array("id","firstname","lastname"));
			include(VIEWPATH."add_class.php");
		}
		else
		{
			$val = new validation($_POST);
			$notempty = array('class_id');
			$val->is_empty($notempty);
			if($val->error_found())
			{
				$val->set_message('teacher_id', 'You must select a teacher');
				$errors = $val->get_error_messages();
				$teacher = new teacher();
				$teacher_list = $teacher->select_all(array("id","firstname","lastname"));
				include(VIEWPATH."add_class.php");
				echo formPopulator::populate();		
			}
			else
			{
				$cls = new schoolClass();
				$cls->insert($_POST);
				$_SESSION['added'] = 'yea';
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
			}
		}
	}
	
	function edit_class()
	{
		$class_id = $_POST['class_id'][0];
		$teacher_obj = new teacher();
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
		
		$class_obj = new schoolClass();
		$class = $class_obj->select($class_id);
		include(VIEWPATH."edit_class.php");
	}
	
	function update_class()
	{
		$id = $_POST['id'];
		$class_obj = new schoolClass();
		$class_obj->update($id,$_POST);
	
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function delete_class()
	{
		$class_id = $_GET['class_id'];
		
		$class_obj = new schoolClass();
		$class_obj->delete($class_id);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function class_subjects()
	{
		$subject_obj = new subject();
		$subjects = $subject_obj->select_all();
		
		$class_obj = new schoolClass();
		$cs = $class_obj->get_subjects($_GET['class']);
		
		$class_subjects = array();
		if(is_array($cs))
		{
		foreach($cs as $s)
			$class_subjects[] = $s['subject_id'];
		}
		
		$class_teachers = $class_obj->get_teachers($_GET['class']);
		//$this->xray($class_teachers);
		
		$teacher_obj = new teacher();
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
		
		include(VIEWPATH."class_subjects.php");
	}
	
	function add_subject()
	{
		$class_id = $_GET['class_id'];
		$subject_id = $_GET['subject_id'];
		
		$class_obj = new schoolClass();
		$class_obj->add_subject($class_id, $subject_id, '');
		header("Location: {$_SERVER['HTTP_REFERER']}");
		
	}
	
	function remove_subject()
	{
		$class_id = $_GET['class_id'];
		$subject_id = $_GET['subject_id'];
		
		$class_obj = new schoolClass();
		$class_obj->remove_subject($class_id, $subject_id);
		header("Location: {$_SERVER['HTTP_REFERER']}");
		
	}
	
	//timetable function below
	function timetable()
	{
		$cls = new schoolClass();
		$subjects = $cls->get_subjects($_GET['class']);
		include(VIEWPATH."timetablev.php");
	}
	
	function edit_timetable()
	{
		$id = $_GET['id'];
		$cls = new schoolClass();
		
		$timetable = $cls->get_timetable($id);

		$class_list = $cls->select_all();
		include(VIEWPATH."edit_timetable.php");
	}
	
	function update_timetable()
	{
		#$this->xray($_POST); exit;
		$timetable_obj = new timetable();
		$data['class'] = $_POST['class'];
		for($i = 0; $i < count($_POST['time']); $i++)
		{
			$data['time'] = $_POST['time'][$i];
			$data['mon'] = $_POST['mon'][$i];
			$data['tue'] = $_POST['tue'][$i];
			$data['wed'] = $_POST['wed'][$i];
			$data['thu'] = $_POST['thu'][$i];
			$data['fri'] = $_POST['fri'][$i];
			
			$timetable_obj->insert($data);
		}

	}
	
	function delete_timetable()
	{
		//delete timetable file from server if it exists
                $timetable_file =  base64_decode($_GET['t']);
                if(file_exists($timetable_file))
                    unlink($timetable_file);
               
                $id = $_GET['id'];
		$tt['timetable'] = "";
		$cls = new schoolClass();
		$cls->update($id, $tt);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=timetables");
	}
	
	
}

?>
