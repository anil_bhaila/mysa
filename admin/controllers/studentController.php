<?php
class studentController extends Controller
{	
	function index()
	{
		$this->view_students();
	}
	
	function add_student()
	{		
		$parent = new thirdeye_parent();
		$result = $parent->select_all(array('parent_id','name'));
		
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."add_student.php");
		}
		else
		{
			if(!isset($_POST['gender']))
				$_POST['gender'] = '';  //this is a hack to force gender to validate
 				
			$validation_obj = new validation($_POST);
			$notempty = array('id','firstname','lastname','gender');
			$text = array('firstname','lastname');
			
			$validation_obj->is_empty($notempty);
			$validation_obj->is_text($text);
			//$validation_obj->is_dob('dob');
			
			if(!empty($_POST['email']))
				$validation_obj->is_email('email');
			if(!empty($_POST['phoneno']))
				$validation_obj->is_number('phoneno');
			if($validation_obj->error_found())
			{
				$errors = $validation_obj->get_error_messages();
				include(VIEWPATH."add_student.php");
				
				echo formPopulator::populate(); //populate the form with typed input
			}
			else
			{
				$this->upload_student_pic();
				
				//$_SESSION['student'] = $_POST; // store student info in session variable, to be used after
				
				if(!isset($_POST['parent_id']))
				{
					$parent = new thirdeye_parent();
					$_POST['password'] = md5('password'); //md5(generate_password());
					$parent_id = $parent->insert($_POST);
					$_POST['parent_id'] = $parent_id;
				}
				
				$_POST['dob'] = $_POST['dob_y'].'-'.$_POST['dob_m'].'-'.$_POST['dob_d'];
				$_POST['password'] = md5($_POST['student_id']);
				
				$student_obj = new student();
				//include(VIEWPATH."add_parent.php");
				$student_id = $student_obj->insert($_POST);	
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=student_profile&id=$student_id");
			}
		}
	}
	
	function save_student()
	{
		$student_obj = new student();
		if(!isset($_POST['parent_id']))
		{
			//insert parent first,so we can retrieve parent id, which will also be entered into the student table
			$parent = new thirdeye_parent();
			$_POST['password'] = md5('password'); //md5(generate_password());
			$parent_id = $parent->insert($_POST);
			
			$_SESSION['student']['parent_id'] = $parent_id;
			$student_id = $student_obj->insert($_SESSION['student']);	
		}
		else
		{
			$_SESSION['student']['parent_id'] = $_POST['parent_id'];
			$student_id = $student_obj->insert($_SESSION['student']);
		}
		
		unset($_SESSION['student']);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=student_profile&id=$student_id");
	}
	
	function view_students()
	{	
		$student_obj = new student();
		$students = $student_obj->select_all(array("status","id","firstname","lastname"));
		$start = 0;
		if(isset($_GET['start'])){
			$start = $_GET['start'];
		}
		if(is_array($students)){
		$per_page = $student_obj->get_record_per_page();
		$paging = student::page_links($students,$per_page,$start);
		$students = array_slice($students,$start,$per_page);
		}
		include(VIEWPATH."view_students.php");
	}
	
	function find_student()
	{
		include(VIEWPATH."find_student.php");
	}
	
	function find()
	{
		$id = $_POST['student_id']; //too trusting. should validate!
		
		$student_obj = new student();
		$student = $student_obj->select_all(null,null,"WHERE student_id = '$id'");
		if(count($student) > 0)
		{
			$session_obj = new session();
			$curr_sess = $session_obj->get_current_session_term();
			
			$cls = new schoolClass();
			$class_list = $cls->select_all();
			include(VIEWPATH."register_student.php");
		}
		else
			include(VIEWPATH."student_not_found.php");
	}
	
	function edit_student()
	{
		if(isset($_POST['student_id']))
			$student_id = $_POST['student_id'][0];
		if(isset($_GET['id']))
			$student_id = $_GET['id'];
		
		$student_obj = new student();
		$student = $student_obj->select($student_id);
		
		$include_page = "edit_student.php";
		include(VIEWPATH."student_profile.php");
	}
	
	function update_student()
	{
		$this->upload_student_pic();
		
		$id = $_POST['id'];
		$student_obj = new student();
		$student_obj->update($id,$_POST);
		$_SESSION['sysmsg'] = "<div id=\"msg\">You have sucessfully updated {$_POST['firstname']} {$_POST['lastname']} information</div>";
		$_SESSION['msgshown'] = false;
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=student_profile&id=$id");
							
	}
	
	function confirm_delete_student()
	{
		$student_id = $_GET['student_id'];
		$student_obj = new student();
		$student = $student_obj->select($student_id);
		
		$include_page = "confirm_delete_student.php";
		include(VIEWPATH."student_profile.php");
	}
	
	function delete_student()
	{
		$students = $_POST['student_id'];
		$student_obj = new student();
		for($i = 0; $i < count($students); $i++)
		{
			$student_obj->delete($students[$i]);
                        //TO-DO: also need to delete the student passport. This can be done
                        //if you know the location of the pic, which need to be retrieved from
                        //the db before calling the delete() method
		}
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=view_students");
	}
	
	function registration_form()
	{	
		$id = $_REQUEST['student_id']; //too trusting. should validate!
		
		$student_obj = new student();
		$student = $student_obj->select($id);
		
		if(count($student) > 0)
		{
			//current session is gotten from includes/top.php.
			//variable name: $csession	
			$cls = new schoolClass();
			$class_list = $cls->select_all();
			
			$include_page = "register_student.php";
			
		}
		else
		{
			$include_page = "student_not_found.php";
		}
		
		include(VIEWPATH."student_profile.php");
	}
	
	/*
	 * this registers a student in the school by inserting student details
	 * into the database, it calls two methods of the student class
	 * 1. register_class - registers student to a class
	 * 2. register_subjects - registers student subjects with respect to class
	 */
	function register_student()
	{	
        $student_obj = new student();
		
		$student_obj->register($_POST);
		
		$id = $_POST['id'];
		$rsession = $_POST['session'];
		$term = $_POST['term'];
		$student = $student_obj->select($id);
		
		$include_page = "registration_successful.php";
		include(VIEWPATH."student_profile.php");
		
	}
	
	function unregister()
	{
		$student_id = $_GET['student_id'];
		$student_obj = new student();
		$student_obj->unregister($student_id);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=student_profile&id=$student_id");
		
	}
	
	function student_profile()
	{
		$id = $_GET['id'];
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
			$this->$page();
		}
		else
		{
			$this->_student_profile();
		}
	}
	
	function _student_profile()
	{
		$id = $_GET['id'];
		$page = $_GET['page'];
		
		$student_obj = new student();
		$student = $student_obj->select($id);
		$subjects = $student_obj->get_subject_offered($id);
		
		$session_obj = new session();
		$session = $session_obj->get_current_session_term();
		
		$include_page = "profile.php";
		include(VIEWPATH."student_profile.php");
	}
	
	function drop_pick_subject()
	{
		$student_id = $_GET['student_id'];
		$class_id = $_GET['class'];
	
		$class_obj = new schoolClass();
		$subjects = $class_obj->get_subjects($class_id);
		
		$student_obj = new student();
		$student = $student_obj->select($student_id);
		$student_subjects = $student_obj->get_subject_offered($student_id);
		
		$include_page = "drop_pick_subject.php";
		include(VIEWPATH."student_profile.php");
	}
	
	function pick_subject()
	{
		$student_id = $_GET['student_id'];
		$subject_id = $_GET['subject_id'];
		
		$student_obj = new student();
		$student_obj->pick_subject($student_id,$subject_id);
		
		$class_id = $student_obj->get_current_class($student_id);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=drop_pick_subject&student_id=$student_id&class=$class_id");
		
	}
	
	function drop_subject()
	{
		$student_id = $_GET['student_id'];
		$subject_id = $_GET['subject_id'];
		
		$student_obj = new student();
		$student_obj->drop_subject($student_id,$subject_id);
		
		$class_id = $student_obj->get_current_class($student_id);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=drop_pick_subject&student_id=$student_id&class=$class_id");
		
	}

    /*
     * this function does not count expelled students
     */
    static function get_total_students()
    {
    	$student_obj = new student();
    	$row = $student_obj->select_all(array("id"),null,"WHERE status != 'expelled'");
    	return count($row);
    }
    
	
    function view_grades()
    {
    	$student_id = $_GET['student_id'];
  
    	$asst = new assesment();
    	$student_obj = new student();
    	
		$student = $student_obj->select($student_id);
    	$data = $student_obj->get_grades($student_id);
		//print_r($data);
		$include_page = "student_grades.php";
    	include(VIEWPATH."student_profile.php");
    }
    
    function view_attendance()
    {
    	$student_id = $_GET['student_id'];
    	$student_obj = new student();
		$student = $student_obj->select($student_id);
		$subjects = $student_obj->get_subject_offered($student_id);
		
		$include_page = "student_attendance.php";
    	include(VIEWPATH."student_profile.php");
    }
    
    function view_parent()
    {
    	
        $parent_id = $_GET['parent_id'];
    	$tp = new thirdeye_parent();
    	$parent = $tp->select($parent_id);

    	//This means we are viewing parent information from context of child student
        if(isset($_GET['student_id']))
        {
            $student_id = $_GET['student_id'];
            $student_obj = new student();
            $student = $student_obj->select($student_id);
			
			$include_page = "view_parent.php";
            include(VIEWPATH."student_profile.php");
        }
        else
        {
            // we are viewing parent information after performing a search
            // by clicking a link on the search result (sr)
            include(VIEWPATH."sr_view_parent.php");
        }
    }
    
    function edit_parent()
    {
        $parent_id = $_GET['parent_id'];
        $tp = new thirdeye_parent();
        $parent = $tp->select($parent_id);

        //This means we are editing parent information from context of child student
        if(isset($_GET['student_id']))
        {
            $student_id = $_GET['student_id'];
            $student_obj = new student();
            $student = $student_obj->select($student_id);
			
			$include_page = "edit_parent.php";
            include(VIEWPATH."student_profile.php");
        }
        else
        {
            // we are editing parent information after performing a search
            // by clicking a link on the search result (sr)
            include(VIEWPATH."sr_edit_parent.php");
        }
    }
	
	function update_parent()
	{
		if(isset($_POST['parent_id']))
		{
			$parent_id = $_POST['parent_id'];
			$student_id = $_POST['student_id'];
			
			$tp = new thirdeye_parent();
			$tp->update($parent_id,$_POST);
			
			header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=view_parent&student_id=$student_id&parent_id=$parent_id");
		}
	}
	
	function reset_password()
	{
		$student_id = $_GET['student_id'];
		$student_obj = new student();
		$student_obj->reset_password($student_id);
		
        $student = $student_obj->select($student_id);
		
		$msg = "<p>You have succesfully reset student password  </p>
		        <p>Student's password is now <strong>password</strong> <br />
		        </p>
        		<a href =\"students.php\">Continue</a>";
		
		$include_page = "password_reset.php";
		include(VIEWPATH."student_profile.php");
	}
	
	function parent_reset_password()
	{
            $parent_id = $_GET['parent_id'];
            $parent_obj = new thirdeye_parent();
            $parent_obj->reset_password($parent_id);
			
			$student_id = $_GET['student_id'];
			$student_obj = new student();
        	$student = $student_obj->select($student_id);
		

            if(isset($_GET['student_id']))
            {
                $student_id = $_GET['student_id'];
                $msg = "<p>You have succesfully reset parent password  </p>
                        <p>Parent's password is now <strong>password</strong> <br />
                        </p>
                        <a href =\"students.php?action=view_parent&parent_id=$parent_id&student_id=$student_id\">Continue</a>";

            }
            else
            {
                 $msg = "<p>You have succesfully reset parent password  </p>
                        <p>Parent's password is now <strong>password</strong> <br />
                        </p>
                        <a href =\"students.php\">Continue</a>";
            }
			
			$include_page = "password_reset.php";
            include(VIEWPATH."student_profile.php");
	}
	
	function search()
	{
		$keywords = $this->get_keywords();
		//$this->xray($keywords); exit;
		
		$student_obj = new student();
		$students = $student_obj->search($keywords);
		
		//$c = count($keywords);
		//$searched = $keywords[$c-1];
		
		include(VIEWPATH."view_students.php");
		echo formPopulator::populate(); //populate the form with typed input
			
	}
	
	function upload_student_pic()
	{
		if($_FILES['picture']['name'] != "")
                {
                    $fu = new fileUpload();
                    $fu->add_file('picture','image','../shared/uploads/images/',true);
                    $fu->validate();
                    return $fu->upload();
                }
	}

}

?>
