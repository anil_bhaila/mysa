<?php
class subjectController extends Controller
{
	
	function index()
	{
		$subject_obj = new subject();
		$subjects = $subject_obj->select_all();
		
		$class_obj = new schoolClass();
		
		$teacher_obj = new teacher();
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
			
				
		$class_list = $class_obj->select_all();
		
		$id = (int)$subject_obj->get_last_id() + 1;
		
		include(VIEWPATH."view_subjects.php");
	}
	
	//this function checks that all classes selected have been assigned a teacher
	function teachers_assigned($classes, $teachers)
	{
		for($i = 0; $i < count($classes); $i++)
		{
			$class_id = $classes[$i];
			$teacher_id = $teachers[$class_id];
			if($teacher_id == "")
				return false;	
		}
		return true; //every class has been assigned a teacher	
	}
	
	function add_subject()
	{
		$subject_obj = new subject();
		$class_obj = new schoolClass();
		$teacher_obj = new teacher();
		
		$subjects = $subject_obj->select_all();
		$id = (int)$subject_obj->get_last_id() + 1;
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
		$class_list = $class_obj->select_all();
		
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."view_subjects.php");
		}
		else
		{
			//validate, check we have a subject name
			$val = new validation($_POST);
			$val->is_empty('name');
			if($val->error_found())
			{
				$errors = $val->get_error_messages();
				include(VIEWPATH."view_subjects.php");
				echo formPopulator::populate();
			}
			else
			{
				//check that at least one class offers the subject before we add it
				$classes = $_POST['class_id'];
				if(count($classes) > 0)
				{
						$subject_id = $_POST['subject_id'];
						$teachers = $_POST['teacher_id'];
						
						if($this->teachers_assigned($classes,$teachers))
						{
							$subject_obj->insert($_POST);
							for($i = 0; $i < count($classes); $i++)
							{
								$class_id = $classes[$i];
								$teacher_id = $teachers[$class_id];
								
								$class_obj->add_subject($class_id, $subject_id, $teacher_id);
							}
							
							header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
						}
						else  //some classes have been selected but no teachers assigned
						{
							$error =  "<br/><br/><span style=\"color:red;\">Please make sure that every class selected has a teacher 
							assigned to them</span>";
							
							include(VIEWPATH."view_subjects.php");
							echo formPopulator::populate();
						}
				 }
				 else //no classes were selected
				 {
						$error =  "<br/><br/><span style=\"color:red;\">Please select at least one class</span>";
						
						include(VIEWPATH."view_subjects.php");
						echo formPopulator::populate();
				 }
			}
			
		}
	}
	
	
	function delete_subject()
	{
		$subject_id = $_GET['id'];

		$subject_obj = new subject();

		$subject_obj->delete($subject_id);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function edit_subject()
	{
		$subject_id = $_GET['id'];
		
		$subject_obj = new subject();
		$subject = $subject_obj->select($subject_id);

		$class_obj = new schoolClass();
		$teacher_obj = new teacher();
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
		$class_list = $class_obj->select_all();
		$id = (int)$subject_obj->get_last_id() + 1;
		
		$include_page = "edit_subject.php";
		include(VIEWPATH."view_subjects.php");
	}
	
	function update()
	{
		$this->upload_subject_outline();
		$id = $_POST['subject_id'];
		$subject_obj = new subject();
		$subject_obj->update($id,$_POST);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function teachers()
	{
		$subject_id = $_GET['id'];
		$teacher_obj = new teacher();
		$teacher_list = $teacher_obj->select_all(array("id","firstname","lastname"));
		
		$class_obj = new schoolClass();
		$class_list = $class_obj->offer_subject($subject_id);
		
		$subject_obj = new subject();
		$subject_name = $subject_obj->select($subject_id, array('name'));
		$subject_name = $subject_name[0]['name'];
		
		$teaching = $subject_obj->get_teachers($subject_id);
		
		$include_page = "change_teachers.php";
		include(VIEWPATH."view_subjects.php");
	}
	
	function change_teachers()
	{
		$subject_id = $_POST['subject_id'];
		$classes = $_POST['class_id'];
		$teachers = $_POST['teacher_id'];
		
		foreach($teachers as $class_id => $teacher_id)
		{
			$subject_obj = new subject();
			$subject_obj->change_teacher($subject_id, $class_id, $teacher_id);
		}
		$this->index();
		
		/*
		- Old code, will have to check that no erros were introduced by new
		- code before removing it permanently
		for($i = 0; $i < count($classes); $i++)
		{
			$class_id = $classes[$i];
			$teacher_id = $teachers[$class_id];
			
			$subject_obj = new subject();
			$subject_obj->change_teacher($subject_id, $class_id, $teacher_id);
		} */
		
		
	}

}

?>
