<?php
ob_start("ob_gzhandler");

if(!isset($_SESSION))
{
	session_start();
}
if(!$_SESSION['admin_login'])
	header("Location: login.php");
	
require_once("bootstrap.php");
require_once("../shared/lib/common.php");
require_once("../config.php");

$school_name = host::load_db(); //loads up the database connection and returns the name of the school

define("SCHOOL_NAME", $school_name);

$student_front = new studentController();
$student_front->run();


?>