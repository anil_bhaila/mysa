<?php
include_once("bootstrap.php");
include("../config.php");
include_once("../shared/lib/common.php"); 

host::load_db();

$keywords = get_keywords();
//$this->xray($keywords); exit;

$student_obj = new student();
$students = $student_obj->search($keywords);
?>
<table width="98%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr class="table-header">
	<td width="29%">Name</td>
	<td width="34%">Class</td>
	<td width="15%">Registered</td>
	<td width="19%">Status</td>
  </tr>
  <?php
  for($i = 0; $i < count($students); $i++)
  { ?>
  <tr bgcolor="<?php echo color($i); ?>">
	<td width="29%"><a href="students.php?action=student_profile&amp;id=<?php echo $students[$i]['id']; ?>"><?php echo $students[$i]['firstname'],' ',$students[$i]['lastname']; ?></a></td>
	<td width="34%"><?php 
	if(student::is_registered($students[$i]['id']) == "Yes")
		echo $student_obj->get_current_class($students[$i]['id']);
	elseif(student::is_registered($students[$i]['id']) == "No") 
		echo "--";
	?></td>
	<td width="15%"><?php echo student::is_registered($students[$i]['id']); ?></td>
	<td width="19%" bgcolor="<?php echo color($i); ?>"><strong><img src="../images/<?php echo $students[$i]['status']; ?>.gif" width="65" height="15"/></strong></td>
  </tr>
  <?php } ?>
</table>