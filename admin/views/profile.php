<table width="400" border="0" align="left" cellpadding="3" cellspacing="3">
  <tr>
    <td valign="top">Status:</td>
    <td valign="top"><?php echo $student[0]['status']; ?></td>
  </tr>
  <tr>
    <td width="186" valign="top">Student ID: </td>
    <td width="493" valign="top"><strong><?php echo $student[0]['student_id']; ?></strong></td>
  </tr>
  <tr>
    <td valign="top">Gender:</td>
    <td valign="top"><?php echo $student[0]['gender']; ?></td>
  </tr>
  <tr>
    <td valign="top">Current Class: </td>
    <td valign="top">
		<?php 
		if(student::is_registered($student[0]['id']) == "Yes")
		echo $student_obj->get_current_class($student[0]['id']);
		elseif(student::is_registered($student[0]['id']) == "No") 
		echo "--";
		?>
</td>
</tr>
<tr>
<td valign="top">Date of birth </td>
<td valign="top"><?php echo readable_date2($student[0]['dob']); ?></td>
</tr>
<tr bordercolor="#FFFFFF">
<td colspan="2" valign="top">&nbsp;</td>
</tr>
<tr bordercolor="#FFFFFF">
<td colspan="2" valign="top"><strong>Contact Information </strong></td>
</tr>
<tr bordercolor="#FFFFFF">
<td valign="top">Email:</td>
<td valign="top"><?php echo $student[0]['email']; ?></td>
</tr>
<tr bordercolor="#FFFFFF">
<td valign="top">Phone number:</td>
<td valign="top"><?php echo $student[0]['phoneno']; ?></td>
</tr>
<tr bordercolor="#FFFFFF">
<td valign="top"> Addresss:</td>
<td valign="top"><?php echo $student[0]['address']; ?></td>
</tr>
<tr bordercolor="#FFFFFF">
<td colspan="2" valign="top">&nbsp;</td>
</tr>
<tr bordercolor="#FFFFFF">
<td colspan="2" valign="top"><strong>Subjects Offered </strong></td>
</tr>
<tr bordercolor="#FFFFFF">
<td colspan="2" valign="top"><?php if(count($subjects) > 0) {?>
  <ol>
    <?php for($j= 0; $j < count($subjects); $j++) {?>
    <li><?php echo $subjects[$j]['name']; ?></li>
    <?php }?>
  </ol>
<?php } 
else
{
if($student[0]['gender'] == 'M') $text = "him"; else $text = "her";
echo $student[0]['firstname']," ",$student[0]['lastname']," is not registered for the current session/term <a href='students.php?action=registration_form&student_id={$student[0]['id']}'>Register $text</a>";

}
?></td>
</tr>
</table>