<?php include('includes/header.php'); ?>
  <h1>Students</h1>
  <div class="ajax-area">
  <table width="98%" border="0" cellpadding="5" cellspacing="0">
        <tr class="table-header">
            <td width="29%">Name</td>
            <td width="34%">Class</td>
            <td width="15%">Registered</td>
            <td width="19%">Status</td>
        </tr>
		  <?php
          for($i = 0; $i < count($students); $i++)
          { ?>
          <tr bgcolor="<?php echo color($i); ?>">
            <td width="29%"><a href="students.php?action=student_profile&id=<?php echo $students[$i]['id']; ?>" title="view student profile" ><?php echo $students[$i]['firstname'],' ',$students[$i]['lastname']; ?></a></td>
            <td width="34%"><?php 
            if(student::is_registered($students[$i]['id']) == "Yes")
                echo $student_obj->get_current_class($students[$i]['id']);
            elseif(student::is_registered($students[$i]['id']) == "No") 
                echo "--";
            ?></td>
            <td width="15%"><?php echo student::is_registered($students[$i]['id']); ?></td>
            <td width="19%" bgcolor="<?php echo color($i); ?>"><strong><img src="../images/<?php echo $students[$i]['status']; ?>.gif" width="65" height="15"/></strong></td>
          </tr>
        <?php } ?>
    </table>
</div> <!-- end of ajax area -->
<?php if(isset($paging) && $paging !=''): ?>
<div id="pager">Page: <?php echo $paging; ?></div>
<?php endif; ?>
</div> <!-- end of main -->

<div class="sidebar right">
	<div class="box rounded">
    	<span class="title">What would you like to do?</span>
        <span class="hint">Click on what you want to do to begin</span>
        <div><a href="students.php?action=add_student">Add a New Student</a></div>
        <div><a href="#">Register a Student</a></div>
        <div>Search for a Student</div>
        <div><input type="text" style="width:193px; height:25px;" id="search_string" onkeyup="searchStudent();"/></div>
        <span class="hint">Enter student information in the box above to search for student. Results found will be dsiplayed in the left side of this page</span>
    </div>
</div>

            <div style="clear:both"></div>
        </div>
    </div>
