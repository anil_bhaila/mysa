<?php
$menu = array('Dashboard' => 'index.php', 
			  'Students' => 'students.php' , 
			  'Teachers' => 'teachers.php' ,
			  'Classes' => 'classes.php' , 
			  'Subjects' => 'subjects.php' , 
			  'Notices/Events' => 'notices-events.php' , 
			  'My Account' => 'myaccount.php',
			  'Report' => 'report.php',
			  'Help' => 'help.php'
			  );

global $hide_menu;

//determine which menu was clicked
//script name
$script_name = $_SERVER['SCRIPT_NAME'];
$sn = substr($script_name,strrpos($script_name,'/')+1,strlen($script_name));

echo "<ul>";
foreach($menu as $page => $link ){
	if(!in_array($link,$hide_menu))
	{
		if($sn == $link)
		{
			echo "<li id=\"current\"><a href=\"$link\" >$page</a></li>";
		}
		else
		{
			echo "<li><a href=\"$link\">$page</a></li>";
		}
	}
}
echo "</ul>";
?>
