<?php $script_name = $_SERVER['SCRIPT_NAME'];
$sn = substr($script_name,strrpos($script_name,'/')+1,strlen($script_name));

 ?>
<!-- <link rel="stylesheet" type="text/css" href="../shared/jquery/css/base/ui.datepicker.css"> -->
<link rel="stylesheet" type="text/css" href="../shared/jquery/css/ui-lightness/jquery-ui-1.7.2.css">
<script src="../shared/jquery/js/ui.datepicker.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jquery/js/jquery-ui-1.7.2.min.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="../shared/jquery/js/ui.core.js"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<?php if($sn=='classes.php'): ?>
<script language="javascript">
function shout()
{	
	$.ui.dialog.defaults.bgiframe = true;
	$("#dialog").dialog({
		bgiframe: true,
		modal: true,
		draggable: false,
		position: 'center',
		resizable: false,
		buttons: {
				Ok: function() {
					$(this).dialog('close');
				}
			}
	});
		
}
function addClass()
{
	document.classlist.action = "classes.php?action=add_class";
	document.classlist.submit();
}
</script>
<?php endif; ?>

<?php if($sn=='report.php'): ?>
<script type="text/javascript" language="javascript" >
function getStudents()
{
	var cls = document.getElementById('report-class');
	var session = document.getElementById('report-session');
	var term = document.getElementById('report-term'); 
	
	var class_id = cls.options[cls.selectedIndex].text;
	var session_text = session.options[session.selectedIndex].text;
	var term_text = term.options[term.selectedIndex].text; 
	
	var url = "get_students.php?class="+class_id+"&session="+session_text+"&term="+term_text;
	$.get(url, function(data){
	$("#studentList").attr("innerHTML", data);
	});
}

function generateReport()
{
    var cls = document.getElementById('report-class');
    var session = document.getElementById('report-session');
    var term = document.getElementById('report-term'); 
    
    var class_id = cls.options[cls.selectedIndex].text;
    var session_text = session.options[session.selectedIndex].text;
    var term_text = term.options[term.selectedIndex].text; 
    
    var url = 'report.php?action=print_report&class='+class_id+'&session='+session_text+'&term='+term_text;
    window.open(url,'report','status,menubar,scrollbars,resizable');
}

</script>
<?php endif; ?>

<?php if($sn == 'notices-events.php'): ?>
<script type="text/javascript">
$(function() {
	$("#tabs").tabs();
});

$(function()
	{
		$('.date-pick').datepicker({
		dateFormat: 'yy-mm-dd'		
		});
	});
	
function populateElement(selector, defvalue) 
{
	    $(selector).each(
			function() 
			{
				if($.trim(this.value) == "") {
					this.value = defvalue;
				}
			}
		);
		
	  
	    $(selector).focus(function() {
	        if(this.value == defvalue) {
	            this.value = "";
	        }
	    });
	    
	    $(selector).blur(function() {
	        if($.trim(this.value) == "") {
	            this.value = defvalue;
	        }
	    });
}
	
</script>

<style type="text/css">
#tabs
{
width: 600px;
margin-left: 0px;
font-family: "Lucida Grande", sans-serif;
font-size: 14px;
}

#tabs ul li
{
height:30px;
}
</style>
<?php endif; ?>

<?php if($sn == 'students.php'): ?>
<script type="text/javascript" language="javascript">
function searchStudent()
{
	searchString = document.getElementById('search_string').value;
	var url = "search_student.php?search_string="+searchString;
	$.get(url, function(data){
	$(".ajax-area").attr("innerHTML", data);
	});
}

function show(obj)
{
	var class_id = obj.options[obj.selectedIndex].text; 
	var url = "get_subjects.php?class="+class_id;
	$.get(url, function(data){
	$("#subjects").attr("innerHTML", data);
	});   
}

$(function() {
	$("#dob").datepicker({
	showOn: 'both', 
	buttonImage: '', 
	buttonImageOnly: false,
	dateFormat: 'yy-mm-dd',
	defaultDate: new Date(1980,00,01)
	});
});


$(function() {
	$("#existingparent").hide();
})

function showAutocomplete()
{
	cb = document.getElementById("pcheck");
	
	if(cb.checked)
	{
		$("#newparent").hide();
		$("#existingparent").show();
	}
	else
	{
		$("#newparent").show();
		$("#existingparent").hide();
	}
}
	
</script>
<?php endif; ?>

<?php if($sn == 'teachers.php'): ?>
<script type="text/javascript" language="javascript">
function searchTeacher()
{
	searchString = document.getElementById('search_string').value;
	var url = "search_teacher.php?search_string="+searchString;
	$.get(url, function(data){
	$(".ajax-area").attr("innerHTML", data);
	});
}

function show(obj)
{
	var class_id = obj.options[obj.selectedIndex].text; 
	var url = "get_subjects.php?class="+class_id;
	$.get(url, function(data){
	$("#subjects").attr("innerHTML", data);
	});   
}
</script>
<?php endif; ?>

<?php if($sn == 'customize.php'): ?>
<script language="javascript">
function editGradeScheme()
{
	$("#gradescheme").load("customize.php?action=edit_grading_scheme");
}

function applyTheme()
{
	document.lookAndFeel.action = "customize.php?action=set_theme";
	document.lookAndFeel.submit();
}
</script>
<?php endif; ?>