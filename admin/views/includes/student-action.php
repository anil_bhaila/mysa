<table width="90%" border="0" align="center" cellpadding="3" cellspacing="0" id="student-profile-action">
  <tr>
    <td><img src="<?php echo $student[0]['picture']; ?>" width="120" height="150"/></td>
  </tr>
  <tr>
    <td><strong>Action</strong></td>
  </tr>
  <tr>
    <td><a href="students.php?page=edit_student&action=student_profile&amp;id=<?php echo $student[0]['id']; ?>">Edit   Information </a></td>
  </tr>
  <tr>
    <td> <?php if(student::is_registered($student[0]['id']) == "No") {?>
    <a href="students.php?action=registration_form&amp;student_id=<?php echo $student[0]['id']; ?>">Register </a>
    <?php } else {?>
    <a href="students.php?page=unregister&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>">Unregister </a>
    <?php } ?></td>
  </tr>
  <tr>
    <td><a href="students.php?page=drop_pick_subject&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>&amp;class=<?php echo $student_obj->get_current_class($student[0]['id']);?>">Drop/Pick Subjects</a> </td>
  </tr>
  <tr>
    <td><a href="students.php?action=student_profile&amp;id=<?php echo $student[0]['id']; ?>">View  Profile</a></td>
  </tr>
  <tr>
    <td><a href="students.php?page=view_grades&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>">View Grades</a> </td>
  </tr>
 <!-- <tr>
    <td><a href="students.php?page=view_attendance&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>">View Attendance</a> </td>
  </tr>-->
  <tr>
    <td><a href="students.php?page=view_parent&action=student_profile&amp;parent_id=<?php echo $student[0]['parent_id']; ?>&amp;student_id=<?php echo $student[0]['id']; ?>">Parent Information</a></td>
  </tr>
  <tr>
    <td><a href="students.php?page=reset_password&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>">Reset Password</a></td>
  </tr>
  <tr>
    <td><a href="students.php?page=confirm_delete_student&action=student_profile&amp;student_id=<?php echo $student[0]['id']; ?>">Delete</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
