<?php
$menu = array('Dashboard' => 'index.php', 
			  'Students' => 'students.php' , 
			  'Teachers' => 'teachers.php' ,
			  'Classes' => 'classes.php' , 
			  'Subjects' => 'subjects.php' , 
			 /* 'Notices/Events' => 'notices-events.php' , */
			  'Report' => 'report.php',
			  'Help' => 'help.php'
			  );
if(ENABLE_ACCOUNT === true)
{
	$menu['My Account'] = 'myaccount.php';
}

//echo "<pre>";
//print_r($_SERVER); 

//determine which menu was clicked

//script name
$script_name = $_SERVER['SCRIPT_NAME'];
$sn = substr($script_name,strrpos($script_name,'/')+1,strlen($script_name));

echo "<ul>";
foreach($menu as $page => $link ){
	if($sn == $link)
		echo "<li><a href=\"$link\" id=\"current\" >$page</a></li>";
	else
		echo "<li><a href=\"$link\">$page</a></li>";	
}
echo "</ul>";
?>
