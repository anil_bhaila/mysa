<?php include('includes/header.php'); ?>
<h1>Generate Report Card</h1>
<div style="min-height:60px;">
<!-- <table width="95%" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="margin:0 0 10px 5px;">
<tr>
  <td bgcolor="#FFFF99" align="center"><p style="text-align:left; background-color:#FFFF99; width:95%; line-height:20px; padding:0.5em; font-size:14px;">For report card generation you would need to have Adobe Reader installed to be able to view the PDF reports. If you don't already have it installed you can download from <a href="http://get.adobe.com/uk/reader/" target="_blank">http://get.adobe.com/reader/</a> </p>
    </td>
</tr>
</table> -->
<table width="95%" border="0" align="left" cellpadding="5" cellspacing="0">
<tr class="table-header">
   <td>Select Session </td>
   <td>Select Term</td>
   <td>Select Class</td>
   <td>&nbsp;</td>
</tr>
<tr>
    <td>
      <select  name="session" id="report-session" onchange="getStudents();">
      <option value="" selected="selected">-select-</option>
      <?php echo session_dropdown(); ?>
      </select>             
    </td>
    <td>
        <select name="term" id="report-term" onchange="getStudents();">
        <option>-select-</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        </select>
    </td>
    <td colspan="2"><select name="class_id" id="report-class" onchange="getStudents();">
        <option>-select-</option>
        <?php
          for($i = 0; $i < count($class_list); $i++)
          {
              echo "<option value=\"{$class_list[$i]['class_id']}\">{$class_list[$i]['class_id']}</option>";
          }
          ?>
      </select>
        <input name="submit" type="button"  onclick="generateReport()" value="Generate Report" />
      </td>
      </tr>
</table>
</div>

<div id="studentList" style="margin-top:10px;"></div>
</div> <!-- end of main -->

<div class="sidebar right">
	<div class="box rounded">
    	 <span class="title">Tips about Report Card</span>
        <div class="hint">To generate a report card  for a whole class, select the session, term and class and click on generate report... </div>
    </div>
</div>
<div style="clear:both"></div>
    


