<?php include('includes/header.php'); ?>
<h1>Class Subjects: <?php echo $_GET['class']; ?></h1>
<form name="subjectlist" method="post" action="classes.php?action=update_subjects">
  <table width="98%" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
      <td colspan="3">
      
      <input type="hidden" name="class_id" value="<?php echo $_GET['class']; ?>" />
      add or remove subjects offered by <strong><?php echo $_GET['class']; ?></strong> class and also assign teachers to those subjects </td>
    </tr>
    <tr class="table-header">
      <td width="7%">&nbsp;</td>
      <td width="42%">Subject Name </td>
      <td width="51%">Teacher</td>
    </tr>
    
    <?php
    $class_id = $_GET['class'];
    for($i = 0; $i < count($subjects); $i++)
    { 
          $subject_id = $subjects[$i]['subject_id'];
    ?>
    <tr bgcolor="<?php echo color($i); ?>">
      <td><?php if(!in_array($subject_id, $class_subjects)): 
              echo '<a href="classes.php?action=add_subject&class_id='.$class_id.'&subject_id='.$subject_id.'" style="color:green; font-size:12px;">add</a>'; 
            else:
              echo '<a href="classes.php?action=remove_subject&class_id='.$class_id.'&subject_id='.$subject_id.'" style="color:red; font-size:12px;">remove</a>';
          endif; 
      ?></td>
      <td>
      <?php 
      if(in_array($subject_id, $class_subjects)): 
          echo $subjects[$i]['name'];
      else:
          echo '<span style="color:#666;font-size:12px;">'.$subjects[$i]['name'].'<span/>';
      endif;
      ?>
      </td>
      <td>
        <?php
              $teacher = '<a href="#">Assign Teacher</a>';
              if(in_array($subject_id, $class_subjects))
              {
                  for($j = 0; $j < count($teacher_list); $j++)
                  {
                      if($class_teachers[$subject_id] == $teacher_list[$j]['id'])
                      {
                          $teacher = "{$teacher_list[$j]['firstname']} {$teacher_list[$j]['lastname']}";
                          break;
                      }
                  }
                  echo $teacher;
              }
              else
                      echo '...';
        ?>
      </td>
    </tr>
     <?php } ?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"><a href="classes.php?action=delete_class&amp;class_id=<?php echo $class_id; ?>">Delete this class</a></td>
    </tr>
  </table>
</form>
</div> <!-- end of main -->

<div class="sidebar right">
<div class="box rounded">
	<span class="title">Hint &amp; Tips</span>
    <div class="hint">To add/remove a subject to class, click on the add/remove link beside the subject name</div>
    <div class="hint">To assign a teacher to particular subject, click on the assign teacher link corresponding to that subject and select the teacher</div>
</div>
</div>
<div style="clear:both"></div>

