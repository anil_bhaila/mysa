<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Student Reports</title>
<style type="text/css" media="screen">
	body{font-family:helvetica;}
	h1,h2{font-size:24px;}
	.page{font-size:14px; border-bottom:1px solid #ccc; margin-bottom:40px; margin-top:40px; min-height:800px;}
	table{border:1px solid #333; padding:10px;}
	table td{border-bottom:1px solid #ccc;}
	.nb{border-bottom:none;} /* no border */
	.coverpage{display:none;}
</style>

<style type="text/css" media="print">
	body{font-family:helvetica;}
	h1,h2{font-size:20px;}
	.page{font-size:12px;  page-break-after:always;}
	table{border:1px solid #333; padding:10px;}
	table td{border-bottom:1px solid #ccc;}
	.nb{border-bottom:none;} /* no border */
	.coverpage{margin-top:200px;}
</style>
</head>
<body>
<div class="page coverpage">
<p align="center"><img src="<?php echo $school_logo; ?>"></p>
<h2 align="center"><?php echo $school_name; ?></h2>
<h2 align="center"><?php echo $report_title; ?></h2>
<h2 align="center"><?php echo $session; ?></h2>
<h2 align="center"><?php echo to_sequence($term); ?> Term</h2>
</div>


<?php
//echo count($students); exit;
for($i = 0; $i < count($students); $i++) 
{ 
	$id = $students[$i]['id'];
	
	//get student grades and display report sheet
	$data = $student->get_grades($id,$session,$term);
	if(count($data) != 0) //if we've got some data
	{
		$student_name = $data[0]['firstname']." ".$data[0]['lastname'];
		$student_id = $data[0]['student_id'];
		$student_class = $data[0]['class_id'];
		$student_gender = $data[0]['gender'];
		$session = $data[0]['session'];
		$term = $data[0]['term'];
		$class_position = $data[0]['class_position'];
?>		
		<div class="page">
		<!-- student info table -->
		<table border="0" cellspacing="0" cellpadding="0" width="800" align="center" style="margin-bottom:40px;">
			<tr><td rowspan="6" width="220" class="nb"><img src="<?php echo $school_logo; ?>" width="200" ></td><td class="nb">&nbsp;</td></tr>
			<tr><td style="font-size:18px;" class="nb"><strong><?php echo $school_name; ?></strong></td></tr>
			<tr><td class="nb"><?php echo $school_address; ?></td></tr>
			<tr><td class="nb">Tel: <?php echo $school_phone; ?></td></tr>
			<tr><td class="nb">&nbsp;</td></tr>
			<tr><td class="nb">&nbsp;</td></tr>
		</table>
		
		<table border="0" cellspacing="0" cellpadding="0" width="800" align="center" style="margin-bottom:20px;">
			<tr>
				<td><strong>Session:</strong></td>
				<td><?php echo $session; ?></td>
				<td><strong>Term:</strong></td>
				<td><?php echo $term; ?></td>
			</tr>
			<tr>
				<td><strong>Student Name:</strong></td>
				<td><?php echo $student_name; ?></td>
				<td><strong>Sex:</strong></td>
				<td><?php echo $student_gender; ?></td>
			</tr>
			<tr>
				<td><strong>Student ID:</strong></td>
				<td><?php echo $student_id; ?></td>
				<td><strong>Student Class:</strong></td>
				<td><?php echo $student_class; ?></td>
			</tr>
			<tr>
				<td><strong>Average Total:</strong></td>
				<td><?php echo ''; ?></td>
				<td><strong>Position in Class:</strong></td>
				<td><?php echo $class_position; ?></td>
			</tr>
		</table>
		
		<!-- grades table -->
		<table border="0" cellspacing="0" cellpadding="0" width="800" align="center" style="margin-bottom:20px;">
		<tr>
			<td><strong>Subject Name</strong></td>
			<?php foreach($assesments as $assesment): ?>
			<td align="center"><strong><?php echo $assesment['assesment_name']; ?></strong></td>
			<?php endforeach; ?>
			<td align="center"><strong>Total %</strong></td>
			<td align="center"><strong>Grade</strong></td>
			<td align="center"><strong>Position</strong></td>
			<!-- <td>Remark</td> -->
		</tr>
		<?php
		$gpa = 0;
		foreach($data as $row):
		
			$gpa += $row['total'];
			$name = ucwords(strtolower($row['name']));
		?>
		<tr>
			<td><?php echo $name; ?></td>
			<?php foreach($assesments as $assesment): ?>
			<?php $score = $row[$assesment['real_name']]; ?>
			<td align="center"><?php echo $score; ?></td>
			<?php endforeach; ?>
			<td align="center"><?php  echo $row['total']; ?></td>
			<td align="center"><?php echo grader::grade($row['total']); ?></td>
			<td align="center"><?php echo $sg->position_text($row['position']); ?></td>
		</tr>
		<?php endforeach;
		$gpa = round($gpa/count($data),2);
		?>
		</table>
		<!-- GPA table -->
		<table border="0" cellspacing="0" cellpadding="0" width="800" align="center" style="margin-bottom:20px;">
		<tr>
			<td width="100" class="nb"><strong>GPA</strong></td>
			<td class="nb"><?php echo $gpa; ?></td>
		</tr>
		</table>
		
		<!-- comments table -->
		
		<table border="0" cellspacing="0" cellpadding="0" width="800" align="center">
			<tr><td class="nb"><strong>Principal's Comment</strong></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
		</table>
		</div>
		
<?php		
	} 

 }
?>
</body>
</html>
