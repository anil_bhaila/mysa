
	<?php include("includes/header.php"); ?>

  <form action="classes.php?action=add_class" method="post" name="add_class" id="add_class">
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td colspan="2" align="left"><h2>New Class </h2>
          <hr/></td>
      </tr>
      <tr>
        <td width="22%" valign="top">Name: </td>
        <td width="78%"><label class="error">
          <input name="class_id" type="text" id="class_id" />
        <?php echo $errors['class_id']; ?>
        </label></td>
      </tr>
      <tr>
        <td valign="top">Teacher: </td>
        <td><label class="error">
        <select name="teacher_id" id="teacher_id">
		<option value="" selected="selected">...select...</option>
		<?php
		for($i = 0; $i < count($teacher_list); $i++)
		{
			echo "<option value=\"{$teacher_list[$i]['id']}\">{$teacher_list[$i]['firstname']} {$teacher_list[$i]['lastname']}</option>";
		}
		?>
        </select>
        <?php echo $errors['teacher_id']; ?>
        </label></td>
      </tr>
      
      
      <tr>
        <td>&nbsp;</td>
        <td><label>
          <input type="submit" name="Submit" value="Submit" />
        </label></td>
      </tr>
    </table>
  </form>
</div> <!-- end of main -->


