<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Event</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
<link href="../shared/jquery/css/ui-lightness/jquery-ui-1.7.2.css" rel="stylesheet" type="text/css" />
<link href="../css/datePicker.css" rel="stylesheet" type="text/css" />
<?php include('includes/jscript_include.php');?>
<script type="text/javascript" language="javascript">
$(function()
	{
		$('.date-pick').datePicker({clickInput: true,createButton: false});
	});
	
</script>
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>	
	<div id="navcontainer">
        <ul id="navlist">
          <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
	<div id="mid-col">
  <form name="form1" method="post" action="index.php?tab=setup&amp;action=update_event">
    <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
	  <tr>
        <td colspan="2" align="left"><h2>New Event </h2></td>
      </tr>
      <tr>
        <td width="17%">Event: </td>
        <td width="83%"><label class="error">
          <input name="event_name" type="text" id="event_name" value="<?php echo $event[0]['event_name']; ?>">
        <?php echo $errors['event_name']; ?>
        <input type="hidden" name="event_id" value="<?php echo $event[0]['event_id']; ?>" />
        </label></td>
      </tr>
      <tr>
        <td>Date: </td>
        <td><label class="error">
          <input type="text" name="event_date" class="date-pick dp-applied" value="<?php echo $event[0]['event_date']; ?>">
           <?php echo $errors['event_date']; ?>
        </label></td>
      </tr>
      <tr>
        <td>Time:  </td>
        <td>
        <select name="hour" id="hour">
          <option value="01" <?php if($hour == '01') echo 'selected'; ?>>01</option>
          <option value="02" <?php if($hour == '02') echo 'selected'; ?>>02</option>
          <option value="03" <?php if($hour == '03') echo 'selected'; ?>>03</option>
          <option value="04" <?php if($hour == '04') echo 'selected'; ?>>04</option>
          <option value="05" <?php if($hour == '05') echo 'selected'; ?>>05</option>
          <option value="06" <?php if($hour == '06') echo 'selected'; ?>>06</option>
          <option value="07" <?php if($hour == '07') echo 'selected'; ?>>07</option>
          <option value="08" <?php if($hour == '08') echo 'selected'; ?>>08</option>
          <option value="09" <?php if($hour == '09') echo 'selected'; ?>>09</option>
          <option value="10" <?php if($hour == '10') echo 'selected'; ?>>10</option>
          <option value="11" <?php if($hour == '11') echo 'selected'; ?>>11</option>
          <option value="12" <?php if($hour == '12') echo 'selected'; ?>>12</option>
        </select>
        <select name="minute" id="minute">
          <option value="00" <?php if($min == '00') echo 'selected'; ?>>00</option>
          <option value="01" <?php if($min == '01') echo 'selected'; ?>>01</option>
          <option value="02" <?php if($min == '02') echo 'selected'; ?>>02</option>
          <option value="03" <?php if($min == '03') echo 'selected'; ?>>03</option>
          <option value="04" <?php if($min == '04') echo 'selected'; ?>>04</option>
          <option value="05" <?php if($min == '05') echo 'selected'; ?>>05</option>
          <option value="06" <?php if($min == '06') echo 'selected'; ?>>06</option>
          <option value="07" <?php if($min == '07') echo 'selected'; ?>>07</option>
          <option value="08" <?php if($min == '08') echo 'selected'; ?>>08</option>
          <option value="09" <?php if($min == '09') echo 'selected'; ?>>09</option>
          <option value="10" <?php if($min == '10') echo 'selected'; ?>>10</option>
          <option value="11" <?php if($min == '11') echo 'selected'; ?>>11</option>
          <option value="12" <?php if($min == '12') echo 'selected'; ?>>12</option>
          <option value="13" <?php if($min == '13') echo 'selected'; ?>>13</option>
          <option value="14" <?php if($min == '14') echo 'selected'; ?>>14</option>
          <option value="15" <?php if($min == '15') echo 'selected'; ?>>15</option>
          <option value="16" <?php if($min == '16') echo 'selected'; ?>>16</option>
          <option value="17" <?php if($min == '17') echo 'selected'; ?>>17</option>
          <option value="18" <?php if($min == '18') echo 'selected'; ?>>18</option>
          <option value="19" <?php if($min == '19') echo 'selected'; ?>>19</option>
          <option value="20" <?php if($min == '20') echo 'selected'; ?>>20</option>
          <option value="21" <?php if($min == '21') echo 'selected'; ?>>21</option>
          <option value="22" <?php if($min == '22') echo 'selected'; ?>>22</option>
          <option value="23" <?php if($min == '23') echo 'selected'; ?>>23</option>
          <option value="24" <?php if($min == '24') echo 'selected'; ?>>24</option>
          <option value="25" <?php if($min == '25') echo 'selected'; ?>>25</option>
          <option value="26" <?php if($min == '26') echo 'selected'; ?>>26</option>
          <option value="27" <?php if($min == '27') echo 'selected'; ?>>27</option>
          <option value="28" <?php if($min == '28') echo 'selected'; ?>>28</option>
          <option value="29" <?php if($min == '29') echo 'selected'; ?>>29</option>
          <option value="30" <?php if($min == '30') echo 'selected'; ?>>30</option>
          <option value="31" <?php if($min == '31') echo 'selected'; ?>>31</option>
          <option value="32" <?php if($min == '32') echo 'selected'; ?>>32</option>
          <option value="33" <?php if($min == '33') echo 'selected'; ?>>33</option>
          <option value="34" <?php if($min == '34') echo 'selected'; ?>>34</option>
          <option value="35" <?php if($min == '35') echo 'selected'; ?>>35</option>
          <option value="36" <?php if($min == '36') echo 'selected'; ?>>36</option>
          <option value="37" <?php if($min == '37') echo 'selected'; ?>>37</option>
          <option value="38" <?php if($min == '38') echo 'selected'; ?>>38</option>
          <option value="39" <?php if($min == '39') echo 'selected'; ?>>39</option>
          <option value="40" <?php if($min == '40') echo 'selected'; ?>>40</option>
          <option value="41" <?php if($min == '41') echo 'selected'; ?>>41</option>
          <option value="42" <?php if($min == '42') echo 'selected'; ?>>42</option>
          <option value="43" <?php if($min == '43') echo 'selected'; ?>>43</option>
          <option value="44" <?php if($min == '44') echo 'selected'; ?>>44</option>
          <option value="45" <?php if($min == '45') echo 'selected'; ?>>45</option>
          <option value="46" <?php if($min == '46') echo 'selected'; ?>>46</option>
          <option value="47" <?php if($min == '47') echo 'selected'; ?>>47</option>
          <option value="48" <?php if($min == '48') echo 'selected'; ?>>48</option>
          <option value="49" <?php if($min == '49') echo 'selected'; ?>>49</option>
          <option value="50" <?php if($min == '50') echo 'selected'; ?>>50</option>
          <option value="51" <?php if($min == '51') echo 'selected'; ?>>51</option>
          <option value="52" <?php if($min == '52') echo 'selected'; ?>>52</option>
          <option value="53" <?php if($min == '53') echo 'selected'; ?>>53</option>
          <option value="54" <?php if($min == '54') echo 'selected'; ?>>54</option>
          <option value="55" <?php if($min == '55') echo 'selected'; ?>>55</option>
          <option value="56" <?php if($min == '56') echo 'selected'; ?>>56</option>
          <option value="57" <?php if($min == '57') echo 'selected'; ?>>57</option>
          <option value="58" <?php if($min == '58') echo 'selected'; ?>>58</option>
          <option value="59" <?php if($min == '59') echo 'selected'; ?>>59</option>
          </select>
        <select name="ampm" id="ampm">
          <option value="am" <?php if($ampm == 'am') echo 'selected'; ?>>am</option>
          <option value="pm" <?php if($ampm == 'pm') echo 'selected'; ?>>pm</option>
        </select>
        </td>
      </tr>
      
      <tr>
        <td>Details:</td>
        <td><label class="error">
          <textarea name="event_details" cols="50" rows="5" id="event_details"><?php echo $event[0]['event_details']; ?></textarea>
         <?php echo $errors['event_details']; ?>
        </label></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><label>
          <input type="submit" name="Submit" value="Submit">
        </label></td>
      </tr>
    </table>
  </form>
</div>
<div id="footer">
	  <p>&reg;myschoolassist 2010</p>
	  <p> <a href="http://myschoolassist.com">myschoolassist.com	</a></p>
	  <p>A product of B.E Systems</p>
	  <p><a href="#">Home</a> | <a href="#">Students</a> | <a href="#">Teachers</a> | <a href="#">Subjects</a> | <a href="#">Classes</a> | <a href="#">Report</a> | <a href="#">My Account</a> | <a href="#">My Settings</a> | <a href="#">Help</a> </p>
	  <p>Read the <a href="#">Terms &amp; Conditions</a> </p>
	</div>
</body>
</html>