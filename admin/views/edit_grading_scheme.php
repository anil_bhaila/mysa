<form action="customize.php?action=update_grader" method="post" name="" id="">
    <h3>Grading Scheme </h3>
    <p>Customize your grading scheme. Click the <strong>Submit</strong> button to save chnages. </p>
    <table width="50%" border="0" cellpadding="5" cellspacing="0">
      
      <tr>
        <td width="15%"><strong>Grade</strong></td>
        <td width="32%"><strong>From</strong></td>
        <td width="53%"><strong>To</strong></td>
      </tr>
	  <?php
	  for($i = 0; $i < count($scheme); $i++)
	  {
	  ?>
      <tr>
        <td><input name="id[]" type="hidden" id="id" value="<?php echo $scheme[$i]['id']; ?>" />
          <label>
          <select name="grade_letter[]" >
            <option value="A" selected="selected" <?php if (!(strcmp("A", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>A</option>
            <option value="B" <?php if (!(strcmp("B", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>B</option>
            <option value="C" <?php if (!(strcmp("C", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>C</option>
            <option value="D" <?php if (!(strcmp("D", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>D</option>
            <option value="E" <?php if (!(strcmp("E", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>E</option>
            <option value="F" <?php if (!(strcmp("F", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>F</option>
            <option value="P" <?php if (!(strcmp("P", $scheme[$i]['grade_letter']))) {echo "selected=\"selected\"";} ?>>P</option>
          </select>
          </label></td>
        <td><input name="grade_min[]" type="text" value="<?php echo $scheme[$i]['grade_min']; ?>" size="3" maxlength="2" /></td>
        <td><input name="grade_max[]" type="text" value="<?php echo $scheme[$i]['grade_max']; ?>" size="3" maxlength="3" /></td>
      </tr>
	  <?php 
	  }
	  ?>
      <tr>
        <td colspan="2"></td>
        <td align="right">
          <label>
          <input name="Cancel" type="button" id="Cancel" value="Cancel" onclick="location.href='customize.php'" />
          </label>
        <input type="submit" name="Submit" value="Submit" /></td>
      </tr>
    </table>
</form>