<form action="students.php?action=update_student" method="post" enctype="multipart/form-data" name="add_student" id="add_student">
<br>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td>Passport Photo: </td>
<td><label>
<input name="picture" type="file" id="picture" size="20" />
</label></td>
</tr>
<tr>
<td>Gender</td>
<td>
<input <?php if (!(strcmp($student[0]['gender'],"M"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" value="M"/>
male
<input <?php if (!(strcmp($student[0]['gender'],"F"))) {echo "checked=\"checked\"";} ?> name="gender" type="radio" value="F" />
female		</td>
</tr>
<tr>
<td width="">Student ID:</td>
<td><label>
<input name="student_id" type="text" id="student_id" value="<?php echo $student[0]['student_id']?>" size="30"/>
<input name="id" type="hidden" id="id" value="<?php echo $student[0]['id']?>" />
</label></td>
</tr>
<tr>
<td width="">First name: </td>
<td width=""><label>
<input name="firstname" type="text" id="firstname" size="30" value="<?php echo $student[0]['firstname']?>" />
</label></td>
</tr>
<tr>
<td width="">Last name: </td>
<td><label>
<input name="lastname" type="text" id="lastname" size="30" value="<?php echo $student[0]['lastname']?>" />
</label></td>
</tr>
<tr>
<td>Date of birth </td>
<td><label>
<input name="dob" type="text" class="date-pick-past dp-applied" id="dob" value="<?php echo $student[0]['dob']?>" size="29" />
</label></td>
</tr>
<tr>
<td>Email address: </td>
<td><label>
<input name="email" type="text" id="email" size="30" value="<?php echo $student[0]['email']?>" />
</label></td>
</tr>
<tr>
<td>Phone number: </td>
<td><label>
<input name="phoneno" type="text" id="phoneno" size="30" value="<?php echo $student[0]['phoneno']?>" />
</label></td>
</tr>

<tr>
<td width="">Status:</td>
<td><select name="status" id="status">
<option value="enrolled" selected="selected" <?php if (!(strcmp("enrolled", $student[0]['status']))) {echo "selected=\"selected\"";} ?>>Enrolled</option>
<option value="expelled" <?php if (!(strcmp("expelled", $student[0]['status']))) {echo "selected=\"selected\"";} ?>>Expelled</option>
<option value="graduated" <?php if (!(strcmp("graduated", $student[0]['status']))) {echo "selected=\"selected\"";} ?>>Graduated</option>
<option value="suspended" <?php if (!(strcmp("suspended", $student[0]['status']))) {echo "selected=\"selected\"";} ?>>Suspended</option>
<option value="transfered" <?php if (!(strcmp("transfered", $student[0]['status']))) {echo "selected=\"selected\"";} ?>>Transfered</option>
</select></td>
</tr>
<tr>
<td align="right">&nbsp;</td>
<td><label>
<input type="submit" name="Submit" value="Update" />
</label></td>
</tr>
</table>
</form>
	  