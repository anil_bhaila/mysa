<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Time Tables</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top"><?php include("includes/top.php"); ?></div>
<?php include("includes/main-nav.php"); ?>
<div id="left-col"><?php include("includes/left-side.php"); ?></div>
<div id="mid-col">
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
<tr>
    <td colspan="3"><h2>Time Tables</h2>
      <p>To replace an old timetable with a new one. Click on the <strong>delete</strong> link corresponding to the timetable you want to replace and then click on <strong>upload</strong> to replace with new timetable file. Only word documents and PDF files are supported. </p></td>
    </tr>
<tr bgcolor="<?php echo color($i); ?>">
    <th>Class</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
</tr>
<?php
for($i = 0; $i < count($ttable_list); $i++)
{
?>
  <tr bgcolor="<?php echo color($i); ?>">
    <td width="26%"><?php echo $ttable_list[$i]['class_id']; ?></td>
    <td width="16%">
	<?php 
	if(!empty($ttable_list[$i]['timetable']))
		echo "<a href=\"{$ttable_list[$i]['timetable']}\" target=\"_blank\">download</a>";
	else
		echo "<a href=\"classes.php?action=edit_timetable&amp;id={$ttable_list[$i]['class_id']}\">upload</a>";	
	?>	</td>
	<td width="58%"><a href="classes.php?action=delete_timetable&amp;id=<?php echo $ttable_list[$i]['class_id']; ?>&amp;t=<?php echo base64_encode($ttable_list[$i]['timetable']); ?>">delete</a></td>
  </tr>
<?php
}
?>
</table>
</div>
<div id="right-col">
<?php include("includes/right-side.php"); ?>
</div>
<div id="footer">myschoolassist 2009 myschoolassist.com</div>
</body>
</html>