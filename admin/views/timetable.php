<?php 
$options = "<option>Select Subject</option>";
for($i = 0; $i < count($subjects); $i++)
	$options .= '<option value ="'.$subjects[$i]['subject_id'].'" >'.$subjects[$i]['name'].'</option>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>

<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" language="javascript">
function displayTeacher(id,obj)
{
	subject = obj.value;
	var url = "get_teacher.php?s="+subject+"&c=<?php echo $_GET['class']; ?>";
	$.get(url, function(data){
	$("#"+id).attr("innerHTML", data);
	});
}
</script>
<style type="text/css">
<!--
.style1 {font-size: 12px}
input select
{
	height:100px;
}
-->
</style>
</head>

<body>
<form id="form1" name="form1" method="post" action="classes.php?action=update_timetable">
  <h1 align="center"><?php echo $_GET['class']; ?> Timetable </h1>
   <input name="class" type="hidden" id="class" value="<?php echo $_GET['class']; ?>" />
  <table width="100%" border="1" align="center" cellpadding="1" cellspacing="1">
    <tr>
      <td width="61" height="60">Time</td>
      <td width="78" height="60" align="center" valign="middle">
        <p>
          <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
        </p>
      </td>
      <td width="80" height="60" align="center" valign="middle">
        <p>
          <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
        </p>
      </td>
      <td width="88" height="60" align="center" valign="middle">
        <p>
          <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
        </p>
      </td>
      <td width="84" height="60" align="center" valign="middle">
        <p>
          <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
        </p>
      </td>
      <td width="88" height="60" align="center" valign="middle">
        <p>
          <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
        </p>
      </td>
      <td width="95" align="center" valign="middle"><p>
        <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
      </p>
      </td>
      <td width="80" align="center" valign="middle"><p>
        <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
      </p>
      </td>
      <td width="88" align="center" valign="middle"><p>
        <input name="time[]" type="text" id="time[]" value="00:00 - 00:00" size="10" />
      </p>
      </td>
    </tr>
    <tr>
      <td width="61" height="60"><h3>
        <label>MON</label>
      </h3></td>
      <td width="78" height="60" align="center">
        <select name="mon[]" id="mon[]" onchange="displayTeacher('t1',this)">
		<?php echo $options; ?>
        </select>
		<div id="t1"></div>
      </td>
      <td width="80" height="60" align="center">
	  <select name="mon[]" id="mon[]" onchange="displayTeacher('t2',this)">
        <?php echo $options; ?>
        </select>
		<div id="t2"></div>	
	  </td>
      <td width="88" height="60" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
      <td width="84" height="60" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
      <td width="95" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" align="center"><select name="mon[]" id="mon[]">
        <?php echo $options; ?>
            </select></td>
    </tr>
    <tr>
      <td width="61" height="60"><h3>TUE</h3></td>
      <td width="78" height="60" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" height="60" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="84" height="60" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="95" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" align="center"><select name="tue[]" id="tue[]">
        <?php echo $options; ?>
            </select></td>
    </tr>
    <tr>
      <td width="61" height="60"><h3>WED</h3></td>
      <td width="78" height="60" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" height="60" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="84" height="60" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="95" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" align="center"><select name="wed[]" id="wed[]">
        <?php echo $options; ?>
            </select></td>
    </tr>
    <tr>
      <td width="61" height="60"><h3>THU</h3></td>
      <td width="78" height="60" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" height="60" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="84" height="60" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="95" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" align="center"><select name="thu[]" id="thu[]">
        <?php echo $options; ?>
            </select></td>
    </tr>
    <tr>
      <td height="60"><h3>FRI</h3></td>
      <td width="78" height="60" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" height="60" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="84" height="60" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" height="60" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="95" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="80" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
      <td width="88" align="center"><select name="fri[]" id="fri[]">
        <?php echo $options; ?>
            </select></td>
    </tr>
  </table>
  <p align="center"><a href="#">
    <label>
    <input type="submit" name="Submit" value="Submit" />
    </label>
  </a></p>
</form>
</body>
</html>
