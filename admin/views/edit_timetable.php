<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Teachers</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="../../shared/jquery/css/ui-lightness/jquery-ui-1.7.2.css"
	rel="stylesheet" type="text/css" />
<link href="../css/datePicker.css" rel="stylesheet" type="text/css" />
<link href="../css/autocomplete.css" rel="stylesheet" type="text/css" />
<?php include('includes/jscript_include.php');?>
</head>

<body>
<div id="top"><?php include("includes/top.php"); ?></div>
<?php include("includes/main-nav.php"); ?>
<div id="left-col"><?php include("includes/left-side.php"); ?></div>
<div id="mid-col">
  <form action="classes.php?action=update_timetable" method="post" enctype="multipart/form-data" name="update_timetable">
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td width="100%" colspan="2" align="left"><h2>Update Time Table </h2>
          <hr/>
          <table width="100%" border="0" cellpadding="5" cellspacing="0">
            <tr>
              <td width="28%">Timetable for:</td>
              <td width="72%">
		  		<?php echo $timetable[0]['class_id']; ?>
                <input name="id" type="hidden" id="id" value="<?php echo $timetable[0]['class_id']; ?>" />
             </td>
            </tr>
            <tr>
              <td>Upload timetable file (doc or pdf) </td>
              <td><label>
                <input type="file" name="timetable" />
              </label></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><label>
                <input type="submit" name="Submit" value="Submit" />
              </label></td>
            </tr>
          </table>
          <p>&nbsp;</p></td>
      </tr>
    </table>
  </form>
</div>
<div id="right-col">
<?php include("includes/right-side.php"); ?>
</div>
<div id="footer">myschoolassist 2009 myschoolassist.com</div>
</body>
</html>