<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Find Student</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
<?php include("includes/top.php"); ?>
</div>
<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col" align="center">
    <br />
<span>	
		<strong>Student not found.</strong>
		<br />Please make sure student ID is correct
    	<br /><a href="students.php?action=find_student">Try again</a>
	</span>
</div>
<div id="footer">
	  <p>&reg;myschoolassist 2010</p>
	  <p> <a href="http://myschoolassist.com">myschoolassist.com	</a></p>
	  <p>Read the <a href="#">Terms &amp; Conditions</a> </p>
</div>
</body>

</html>
