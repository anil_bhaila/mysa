<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin | Home</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>	
	<div id="navcontainer">
        <ul id="navlist">
          <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col">
 <table width="95%" border="0">
  <tr>
    <td>
	<h2><?php echo $event[0]['event_name']; ?></h2>
	<p><strong>Date:</strong><?php echo readable_date($event[0]['event_date']); ?></p>
	<p><strong>Time:</strong> <?php echo $event[0]['event_time']; ?></p>
	<p><strong>Details:<br></strong>
	<?php echo $event[0]['event_details']; ?></p>
	<a href="index.php?action=delete_event&event_id=<?php echo $event[0]['event_id']; ?>">delete this event</a>
	</td>
    </tr>
</table>

</div>
	<div id="footer">
	  <p>&reg;myschoolassist 2010</p>
	  <p> <a href="http://myschoolassist.com">myschoolassist.com	</a></p>
	  <p>Read the <a href="#">Terms &amp; Conditions</a> </p>
	</div>
</body>
</html>
