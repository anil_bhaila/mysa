<?php include('includes/header.php'); ?>
<h1>Teachers</h1>
<div class="ajax-area">
<table width="98%" border="0" cellpadding="5" cellspacing="0">
      <tr class="table-header">
        <td width="36%">Name</td>
        <td width="64%">ID</td>
      </tr>
      <?php
      for($i = 0; $i < count($stafflist); $i++)
      { ?>
      <tr bgcolor="<?php echo color($i); ?>">
        <td><a href="teachers.php?action=teacher_profile&teacher_id=<?php echo $stafflist[$i]['id']; ?>"><?php echo $stafflist[$i]['firstname'],' ',$stafflist[$i]['lastname']; ?></a></td>
        <td width="64%"><a href="teachers.php?action=teacher_profile&teacher_id=<?php echo $stafflist[$i]['id']; ?>"><?php echo $stafflist[$i]['teacher_id']; ?></a></td>
      </tr>
      <?php } ?>
</table>
</div>
</div> <!-- end of main -->

<div class="sidebar right">
	<div class="box rounded">
    	<span class="title">What would you like to do?</span>
        <span class="hint">Click on what you want to do to begin</span>
      	<div><a href="teachers.php?action=add_teacher">Add a New Teacher</a></div>
        <div>Search for a Teacher</div>
        <div><input style="width:193px; height:25px;" name="search_string" type="text" id="search_string" size="40" onkeyup="searchTeacher();" /></div>
        <span class="hint">Enter teacher information in the box above to search for teacher. Results found will be dsiplayed in the left side of this page</span>
    </div>
</div>
<div style="clear:both"></div>


