<?php include('includes/header.php'); ?>
<h1>Subjects</h1>
<?php if(!isset($include_page)): ?>
 <table width="98%" border="0" cellpadding="5" cellspacing="0">
  <tr class="table-header">
     <td width="91%">Name</td>
     <td width="9%"></td>
   </tr>
   <?php for($i = 0; $i < count($subjects); $i++) { ?>
   <tr bgcolor="<?php echo color($i); ?>">
     <td width="91%"><a href="subjects.php?action=teachers&amp;id=<?php echo $subjects[$i]['subject_id']; ?>" title="change teachers for this subject"> <?php echo $subjects[$i]['name']; ?></a></td>
     <td width="9%" align="right"><a href="subjects.php?action=delete_subject&amp;id=<?php echo $subjects[$i]['subject_id']; ?>" class="notice-icons"><img src="../images/delete.png" alt="" width="16" height="16" border="0" title="delete this subject" /></a> <a href="subjects.php?action=edit_subject&amp;id=<?php echo $subjects[$i]['subject_id']; ?>" class="notice-icons"><img src="../images/edit2.png" alt="" width="16" height="16" border="0" title="edit this subject" /></a></td>
   </tr>
   <?php } ?>
 </table>
 <?php else: include($include_page); endif; ?> 
</div>

<div class="sidebar right">
	<div class="box rounded">
   	 <form id="add_subject" name="add_subject" method="post" action="subjects.php?action=add_subject" >
     <table width="98%" border="0" cellpadding="5" cellspacing="0">
       <tr>
         <td align="left"><h2>Add New Subject</h2></td>
       </tr>
       <tr>
         <td width="26%" valign="top"><strong>Subject name:</strong>
           <label class="error">
           <input name="name" type="text" id="name" size="25" style="<?php echo isset($errors['name'])? 'border:1px solid red' : ''; ?>" />
           <input name="subject_id" type="hidden" id="subject_id" value="<?php echo $id;?>" />
          </label></td>
       </tr>
      </table>
      <table width="98%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="3" valign="bottom" style="font-size:small">Select class(es) that offer this subject and assign a teacher to the class(es) <?php echo $error; ?> </td>
        </tr>
        <tr>
          <td width="86">&nbsp;</td>
          <td width=""><strong>Class</strong></td>
          <td width=""><strong>Teacher</strong></td>
        </tr>
        <?php for($i = 0; $i < count($class_list); $i++): ?>
					<tr bgcolor="<?php //echo color($i); ?>">
					  <td>
					  <label>
						<input name="class_id[]" type="checkbox" id="class_id[]" value="<?php echo $class_list[$i]['class_id']; ?>" 
				  onclick="enableElementById('teacher_id[<?php echo $class_list[$i]['class_id']; ?>]',this)"/>
				  </label>
					  </td>
					  <td><?php echo $class_list[$i]['class_id']; ?></td>
					  <td><label>
						<select name="teacher_id[<?php echo $class_list[$i]['class_id']; ?>]" disabled="disabled" id="teacher_id[<?php echo $class_list[$i]['class_id']; ?>]">
						  <option selected="selected" value="">...select...</option>
						  <?php for($j = 0; $j < count($teacher_list); $j++): ?>
					    <option value="<?php echo $teacher_list[$j]['id']; ?>"><?php echo $teacher_list[$j]['firstname'].' '.$teacher_list[$j]['lastname']; ?></option>
						  <?php endfor; ?>
            </select>
          </label></td>
        </tr>
        <?php endfor; ?>
      </table>
      <table width="98%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td align="right"><input name="Submit" type="submit" id="Submit" value="Add Subject" /></td>
        </tr>
    </table>
   </form>
  </div>
</div>
<div style="clear:both"></div>

