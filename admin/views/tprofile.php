
<table width="400" border="0" cellpadding="3" cellspacing="3">
<tr>
            <td colspan="2" valign="top"><strong>Basic Information </strong></td>
          </tr>
          <tr>
            <td valign="top">Name:</td>
            <td valign="top"><?php echo $teacher[0]['firstname']," ",$teacher[0]['lastname']; ?></td>
          </tr>
          
          <tr>
            <td width="186" valign="top">Teacher ID: </td>
            <td width="493" valign="top"><?php echo $teacher[0]['teacher_id']; ?></td>
          </tr>
          <tr>
            <td colspan="2" valign="top"><strong>Contact Information </strong></td>
          </tr>
          <tr>
            <td valign="top">Email:</td>
            <td valign="top"><?php echo $teacher[0]['email']; ?></td>
          </tr>
          <tr>
            <td valign="top">Phone number:</td>
            <td valign="top"><?php echo $teacher[0]['phoneno']; ?></td>
          </tr>
          <tr>
            <td valign="top"> Addresss:</td>
            <td valign="top"><?php echo $teacher[0]['address']; ?></td>
          </tr>
          <tr>
            <td colspan="2" valign="top"><strong>Subjects (<?php echo count($subjects); ?>)</strong></td>
          </tr>
          <tr>
            <td colspan="2" valign="top"><ol>
              <?php for($i = 0; $i < count($subjects); $i++): ?>
              <li><?php echo $subjects[$i]['name']," - ",$subjects[$i]['class_id']?></li>
              <?php endfor; ?>
            </ol></td>
          </tr>
        </table>
<br />
<br />