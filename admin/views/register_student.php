<form action="students.php?action=register_student" method="post" name="register_student" id="register_student">
   <table width="95%" border="0" cellpadding="5" cellspacing="0">
    <tr>
      <td colspan="5"><h3>Student Registration 
      </h3>
      <p><strong>Student ID:</strong> <?php echo $student[0]['student_id']; ?></p></td>
    </tr>
    <tr>
    <td colspan="5">Class &amp; Session</td>
    </tr>
      <tr>
        <td width="80">Session:
          <label></label></td>
        <td width="148"><label>
          <select name="session" id="session">
            <option value="1990/1991" <?php if (!(strcmp("1990/1991", $csession['session']))) {echo "selected=\"selected\"";} ?>>1990/1991</option>
            <option value="1991/1992" <?php if (!(strcmp("1991/1992", $csession['session']))) {echo "selected=\"selected\"";} ?>>1991/1992</option>
            <option value="1992/1993" <?php if (!(strcmp("1992/1993", $csession['session']))) {echo "selected=\"selected\"";} ?>>1992/1993</option>
            <option value="1993/1994" <?php if (!(strcmp("1993/1994", $csession['session']))) {echo "selected=\"selected\"";} ?>>1993/1994</option>
            <option value="1994/1995" <?php if (!(strcmp("1994/1995", $csession['session']))) {echo "selected=\"selected\"";} ?>>1994/1995</option>
            <option value="1995/1996" <?php if (!(strcmp("1995/1996", $csession['session']))) {echo "selected=\"selected\"";} ?>>1995/1996</option>
            <option value="1996/1997" <?php if (!(strcmp("1996/1997", $csession['session']))) {echo "selected=\"selected\"";} ?>>1996/1997</option>
            <option value="1998/1999" <?php if (!(strcmp("1998/1999", $csession['session']))) {echo "selected=\"selected\"";} ?>>1998/1999</option>
            <option value="1999/2000" <?php if (!(strcmp("1999/2000", $csession['session']))) {echo "selected=\"selected\"";} ?>>1999/2000</option>
            <option value="2000/2001" <?php if (!(strcmp("2000/2001", $csession['session']))) {echo "selected=\"selected\"";} ?>>2000/2001</option>
            <option value="2001/2002" <?php if (!(strcmp("2001/2002", $csession['session']))) {echo "selected=\"selected\"";} ?>>2001/2002</option>
            <option value="2002/2003" <?php if (!(strcmp("2002/2003", $csession['session']))) {echo "selected=\"selected\"";} ?>>2002/2003</option>
            <option value="2003/2004" <?php if (!(strcmp("2003/2004", $csession['session']))) {echo "selected=\"selected\"";} ?>>2003/2004</option>
            <option value="2004/2005" <?php if (!(strcmp("2004/2005", $csession['session']))) {echo "selected=\"selected\"";} ?>>2004/2005</option>
            <option value="2005/2006" <?php if (!(strcmp("2005/2006", $csession['session']))) {echo "selected=\"selected\"";} ?>>2005/2006</option>
            <option value="2006/2007" <?php if (!(strcmp("2006/2007", $csession['session']))) {echo "selected=\"selected\"";} ?>>2006/2007</option>
            <option value="2007/2008" <?php if (!(strcmp("2007/2008", $csession['session']))) {echo "selected=\"selected\"";} ?>>2007/2008</option>
            <option value="2008/2009" <?php if (!(strcmp("2008/2009", $csession['session']))) {echo "selected=\"selected\"";} ?>>2008/2009</option>
            <option value="2009/2010" <?php if (!(strcmp("2009/2010", $csession['session']))) {echo "selected=\"selected\"";} ?>>2009/2010</option>
            <option value="2010/2011" <?php if (!(strcmp("2010/2011", $csession['session']))) {echo "selected=\"selected\"";} ?>>2010/2011</option>
            <option value="2011/2012" <?php if (!(strcmp("2011/2012", $csession['session']))) {echo "selected=\"selected\"";} ?>>2011/2012</option>
            <option value="2012/2013" <?php if (!(strcmp("2012/2013", $csession['session']))) {echo "selected=\"selected\"";} ?>>2012/2013</option>
            <option value="2013/2014" <?php if (!(strcmp("2013/2014", $csession['session']))) {echo "selected=\"selected\"";} ?>>2013/2014</option>
            <option value="2014/2015" <?php if (!(strcmp("2014/2015", $csession['session']))) {echo "selected=\"selected\"";} ?>>2014/2015</option>
            <option value="2015/2016" <?php if (!(strcmp("2015/2016", $csession['session']))) {echo "selected=\"selected\"";} ?>>2015/2016</option>
            <option value="2016/2017" <?php if (!(strcmp("2016/2017", $csession['session']))) {echo "selected=\"selected\"";} ?>>2016/2017</option>
            <option value="2017/2018" <?php if (!(strcmp("2017/2018", $csession['session']))) {echo "selected=\"selected\"";} ?>>2017/2018</option>
            <option value="2018/2019" <?php if (!(strcmp("2018/2019", $csession['session']))) {echo "selected=\"selected\"";} ?>>2018/2019</option>
            <option value="2019/2020" <?php if (!(strcmp("2019/2020", $csession['session']))) {echo "selected=\"selected\"";} ?>>2019/2020</option>
          </select>
          <input name="id" type="hidden" id="id" value="<?php echo $student[0]['id']; ?>" />
        </label></td>
        <td width="169"><label></label></td>
        <td width="51">Term:
        <label></label></td>
        <td width="508"><select name="term" id="term">
          <option value="1" <?php if (!(strcmp(1, $csession['term']))) {echo "selected=\"selected\"";} ?>>1</option>
          <option value="2" <?php if (!(strcmp(2, $csession['term']))) {echo "selected=\"selected\"";} ?>>2</option>
          <option value="3" <?php if (!(strcmp(3, $csession['term']))) {echo "selected=\"selected\"";} ?>>3</option>
        </select></td>
      </tr>
      <tr>
        <td>Class:          </td>
        <td><select name="class_id"  id="class_id" onchange="show(this);">
          <option selected="selected" value="select">...select...</option>
          <?php
		  for($i = 0; $i < count($class_list); $i++)
		  {
		  		echo "<option value=\"{$class_list[$i]['class_id']}\">{$class_list[$i]['class_id']}</option>";
		  }
		?>
        </select></td>
        <td>&nbsp;</td>
        <td>Mode: </td>
        <td><select name="mode">
          <option selected="selected">Fresh</option>
          <option>Repeat</option>
        </select></td>
      </tr>
    </table>
	<div id="subjects">
	</div>
</form>