
<?php include('includes/header.php'); ?>
<h1><?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?></h1>
    <div style="">
        <div style="float:left; width:140px;">
        	<?php include("includes/student-action.php"); ?>
        </div>
        <div style="float:right; width:450px;">
        	<?php include($include_page); ?> 
        </div>
    </div>
</div> <!-- end of main -->

<div class="sidebar right">
	<div class="box rounded">
    	<span class="title">What would you like to do?</span>
        <span class="hint">Click on what you want to do to begin</span>
        <div><a href="students.php?action=add_student">Add a New Student</a></div>
        <div><a href="#">Register a Student</a></div>
        <div>Search for a Student</div>
        <div><input type="text" style="width:193px; height:25px;" id="search_string" onkeyup="searchStudent();"/></div>
        <span class="hint">Enter student information in the box above to search for student. Results found will be dsiplayed in the left side of this page</span>
    </div>
</div>
<div style="clear:both"></div>
