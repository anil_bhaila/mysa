<?php
ob_start("ob_gzhandler");

if(!isset($_SESSION))
{
	session_start();
}
if(!$_SESSION['admin_login'])
	header("Location: login.php");
	
require_once("bootstrap.php");
require_once("../shared/lib/common.php");
require_once("../config.php");

$school_name = host::start_app();

define("SCHOOL_NAME", $school_name);

$ne_front = new noticesEventsController();
$ne_front->run();


?>