<?php session_start(); 
$valid_users = array('admin','teacher','student','parent');

$user = 'admin';
if(isset($_GET['user']) && in_array($_GET['user'],$valid_users)){
	$user = $_GET['user'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>MySchoolAssist: <?php echo $school; ?></title>
<link rel="icon" href="website/images/ico.png" />
<style type="text/css">
body{font-family: Helvetica; margin:0; padding:0; background-color:#fff;}
#wrapper{width:860px; height:600px; right:0; bottom:2%; position: absolute;}
#school-name{width:450px; margin:0px auto 0 200px; font-size:23px;}
#menu{width:450px; margin:0px auto 0 200px; list-style:none; height:12px; padding:10px 10px 10px 0;}
	#menu li{float:left; margin-right:3px; display:block;}
	#current{background-color:#0A50CC !important; color:#fff !important;}
	#menu a{ background:#ccc; padding:10px 10px 4px 10px; text-decoration:none; color:#000; font-size:16px; -webkit-border-radius:5px 5px 0 0;}
	#menu a:hover{ background:#D6E3F7;}
#tab-bottom{width:450px; height:3px; margin:0px auto 0 200px; padding:0px; background:url(images/login-bg.jpg) no-repeat; }
#login-form{width:450px; padding:0px; margin:20px auto 0 200px;font-size:16px;}
#login-form input{width:200px; height:25px; font-size:19px;}
#login-form div, #login-form span{padding:10px 10px 10px 10px;}
#login-btn{width:95px !important; height:34px !important;  margin-left:120px;  background-color:#0A50CC; border:0; color:#fff; font-size:19px;}
#school-logo{position:absolute; top:70px;}
#error-msg{text-align:center; color:#F00; width:100%; margin:5px 0 10px 0px; padding:0 !important; font-size:12px;}
</style>
</head>

<body onload="document.forms[0].username.focus();">
	<div id="wrapper">

        <div style="float:left; padding-top:100px;"><img src="images/logo.jpg"></div>

        <div style="float:right; width:720px;">
        	<div id="school-name"><h1 style="font-weight:normal; font-size:26px;">Login</h1></div>
            <ul id="menu">
                <li><a href="?user=admin" <?php if($user == 'admin'): ?>id="current" <?php endif; ?>>Admin</a></li>
                <li><a href="?user=teacher" <?php if($user == 'teacher'): ?>id="current" <?php endif; ?>>Teacher</a></li>
                <li><a href="?user=student" <?php if($user == 'student'): ?>id="current" <?php endif; ?>>Student</a></li>
                <li><a href="?user=parent" <?php if($user == 'parent'): ?>id="current" <?php endif; ?>>Parent</a></li>
            </ul>
            <div id="tab-bottom"></div>
            <div id="login-form">
            <?php if(isset($_SESSION['login_err'])): unset($_SESSION['login_err']); ?><div id="error-msg">The Username/Password you've entered seems to be incorrect. Please try again.</div><?php endif; ?>
                <form id="form" action="<?php echo $user; ?>/login.php" method="post">
                    <span>Username:</span>
                    <div><input type="text" name="username" /></div>
                    <span>Password:</span>
                    <div><input type="password" name="password"/></div>
                    <input type="hidden" name="user" />
                    <input type="submit" name="Submit" value="Login" id="login-btn"/>
                </form>
            </div>
        </div>


    </div>
</body>
</html>
	