<?php
if(!isset($_SESSION))
session_start();

if(!$_SESSION['manager_login'])
	header("Location: login.php");

require_once("bootstrap.php");
require_once("dbConfig.php");
require_once("../shared/lib/common.php");


function current_tab($tab)
{
	if($_GET['t'] == $tab)
		echo "current";
	else
		echo "";
}

ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Third eye: Admin</title>

<link href="../shared/jquery/css/ui-lightness/jquery-ui-1.7.2.css" rel="stylesheet" type="text/css" />
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

<link href="css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="css/tabs.css" rel="stylesheet" type="text/css" />

<script src="../shared/jscripts/firebug.js"  type="text/javascript" ></script>

<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jquery/js/jquery-ui-1.7.2.min.js" type="text/javascript" language="javascript"></script>


<!-- required plugins -->
<script src="../shared/jscripts/date.js" type="text/javascript" language="javascript"></script>
<!--[if IE]><script src="../shared/jscripts/jquery.bgiframe.js" type="text/javascript" ></script><![endif]-->
<!-- jquery.datePicker.js -->

<script src="../shared/jscripts/jquery.datePicker.js" type="text/javascript" ></script>


<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/bsn.AutoSuggest_c_2.0.js" type="text/javascript" ></script>
<script src="../shared/jscripts/hacks.js" type="text/javascript" ></script>



</head>
<body>
<div id="header">
  <h2>Third Eye Manager </h2>	
</div>
<div id="tabs" class="dd">
 <ul>
 	<li id="<?php echo current_tab('1');?>"><a href="index.php?t=1&tab=account"><span>Accounts</span></a></li>
	<li id="<?php echo current_tab('2');?>"><a href="index.php?t=2&tab=user"><span>Users</span></a></li>
	<li id="<?php echo current_tab('3');?>"><a href="index.php?t=3&tab=user&action=changepass"><span>Change password</span></a></li>
	<li><a href="logout.php"><span style="color:#FF0000">X Logout</span></a></li>
</ul>
</div>
<?php
$front = new Controller();
$front->run(); 

?>
<!--<div id="bottom">powered by thirdeye 2009</div>-->

</body>


</html>
