<?php
class accountController extends Controller
{
	function __construct()
	{
		$this->acc = new account();
	}
	
	function index()
	{
		$accounts = $this->acc->select_all();
		include(VIEWPATH."accounts.php");
	}
	
	function create()
	{
		include(VIEWPATH."register.php");
	}
	
	function create_account()
	{
		$this->acc->insert($_POST,$_POST['url']);
	}
	
	function view()
	{
		$accounts = $this->acc->select_all();
		include(VIEWPATH."accounts.php");
	}
	
	function edit()
	{
		$id = $_POST['account'][0];
		$account = $this->acc->select($id);
		include(VIEWPATH."edit_account.php");
	}
	
	function update_account()
	{
		$id = $_POST['id'];
		$this->acc->update($id,$_POST);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?t=1&tab=account");
	}
	
	function delete()
	{
		include(VIEWPATH."cant_delete.php");
	}
	
	function activate()
	{
		include(VIEWPATH."activate_account.php");
	}
}

?>